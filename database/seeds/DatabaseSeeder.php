<?php

use App\Models\Destacamento;
use App\Models\EstadoIncidenciaCivil;
use App\Models\EstadoUnidad;
use App\Models\Marca;
use App\Models\Modelo;
use App\Models\Municipio;
use App\Models\Provincia;
use App\Models\Rango;
use App\Models\Sector;
use App\Models\TipoOficial;
use Carbon\Carbon;
use Grimzy\LaravelMysqlSpatial\Types\LineString;
use Grimzy\LaravelMysqlSpatial\Types\Point;
use Grimzy\LaravelMysqlSpatial\Types\Polygon;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = \Faker\Factory::create();
        //Geografia
        foreach ($this->getDataFile("provincias") as $p) {
            $i = new Provincia();
            $i->nombre = $p['nombre'];
            $coll = [];
            if (!empty($p['limites'])){
                foreach ($p['limites'] as $l) {
                    array_push($coll, new Point($l[0],$l[1]));
                }
                $i->limites = new Polygon([new LineString($coll)]);
            }
            $i->save();
        }



        $santiago = Provincia::where('nombre','Santiago')->get()->first();
        foreach ($this->getDataFile("municipios") as $p) {
            $i = new Municipio();
            $i->nombre = $p['nombre'];
            $i->provincia()->associate($santiago);
            $coll = [];
            if (!empty($p['limites'])){
                foreach ($p['limites'] as $l) {
                    array_push($coll, new Point($l[0],$l[1]));
                }
                $i->limites = new Polygon([new LineString($coll)]);
            }
            $i->save();
        }

        $santiago = Municipio::where('nombre','Santiago de Los Caballeros')->get()->first();
        $zona = new \App\Models\Zona();
        $zona->nombre = "Sur";
        $zona->municipio()->associate($santiago);
        $zona->save();

        $zona2 = new \App\Models\Zona();
        $zona2->nombre = "Norte";
        $zona2->municipio()->associate($santiago);
        $zona2->save();
        $zonas = [
            $zona, $zona2
        ];

        foreach ($this->getDataFile("sectores") as $p) {
            $i = new Sector();
            $i->nombre = $p['nombre'];
            $i->zona()->associate($zona2);
            $i->municipio()->associate($santiago);
            $coll = [];
            if (!empty($p['limites'])){
                foreach ($p['limites'] as $l) {
                    array_push($coll, new Point($l[0],$l[1]));
                }
                $i->limites = new Polygon([new LineString($coll)]);
            }
            $i->save();
        }

        foreach ($this->getDataFile('estados_incidencias_civiles') as $e) {
            EstadoIncidenciaCivil::create([
                'nombre' => $e
            ]);
        }

        $modelos = [];
        foreach ($this->getDataFile('marcas_modelos') as $m) {
            $marca = Marca::create(['nombre' => $m['name']]);
            foreach ($m['models'] as $i) {
                $mod = new Modelo();
                $mod->nombre = $i;
                $mod->marca()->associate($marca);
                $mod->save();
                array_push($modelos,$mod->id);
            }
        }


        foreach ($this->getDataFile('tipos_incidencias_civiles') as $m) {
            \App\Models\TipoIncidenciaCivil::create(
                ['nombre' => $m]);
        }

        foreach ($this->getDataFile('prioridades_incidencias_civiles') as $key => $v) {
            \App\Models\PrioridadIncidenciaCivil::create(
                ['nombre' => $key, 'orden' => $v]);
        }

        $i=0;
        foreach ($this->getDataFile('rangos') as $m) {
            Rango::create([
                'nombre' => $m,
                'orden' => $i++
            ]);
        }
        $i=0;
        foreach ($this->getDataFile('tipos_oficiales') as $m) {
            TipoOficial::create(['nombre' => $m, 'orden' => $i++]);
        }

        $sectores = Sector::whereIn('nombre', ["Embrujo I","Embrujo II", "Embrujo III", "Villa Olga", "Rincon Largo"])->get();
        foreach ($sectores as $s) {
            $zona->sectores()->save($s);
        }

        $dest =  Destacamento::create([
            'zona_id' => $zona->id,
            'nombre' => $faker->name,
            'direccion' => $faker->address,
            'ubicacion' => new Point(19.449027, -70.666220),
            'telefono' => '8095736565'
        ]);
        foreach ($this->getDataFile('estados_unidades') as $m) {
            EstadoUnidad::create(['nombre' => $m]);
        }

        foreach ($this->getDataFile('estados_mensajes_hubs') as $m) {
            \App\Models\EstadoMensajeHub::create(['nombre' => $m]);
        }

        $estado = EstadoUnidad::where('nombre','Disponible')->get()->first();

        $unidades = [];
        $secs = Municipio::where('nombre','Santiago de Los Caballeros')->get()
            ->first()->sectores()->whereNotNull('limites')->get();

        $created = false;

        for ($i=0;$i<14;$i++ ) {
            $unidad = \App\Models\Unidad::create([
                'modelo_id' => $modelos[array_rand($modelos)],
                'sector_id' => $secs->random()->id,
                'estado_unidad_id' => $estado->id,
                'destacamento_id' => $dest->id,
                'ano' => $faker->numberBetween(1980,2005),
                'tipo' =>$faker->numberBetween(0,1),
                'placa' => $faker->randomLetter.$faker->randomDigit.$faker->randomDigit.$faker->randomDigit.
                    $faker->randomDigit.$faker->randomDigit.$faker->randomDigit
            ]);
            array_push($unidades,$unidad->id);
            if (!$created) {
                \App\Models\PmsHub::create([
                    'unidad_id' => $unidad->id,
                    'serial' => "00000000ddc7c7fb"
                ]);
                $created = true;
            } else {
                \App\Models\PmsHub::create([
                    'unidad_id' => $unidad->id,
                    'serial' => $faker->iban(null)
                ]);
            }

        }

        $nel = \App\Models\Oficial::create([
            'rango_id' => Rango::inRandomOrder()->get()->first()->id,
            'tipo_oficial_id' => TipoOficial::SARGENTO_GUARDIA,
            'destacamento_id' => $dest->id,
            'nombre' => 'Nelson',
            'apellido' => 'Perez Lora',
            'fecha_nacimiento' => '1995-05-25',
            'fecha_ingreso' => '2018-01-20',
            'sexo' => 0,
            'cedula' => '402-2322273-4'
        ]);

        $nelsonusuario = \App\Models\Usuario::create([
            'oficial_id' => $nel->id,
            'email' => 'elnel@gmail.com',
            'password' => bcrypt('123456'),
            'tipo' => \App\Models\Usuario::TIPO_SADMIN
        ]);

        $oficiales = [];


        $pruebaofi = \App\Models\Oficial::create([
            'rango_id' => Rango::inRandomOrder()->get()->first()->id,
            'foto' => null,
            'destacamento_id' => $dest->id,
            'nombre' => "Nelson Prueba",
            'apellido' => "Policia Unidad",
            'fecha_nacimiento' => $faker->dateTimeBetween('1960-01-01','1995-01-01'),
            'fecha_ingreso' => $faker->dateTimeBetween('2000-01-01','2018-01-20'),
            'sexo' => $faker->numberBetween(0,1),
            'tipo_oficial_id' =>2,
            'cedula' => '402-'.$faker->randomDigit.$faker->randomDigit.$faker->randomDigit.
                $faker->randomDigit.$faker->randomDigit.$faker->randomDigit.'-'.$faker->randomDigit
        ]);

        \App\Models\Ibutton::create([
            'oficial_id' => $pruebaofi->id,
            'code' => '01-000001cd5180'
        ]);

        \App\Models\Flota::create([
            'oficial_id' => $pruebaofi->id,
            'digitos' => '809-341-0984',
            'mac_address' =>  "80:65:6D:90:43:F7" //Mi celular
        ]);

        $uni = \App\Models\Unidad::all()->first();
        $uni->oficiales()->save($pruebaofi);

        for ($i=0;$i<61;$i++) {
            $o = \App\Models\Oficial::create([
                'rango_id' => Rango::inRandomOrder()->get()->first()->id,
                'foto' => null,
                'destacamento_id' => $dest->id,
                'nombre' => $faker->firstName,
                'apellido' => $faker->lastName,
                'tipo_oficial_id' => 1,
                'fecha_nacimiento' => $faker->dateTimeBetween('1960-01-01','1995-01-01'),
                'fecha_ingreso' => $faker->dateTimeBetween('2000-01-01','2018-01-20'),
                'sexo' => $faker->numberBetween(0,1),
                'cedula' => '402-'.$faker->randomDigit.$faker->randomDigit.$faker->randomDigit.
                    $faker->randomDigit.$faker->randomDigit.$faker->randomDigit.'-'.$faker->randomDigit
            ]);
            \App\Models\Flota::create([
                'oficial_id' => $o->id,
                'digitos' => '809-'.$faker->randomDigit.$faker->randomDigit.$faker->randomDigit.'-'.$faker->randomDigit.
                    $faker->randomDigit.$faker->randomDigit.$faker->randomDigit,
                'mac_address' => $faker->macAddress
            ]);
            array_push($oficiales,$o);
        }

        $uni = \App\Models\Unidad::all();
        $h = 1;
        foreach ($uni as $u) {
            if ($u->id !== 1) {
                $oficiales[$h]->tipo_oficial_id = 2;
            } else {
                $oficiales[$h]->tipo_oficial_id = 1;
            }

            $oficiales[$h+1]->tipo_oficial_id = 1;
            $oficiales[$h+2]->tipo_oficial_id = 1;

            $oficiales[$h]->save();
            $oficiales[$h+1]->save();
            $oficiales[$h+2]->save();
            $u->oficiales()->save($oficiales[$h]);
            $u->oficiales()->save($oficiales[$h+1]);
            if ($u->id !== 1) {
                $u->oficiales()->save($oficiales[$h+2]);
            }

            \App\Models\MensajeHub::create([
                'oficial_unidad_id' => $oficiales[$h]->id,
                'estado_mensaje_hub_id' => \App\Models\EstadoMensajeHub::ENVIADO,
                'pms_hub_id' => $oficiales[$h]->unidad->hub->id,
                "contenido" => $faker->sentences(1,true),
                "usuario_id" => $nelsonusuario->id,
                'sentido' => 0
            ]);

            \App\Models\MensajeHub::create([
                'oficial_unidad_id' => $oficiales[$h]->id,
                'estado_mensaje_hub_id' => \App\Models\EstadoMensajeHub::ENVIADO,
                'pms_hub_id' => $oficiales[$h]->unidad->hub->id,
                "contenido" => $faker->sentences(5,true),
                'sentido' => 1
            ]);

            $h +=3;

        }


        \App\Models\Ibutton::create([
            'oficial_id' => $oficiales[0]->id,
            'code' => '0000016BE680'
        ]);
        \App\Models\Ibutton::create([
            'oficial_id' => $oficiales[1]->id,
            'code' => '00000134A189'
        ]);



        $locations  = $this->getDataFile('test_locations');
        foreach (\App\Models\Unidad::all() as $u) {
            $n = array_rand($locations);
            \App\Models\HubLocalizacion::create([
                'pms_hub_id' => $u->hub->id,
                'fecha_obtenida' => Carbon::now()->subMinutes(
                    $faker->numberBetween(60,12*60))->toDateTimeString(),
                'ubicacion' => new Point($locations[$n][0],
                    $locations[$n][1])
            ]);
            unset($locations[$n]);
            array_filter($locations);

            $n = array_rand($locations);

            unset($locations[$n]);
            array_filter($locations);
        }

        $uni = \App\Models\Unidad::find(1);
        $uni->sector_id = 113;
        $uni->save();
        $uni->localizaciones->first()->delete();
        $startdate = \Carbon\Carbon::today();
        foreach ($this->getDataFile('snap_locations') as $m) {
            $startdate = $startdate->addSeconds($faker->numberBetween(30,200));
            \App\Models\HubLocalizacion::create([
                'pms_hub_id' => $uni->hub->id,
                'fecha_obtenida' => $startdate,
                'ubicacion' => new Point($m[0], $m[1])
            ]);
        }

        $tipoid  = \App\Models\TipoIncidenciaPolicial::create([
            'nombre' => 'Fuera de Zona'
        ])->id;

        $service = new \App\Services\IncidenciaCivilService();

        $sp = 0;
        $z  = 0;
        for ($i=0;$i<150;$i++) {
            $n = array_rand($locations);

            $fecha_incidencia  = Carbon::now()->subMinutes($faker->numberBetween(10,44640));

            $creado_en = $fecha_incidencia->copy()->addMinutes($faker->numberBetween(3,20));

            $inci = $service->createIncidenciaCivil([
                'nombre_civil' => $faker->name,
                'telefono_civil' =>  '809-'.$faker->randomDigit.$faker->randomDigit.$faker->randomDigit.'-'.$faker->randomDigit.
                    $faker->randomDigit.$faker->randomDigit.$faker->randomDigit,
                'destacamento' => $dest->id,
                'sector' => $secs->random()->id,
                'prioridad' => \App\Models\PrioridadIncidenciaCivil::all()->random()->id,
                'tipo' => \App\Models\TipoIncidenciaCivil::all()->random()->id,
                'ubicacion' => new Point($locations[$n][0],
                    $locations[$n][1]),
                'fecha_incidencia' => $fecha_incidencia->toDateTimeString(),
                'creado_en' => $creado_en->toDateTimeString(),
                'actualizado_en' => $creado_en->toDateTimeString(),
                'creador' => $nelsonusuario->id,
                'ubicacion_texto' => $faker->streetName,
                'detalle_incidente' => $faker->sentence(15),
                'detalle_ubicacion' => $faker->streetAddress,
                'personas_involucradas' => $faker->numberBetween(0,7)
            ]);



            if ($sp > 2) {

                //Visto
                $timelater = $creado_en->copy()->addSeconds($faker->numberBetween(20,5*60));

                DB::table('incidencias_civiles')
                    ->where('id', $inci->id)
                    ->update(['estado_id' => EstadoIncidenciaCivil::ABIERTA]);

                DB::table("revisions")->insert([
                    'revisionable_type' => 'App\Models\IncidenciaCivil',
                    'revisionable_id' => $inci->id,
                    'user_id' => $nelsonusuario->id,
                    'key' => 'estado_id',
                    'old_value' => EstadoIncidenciaCivil::CREADA,
                    'new_value' => EstadoIncidenciaCivil::ABIERTA,
                    'created_at' => $timelater->toDateTimeString(),
                    'updated_at' => $timelater->toDateTimeString(),
                ]);


//          Asignado
                $timelater2 = $timelater->copy()->addSeconds($faker->numberBetween(20,3*60));
                DB::table('incidencias_civiles')
                    ->where('id', $inci->id)
                    ->update(['estado_id' => EstadoIncidenciaCivil::ASIGNADA]);

                DB::table("revisions")->insert([
                    'revisionable_type' => 'App\Models\IncidenciaCivil',
                    'revisionable_id' => $inci->id,
                    'user_id' => $nelsonusuario->id,
                    'key' => 'estado_id',
                    'old_value' => EstadoIncidenciaCivil::ABIERTA,
                    'new_value' => EstadoIncidenciaCivil::ASIGNADA,
                    'created_at' => $timelater2->toDateTimeString(),
                    'updated_at' => $timelater2->toDateTimeString(),
                ]);


                //Aceptado
                $timelater = $timelater2->copy()->addSeconds($faker->numberBetween(20,2*60));
                DB::table('incidencias_civiles')
                    ->where('id', $inci->id)
                    ->update(['estado_id' => EstadoIncidenciaCivil::EN_CURSO]);

                DB::table("revisions")->insert([
                    'revisionable_type' => 'App\Models\IncidenciaCivil',
                    'revisionable_id' => $inci->id,
                    'user_id' => null,
                    'key' => 'estado_id',
                    'old_value' => EstadoIncidenciaCivil::ASIGNADA,
                    'new_value' => EstadoIncidenciaCivil::EN_CURSO,
                    'created_at' => $timelater->toDateTimeString(),
                    'updated_at' => $timelater->toDateTimeString(),
                ]);


                if ($z >= 2) {
                    //Completada
                    $timelater = $timelater->copy()->addMinutes($faker->numberBetween(10,75));
                    DB::table('incidencias_civiles')
                        ->where('id', $inci->id)
                        ->update(['estado_id' => EstadoIncidenciaCivil::RESUELTA]);

                    DB::table("revisions")->insert([
                        'revisionable_type' => 'App\Models\IncidenciaCivil',
                        'revisionable_id' => $inci->id,
                        'user_id' => null,
                        'key' => 'estado_id',
                        'old_value' => EstadoIncidenciaCivil::EN_CURSO,
                        'new_value' => EstadoIncidenciaCivil::RESUELTA,
                        'created_at' => $timelater->toDateTimeString(),
                        'updated_at' => $timelater->toDateTimeString(),
                    ]);
                }

                $service->asignarUnidad($inci->id,$unidades[array_rand($unidades)]);

                $z++;
            }

            $sp++;

            unset($locations[$n]);
            array_filter($locations);
        }

    }

    private function getDataFile ($file) {
        return include __DIR__."/data/".$file.'.php';
    }
}
