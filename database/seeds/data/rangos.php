<?php
return [
    'Raso en Entrenamiento',
    'Raso Graduado',
    'Caboo',
    'Sargento',
    'Sargento Mayor',
    'Cadete',
    'Segundo Teniente',
    'Primer Teniente',
    'Capitán',
    'Mayor',
    'Teniente Coronel',
    'Coronel',
    'General de Brigada',
    'Mayor General'
];