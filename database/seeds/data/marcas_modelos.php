<?php
return [
    ['name' => 'Toyota', 'models' => ['Camry', 'Corolla', 'Hilux','Tacoma']],
    ['name' => 'Honda', 'models' => ['Civic', 'CR-V', 'Accord','Ridgeline']],
    ['name'=> 'Suzuki', 'models' => ['Equator', 'S-90', 'Samurai','Sidekick']]
];