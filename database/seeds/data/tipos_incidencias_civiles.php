<?php
return [
    'Asalto','Accidente de Tránsito', 'Robo', 'Tiroteo', 'Disputa', 'Hurto', 'Asalto simple',
    'Conducir bajo la influencia', 'Falsificación', 'Delitos contra la familia','Vagabundeo',
    'Incendio','Agresión agravada','Embriaguez pública', 'Fugitivos','Delitos sexuales','Vandalismo'
];