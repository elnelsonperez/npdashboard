<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateIncidenciasCivilesTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $set_schema_table = 'incidencias_civiles';

    /**
     * Run the migrations.
     * @table incidencias_civiles
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable($this->set_schema_table)) return;
        Schema::create($this->set_schema_table, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('creada_por')->unsigned();
            $table->integer('destacamento_id')->unsigned();
            $table->integer('sector_id')->unsigned();
            $table->integer('prioridad_id')->unsigned();
            $table->integer('estado_id')->unsigned();
            $table->integer('tipo_id')->unsigned();
            $table->dateTime('fecha_incidencia');
            $table->string('detalle_incidente');
            $table->text('detalle_ubicacion');
            $table->point('ubicacion');
            $table->string('detalle_solucion')->nullable();
            $table->string('ubicacion_texto');
            $table->string('nombre_civil', 120)->nullable();
            $table->string('telefono_civil', 20)->nullable();
            $table->integer('personas_involucradas')->unsigned()->nullable();
            $table->index(["tipo_id"], 'incidencias_civiles_tipos_incidencias');
            $table->index(["prioridad_id"], 'incidencias_civiles_prioridad_incidencia');

            $table->index(["destacamento_id"], 'incidencias_destacamentos');
            $table->index(["estado_id"]);
            $table->index(["creada_por"], 'incidencias_usuarios');

            $table->index(["sector_id"], 'incidencias_sectores');
            $table->timestamp('creado_en')->nullable();$table->timestamp('actualizado_en')->nullable();


            $table->foreign('tipo_id', 'incidencias_civiles_tipos_incidencias')
                ->references('id')->on('tipos_incidencias_civiles')
                ->onDelete('restrict')
                ->onUpdate('restrict');

            $table->foreign('estado_id', 'incidencias_civiles_estados')
                ->references('id')->on('estados_incidencias_civiles')
                ->onDelete('restrict')
                ->onUpdate('restrict');

            $table->foreign('prioridad_id', 'incidencias_civiles_prioridades_incidencias')
                ->references('id')->on('prioridades_incidencias_civiles')
                ->onDelete('restrict')
                ->onUpdate('restrict');

            $table->foreign('destacamento_id', 'incidencias_destacamentos')
                ->references('id')->on('destacamentos')
                ->onDelete('restrict')
                ->onUpdate('restrict');

            $table->foreign('sector_id', 'incidencias_sectores')
                ->references('id')->on('sectores')
                ->onDelete('restrict')
                ->onUpdate('restrict');

            $table->foreign('creada_por', 'incidencias_usuarios')
                ->references('id')->on('usuarios')
                ->onDelete('restrict')
                ->onUpdate('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->set_schema_table);
     }
}
