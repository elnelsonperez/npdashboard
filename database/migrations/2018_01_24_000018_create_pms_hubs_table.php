<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePmsHubsTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $set_schema_table = 'pms_hubs';

    /**
     * Run the migrations.
     * @table pms_hubs
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable($this->set_schema_table)) return;
        Schema::create($this->set_schema_table, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('unidad_id')->unsigned()->nullable();
            $table->string('serial', 40)->comment('Pi serial number');

            $table->index(["unidad_id"], 'pms_hubs_vehiculos');

            $table->unique(["serial"], 'pms_hubs_ak_1');
            $table->timestamp('creado_en')->nullable();$table->timestamp('actualizado_en')->nullable();


            $table->foreign('unidad_id', 'pms_hubs_vehiculos')
                ->references('id')->on('unidades')
                ->onDelete('restrict')
                ->onUpdate('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->set_schema_table);
     }
}
