<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHubDataPendiente extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('hubs_data_pendiente', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedInteger('pms_hub_id');
            $table->unique('pms_hub_id');
            $table->boolean('configs')->default(0);
            $table->timestamp("configs_ultimo_pull")->nullable();
            $table->foreign('pms_hub_id')
                ->references('id')->on('pms_hubs');
        });
    }

    /**
     * Reverse the migrations.cd
     * cd
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('hubs_data_pendiente');
    }
}
