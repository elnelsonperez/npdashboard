<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSectoresTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $set_schema_table = 'sectores';

    /**
     * Run the migrations.
     * @table sectores
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable($this->set_schema_table)) return;
        Schema::create($this->set_schema_table, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('municipio_id')->unsigned();
            $table->integer('zona_id')->unsigned()->nullable()->default(null);
            $table->string('nombre', 60);
            $table->polygon('limites')->nullable()->default(null);

            $table->index(["municipio_id"], 'sectores_municipios');

            $table->index(["zona_id"], 'sectores_zonas');


            $table->foreign('municipio_id', 'sectores_municipios')
                ->references('id')->on('municipios')
                ->onDelete('restrict')
                ->onUpdate('restrict');

            $table->foreign('zona_id', 'sectores_zonas')
                ->references('id')->on('zonas')
                ->onDelete('restrict')
                ->onUpdate('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->set_schema_table);
     }
}
