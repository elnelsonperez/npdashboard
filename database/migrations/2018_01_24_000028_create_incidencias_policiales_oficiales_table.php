<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateIncidenciasPolicialesOficialesTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $set_schema_table = 'incidencias_policiales_oficiales';

    /**
     * Run the migrations.
     * @table incidencias_policiales_oficiales
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable($this->set_schema_table)) return;
        Schema::create($this->set_schema_table, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('incidencia_policial_id')->unsigned();
            $table->integer('oficial_id')->unsigned();
            $table->timestamp('creado_en')->nullable();$table->timestamp('actualizado_en')->nullable();
            $table->index(["incidencia_policial_id"], 'incidencias_policiales_oficiales_incidencias_policiales');
            $table->index(["oficial_id"], 'incidencias_policiales_oficiales_oficiales');

            $table->foreign('incidencia_policial_id', 'incidencias_policiales_oficiales_incidencias_policiales')
                ->references('id')->on('incidencias_policiales')
                ->onDelete('restrict')
                ->onUpdate('restrict');

            $table->foreign('oficial_id', 'incidencias_policiales_oficiales_oficiales')
                ->references('id')->on('oficiales')
                ->onDelete('restrict')
                ->onUpdate('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->set_schema_table);
     }
}
