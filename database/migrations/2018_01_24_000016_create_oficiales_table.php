<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOficialesTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $set_schema_table = 'oficiales';

    /**
     * Run the migrations.
     * @table oficiales
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable($this->set_schema_table)) return;
        Schema::create($this->set_schema_table, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('rango_id')->unsigned();
            $table->integer('destacamento_id')->unsigned();
            $table->integer('unidad_id')->unsigned()->nullable();
            $table->integer('tipo_oficial_id')->unsigned()->nullable();
            $table->string('nombre', 65);
            $table->string('apellido', 65);
            $table->text('foto')->nullable();
            $table->date('fecha_nacimiento');
            $table->date('fecha_ingreso');
            $table->integer('sexo')->comment("0 M, 1 F");
            $table->string('cedula', 14);

            $table->index(["destacamento_id"], 'personal_destacamentos');
            $table->index(["rango_id"], 'personal_rangos');
            $table->unique(["cedula"], 'oficiales_ak_1');
            $table->timestamp('creado_en')->nullable();$table->timestamp('actualizado_en')->nullable();

            $table->foreign('destacamento_id', 'personal_destacamentos')
                ->references('id')->on('destacamentos')
                ->onDelete('restrict')
                ->onUpdate('restrict');

            $table->foreign('unidad_id')
                ->references('id')->on('unidades')
                ->onDelete('restrict')
                ->onUpdate('restrict');

            $table->foreign('tipo_oficial_id' )
                ->references('id')->on('tipos_oficiales')
                ->onDelete('restrict')
                ->onUpdate('restrict');


            $table->foreign('rango_id', 'personal_rangos')
                ->references('id')->on('rangos')
                ->onDelete('restrict')
                ->onUpdate('restrict');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->set_schema_table);
     }
}
