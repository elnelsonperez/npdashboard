<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateIncidenciasPolicialesUnidadesTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $set_schema_table = 'incidencias_policiales_unidades';

    /**
     * Run the migrations.
     * @table incidencias_policiales_unidades
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable($this->set_schema_table)) return;
        Schema::create($this->set_schema_table, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('incidencia_policial_id')->unsigned();
            $table->integer('unidad_id')->unsigned();

            $table->index(["unidad_id"], 'incidencias_policiales_unidades_unidades');

            $table->index(["incidencia_policial_id"], 'incidencias_policiales_unidades_incidencias_policiales');


            $table->foreign('incidencia_policial_id', 'incidencias_policiales_unidades_incidencias_policiales')
                ->references('id')->on('incidencias_policiales')
                ->onDelete('restrict')
                ->onUpdate('restrict');

            $table->foreign('unidad_id', 'incidencias_policiales_unidades_unidades')
                ->references('id')->on('unidades')
                ->onDelete('restrict')
                ->onUpdate('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->set_schema_table);
     }
}
