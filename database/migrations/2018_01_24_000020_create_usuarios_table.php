<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsuariosTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $set_schema_table = 'usuarios';

    /**
     * Run the migrations.
     * @table usuarios
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable($this->set_schema_table)) return;
        Schema::create($this->set_schema_table, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('oficial_id')->unsigned();
            $table->string('email');
            $table->string('password');
            $table->rememberToken();
            $table->integer('tipo');
            $table->index(["oficial_id"], 'usuarios_personal');
            $table->timestamp('creado_en')->nullable();$table->timestamp('actualizado_en')->nullable();
            $table->foreign('oficial_id', 'usuarios_personal')
                ->references('id')->on('oficiales')
                ->onDelete('restrict')
                ->onUpdate('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->set_schema_table);
     }
}
