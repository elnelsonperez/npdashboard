<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMensajesHubsTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $set_schema_table = 'mensajes_hubs';

    /**
     * Run the migrations.
     * @table recado
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable($this->set_schema_table)) return;
        Schema::create($this->set_schema_table, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('oficial_unidad_id')->unsigned()->nullable();
            $table->integer('usuario_id')->unsigned()->nullable();
            $table->integer('estado_mensaje_hub_id')->unsigned();
            $table->integer('pms_hub_id')->unsigned();
            $table->text("contenido");
            $table->boolean('sentido');
            $table->timestamp('creado_en')->nullable();
            $table->index('creado_en');
            $table->index('oficial_unidad_id');
            $table->timestamp('actualizado_en')->nullable();

            $table->foreign('oficial_unidad_id')
                ->references('id')->on('oficiales')
                ->onDelete('restrict')
                ->onUpdate('restrict');

            $table->foreign('usuario_id')
                ->references('id')->on('usuarios')
                ->onDelete('restrict')
                ->onUpdate('restrict');

            $table->foreign('pms_hub_id')
                ->references('id')->on('pms_hubs')
                ->onDelete('restrict')
                ->onUpdate('restrict');

            $table->foreign('estado_mensaje_hub_id')
                ->references('id')->on('estados_mensajes_hubs')
                ->onDelete('restrict')
                ->onUpdate('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->set_schema_table);
     }
}
