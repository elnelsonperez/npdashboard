<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUnidadesIncidenciasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('unidades_incidencias')) return;
        Schema::create('unidades_incidencias', function (Blueprint $table) {
            $table->unsignedInteger('unidad_id');
            $table->unsignedInteger('incidencia_id');
            $table->primary(['unidad_id','incidencia_id'],'id');
            $table->timestamp('creado_en')->nullable();
            $table->index('creado_en');
            $table->timestamp('actualizado_en')->nullable();
            $table->index('unidad_id');
            $table->index('incidencia_id');
            $table->foreign('incidencia_id', 'incidencia_unidades_incidencias')
                ->references('id')->on('incidencias_civiles')
                ->onDelete('restrict')
                ->onUpdate('restrict');

            $table->foreign('unidad_id', 'unidad_unidades_incidencias')
                ->references('id')->on('unidades')
                ->onDelete('restrict')
                ->onUpdate('restrict');
        });


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('unidades_incidencias');
    }
}
