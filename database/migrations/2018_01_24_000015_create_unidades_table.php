<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUnidadesTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $set_schema_table = 'unidades';

    /**
     * Run the migrations.
     * @table unidades
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable($this->set_schema_table)) return;
        Schema::create($this->set_schema_table, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('modelo_id')->unsigned();
            $table->integer('estado_unidad_id')->unsigned()->default(1);
            $table->integer('sector_id')->unsigned()->nullable();
            $table->integer('destacamento_id')->unsigned();
            $table->integer('ano');
            $table->integer('tipo')->comment('0 para vehiculos de 4 gomas. 1 de dos');
            $table->string('placa', 20);
            $table->dateTime('last_update_time')->default(DB::raw('CURRENT_TIMESTAMP'));

            $table->index(["modelo_id"], 'vehiculos_modelos');
            $table->index(["last_update_time"]);

            $table->index(["destacamento_id"], 'vehiculos_destacamentos');

            $table->index(["estado_unidad_id"], 'vehiculos_estados_vehiculos');
            $table->timestamp('creado_en')->nullable();
            $table->timestamp('actualizado_en')->nullable();

            $table->foreign('sector_id', 'unidad_sector')
                ->references('id')->on('sectores')
                ->onDelete('restrict')
                ->onUpdate('restrict');

            $table->foreign('destacamento_id', 'vehiculos_destacamentos')
                ->references('id')->on('destacamentos')
                ->onDelete('restrict')
                ->onUpdate('restrict');

            $table->foreign('estado_unidad_id', 'vehiculos_estados_vehiculos')
                ->references('id')->on('estados_unidades')
                ->onDelete('restrict')
                ->onUpdate('restrict');

            $table->foreign('modelo_id', 'vehiculos_modelos')
                ->references('id')->on('modelos')
                ->onDelete('restrict')
                ->onUpdate('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->set_schema_table);
     }
}
