<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDestacamentosTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $set_schema_table = 'destacamentos';

    /**
     * Run the migrations.
     * @table destacamentos
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable($this->set_schema_table)) return;
        Schema::create($this->set_schema_table, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('zona_id')->unsigned();;
            $table->string('nombre', 50);
            $table->string('direccion');
            $table->point('ubicacion');
            $table->string('telefono', 10);
            $table->index(["zona_id"], 'destacamentos_zonas');
            $table->timestamp('creado_en')->nullable();$table->timestamp('actualizado_en')->nullable();
            $table->foreign('zona_id', 'destacamentos_zonas')
                ->references('id')->on('zonas')
                ->onDelete('restrict')
                ->onUpdate('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->set_schema_table);
     }
}
