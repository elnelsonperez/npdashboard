<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateIncidenciasPolicialesTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $set_schema_table = 'incidencias_policiales';

    /**
     * Run the migrations.
     * @table incidencias_policiales
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable($this->set_schema_table)) return;
        Schema::create($this->set_schema_table, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('creada_por')->unsigned();
            $table->integer('destacamento_id')->unsigned();
            $table->point('ubicacion');
            $table->text('detalle');
            $table->text('detalle_solucion');
            $table->dateTime('fecha_incidencia');
            $table->integer('tipo_id')->unsigned();

            $table->index(["tipo_id"], 'incidencias_policiales_tipos_incidencias_policiales');

            $table->index(["destacamento_id"], 'incidencias_policiales_destacamentos');

            $table->index(["creada_por"], 'incidencias_policiales_usuarios');
            $table->timestamp('creado_en')->nullable();$table->timestamp('actualizado_en')->nullable();


            $table->foreign('destacamento_id', 'incidencias_policiales_destacamentos')
                ->references('id')->on('destacamentos')
                ->onDelete('restrict')
                ->onUpdate('restrict');

            $table->foreign('tipo_id', 'incidencias_policiales_tipos_incidencias_policiales')
                ->references('id')->on('tipos_incidencias_policiales')
                ->onDelete('restrict')
                ->onUpdate('restrict');

            $table->foreign('creada_por', 'incidencias_policiales_usuarios')
                ->references('id')->on('usuarios')
                ->onDelete('restrict')
                ->onUpdate('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->set_schema_table);
     }
}
