<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFlotasTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $set_schema_table = 'flotas';

    /**
     * Run the migrations.
     * @table flotas
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable($this->set_schema_table)) return;
        Schema::create($this->set_schema_table, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('oficial_id')->unsigned();
            $table->string('digitos', 13);
            $table->string('mac_address', 20)->nullable();

            $table->index(["oficial_id"], 'flotas_oficiales');
            $table->timestamp('creado_en')->nullable();$table->timestamp('actualizado_en')->nullable();


            $table->foreign('oficial_id', 'flotas_oficiales')
                ->references('id')->on('oficiales')
                ->onDelete('restrict')
                ->onUpdate('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->set_schema_table);
     }
}
