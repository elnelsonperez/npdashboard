<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePmsHubLocalizacionesTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $set_schema_table = 'pms_hubs_localizaciones';

    /**
     * Run the migrations.
     * @table pms_hub_localizaciones
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable($this->set_schema_table)) return;
        Schema::create($this->set_schema_table, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('pms_hub_id')->unsigned();
            $table->dateTime('fecha_obtenida');
            $table->point('ubicacion');

            $table->index(["pms_hub_id"], 'pms_hub_location_pms_hubs');
            $table->timestamp('creado_en');
            $table->foreign('pms_hub_id', 'pms_hub_location_pms_hubs')
                ->references('id')->on('pms_hubs')
                ->onDelete('restrict')
                ->onUpdate('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->set_schema_table);
     }
}
