<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateIbuttonsTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $set_schema_table = 'ibuttons';

    /**
     * Run the migrations.
     * @table ibuttons
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable($this->set_schema_table)) return;
        Schema::create($this->set_schema_table, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('oficial_id')->unsigned();
            $table->string('code',20);

            $table->index(["oficial_id"], 'ibuttons_oficiales');
            $table->timestamp('creado_en')->nullable();
            $table->timestamp('actualizado_en')->nullable();


            $table->foreign('oficial_id', 'ibuttons_oficiales')
                ->references('id')->on('oficiales')
                ->onDelete('restrict')
                ->onUpdate('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->set_schema_table);
     }
}
