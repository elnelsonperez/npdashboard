## NP PMS Dashboard/Server Software
Interfaz web del sistema NP PMS.
Desarrollado por Nelson P�rez y Nathaly Persia como proyecto de grado. 2017-2018.

Para solicitar documentacion adicional sobre algo, enviame un email a 
me@nelsonperez.net

La aplicacion, en el backend, esta desarrollada en PHP, especificamente con el framework Laravel.
En el frontend, se utiliza VueJS.

##### Recomendaciones previas
La mayoria del codigo del backend se explica solo si entiendes Laravel.
Es muy importante leer los siguientes documentos y entender los conceptos detras de ellos, asi que recomendamos
buscar recursos en youtube y paginas similares. No te limites a los links provistos.

- Conceptos de [MVC](https://medium.com/@busbyactual/what-is-mvc-dd7032444e2e) y leer toda la documentacion de [Laravel 5.5](https://laravel.com/docs/5.5/).
- [Documentacion de Vuejs](https://vuejs.org/)
- [Highlights of Vuejs](https://medium.com/front-end-hacking/highlights-of-vue-js-aa37d813064d)
- [Vuex](https://vuex.vuejs.org/en/intro.html)
- [Understanding Vuex](https://medium.com/js-dojo/vuex-for-the-clueless-the-missing-primer-on-vues-application-data-store-33fa51ffc3af)
- [Normalizr](https://github.com/paularmstrong/normalizr)
- [What are Websockets?](https://medium.com/front-end-hacking/what-are-websockets-7bf0e2e1af2)
- [Websockets con PusherJS](https://pusher-community.github.io/real-time-laravel/introduction/what-is-pusher.html)

Antes de tocar este proeycto, es recomendable practicar y jugar tanto con Laravel y Vue para entender sus conceptos.
Si se entienden ambas de estas librerias, modificar el proyecto se vuelve pan comido. Por esta razon, hacemos mucho
enfasis en aprender a usar las librerias y ver como funcionan.

##### Notas
El backend utiliza las mismas pautas que son dadas por Laravel en su documentacion.
Osea, que las Views y Controllers estan en el mismo directorio que en un proyecto nuevo de Laravel. 
Los Models se encuentran en `app/Models`.

Es **indispensable** entender Laravel para modificar el backend.

Si necesitas saber como algo funciona, solo basta con seguir los archivos 'routes/web.php' o 'routes/api.php' para 
saber a que controlador lleva cada ruta, luego a `app/Http/Controllers` para ver el controlador de cualquiera de las rutas definidas,
y a `resources/views` si quieres modificar o ver las views.

Las migrations de laravel te permiten crear las tablas en la base de datos configuradas en el [.env](https://laravel.com/docs/5.5/configuration)
con un solo comando: `php artisan migrate`, o borrar todo y volver a crear las tablas con `php artisan migrate:fresh`.

Con el comando `php artisan db:seed` puedes llenar las tablas generadas con informacion fabricada. Los archivos que
realizan esta funcion se encuentran en `app/database/seeds`. La gente de Laravel da una buena explicacion del
 [seeding](https://laravel.com/docs/5.5/seeding) en su documentacion.

##### Sobre Vue.Js
La pantalla mas importante del dashboard, el control de patrullas, esta hecho con la libreria de javascript Vue.js.
Es escencial entender al menos lo basico de esta libreria para poder modificar algo en la pagina.
Los archivos relacionados a Vue estan ubicados en el directorio `resources/assets/js`.

El directorio `resources/assets/js/components` contiene todos los componentes de Vue que son utilizados en el proyecto.
El archivo `app.js` es la puerta de entrada a la aplicacion de Javascript. Utilizando [Laravel Mix](https://laravel.com/docs/5.5/mix)
y el archivo `webpack.mix.js`, se 'compila' el `app.js` y se publica al directorio `public/js`.

La aplicacion de Vue se programa utilizando Javascript ES6, que es una version mas nueva que el estandar que utilizan
los navegadores, ES5. Por lo que, Laravel, con ayuda de Webpack y Babel, convierte el Javascript ES6 de 
`resources/assets/js/app.js` a Javascript ES5, y el resultado lo pone en `public/js`. Si aun no entiendes este meneo,
te invito a leer el siguiente [articulo.](https://scotch.io/tutorials/javascript-transpilers-what-they-are-why-we-need-them)

**El archivo** `public/js/` **no se edita directamente.** Lo que se hace es editar los source files del directorio
`resources/assets/js` y correr Laravel Mix para que genere el `public/js/app.js`.

Si te fijas en `resources/views/dashboards/controlPatrullas.blade.php`, veras que en la seccion scripts se carga
el app.js:

```html
    <script src="{{asset("js/app.js")}}" type="text/javascript"></script>
```
Tener en cuenta que los dobles corchetes `{{ }}` son asunto de [Blade](https://laravel.com/docs/5.5/blade),
y la funcion '[asset](https://laravel.com/docs/5.5/helpers#method-asset)' es una funcion helper de laravel para
 generar rutas relativas a la carpeta publica. 

###### Vuex
Si ya has revisado los links provistos al inicio de la documentacion, tendras una idea de que es Vuex y para que se usa.
En el directorio `assets/js/store` se encuentra todo lo relacinado a Vuex, con los actions, getters, y mutations separados 
en archivos distintos para mayor claridad.

La data almacenada en Vuex se normaliza con [normalizr](https://hackernoon.com/using-normalizr-to-organize-data-in-stores-practical-guide-82fa061b60fb) 
antes de ser guardada. Esto es para que los updates sean mas sencillos y la data no se repita una y otra vez
en el state de la aplicacion.

Los getters de Vuex, en `resources/assets/js/store/getters.js`, utilizan la funcion [denormalize](https://github.com/paularmstrong/normalizr/blob/master/docs/api.md#denormalizeinput-schema-entities) 
de normalizr para 'denormalizar' la data y que sea usable en los componentes de la aplicacion.

###### Librerias usadas en conjunto con Vue y Vuex
Si te encuentras con algun `import` de algun componente externo, por ejemplo

```javascript
  import {Marker} from 'vue2-google-maps';
```
Significa que el componente pertenece a la libreria vue2-google-maps, y que su documentacion respondera a cualquier 
pregunta que surja sobre el componente.

Los links a la documentacion de cada libreria importante extra utilizada se detallan a continuacion:

* [axios](https://github.com/axios/axios)
* [dateformat](https://www.npmjs.com/package/dateformat)
* [normalizr](https://github.com/paularmstrong/normalizr)
* [vue-perfect-scrollbar](https://github.com/Degfy/vue-perfect-scrollbar)
* [turf](http://turfjs.org/)
* [vue-carousel](https://ssense.github.io/vue-carousel/)
* [vue-fab](https://github.com/PygmySlowLoris/vue-fab)
* [vue-router](https://router.vuejs.org/en/)
* [vue-spinner](https://github.com/greyby/vue-spinner)
* [vue-sweetalert2](https://www.npmjs.com/package/vue-sweetalert2)
* [vue-toastr](https://www.npmjs.com/package/vue-toastr)
* [vue2-bootstrap-modal](https://www.npmjs.com/package/vue2-bootstrap-modal)
* [vue2-google-maps](https://github.com/xkjyeah/vue-google-maps)
* [vuex](https://vuex.vuejs.org/en/)

