<?php
require_once "auth.php";

Route::middleware(['auth'])->group(function () {

    Route::get("incidencias_civiles", "DashboardController@indexDashboardRegistroIncidencias")
        ->name('registro_incidencias');

    Route::get("controlpatrullas", "DashboardController@indexDashboardPatrullas")
        ->name('control_patrullas');

    Route::post('incidencias_civiles', "IncidenciaCivilController@store")
        ->name('incidencias_civiles.store');

//    Route::get("incidenciaspoliciales", "DashboardController@indexIncidenciasPoliciales")
//        ->name('incidencias_policiales');


    Route::get('/', function () {
        return view ('home');
    });

    Route::get('api/unidades_fuera_de_rango', 'UnidadController@obtenerUnidadesFueraDeSector');
    Route::get('api/unidades_para_incidente', 'UnidadController@unidadesParaIncidente');
    Route::get('api/sectores', 'SectorController@getSectores');
    Route::get('api/modelos', 'ModeloController@getModelos');
    Route::post('api/unidad_incidente', 'IncidenciaCivilController@asignarUnidad');
    Route::post('api/mensajes', 'MensajeController@store');
    Route::post('api/locations_snapped', 'HubLocalizacionController@getEvenlySpacedLocationsSnapped');
    Route::post('api/mensajes_leidos', 'MensajeController@marcarComoLeido');

    Route::post('api/incidencias_civiles_estado', 'IncidenciaCivilController@changeStatus');
    Route::get('api/incidencias_civiles/{id}', 'IncidenciaCivilController@getIncidencia');

    Route::resource('oficiales', 'OficialController');
    Route::resource('unidades', 'UnidadController');

    Route::get('incidencias', 'IncidenciaCivilController@index');

    Route::get('incidencias/{id}', 'IncidenciaCivilController@show');

    Route::get('configurar_hubs', 'HubConfigController@index')->name('hub_config');
    Route::post('configurar_hubs', 'HubConfigController@store')->name('hub_config.store');

    Route::prefix('reportes')->group( function () {
        Route::get('crimen', 'ReporteCrimenController@index')->name('reportes.crimen');
    });

});

Route::get("test", function () {
    $uni = \App\Models\Unidad::find(1);
    $uni->last_update_time = \Carbon\Carbon::now()->subMinutes(5);
    $uni->save();
    broadcast(new \App\Events\UnidadLastUpdate($uni,
        \App\Models\Destacamento::find(1)->id));
});

Route::get("formtest", function () {
    return view('formtest',[
            'breadCrumbs' =>
                [
                    ['text' => 'Recurso', 'link' => '/recurso'],
                    ['text' => 'Crear', 'link' => '/crear','active' => true,]
                ]
        ]
    );
});