<?php

Route::post('hub_localizaciones', 'HubLocalizacionController@store');
Route::post('hub_mensajes', 'MensajeController@apiCrearMensaje');
Route::post('hub_get_data', 'DataPullingController@getHubData');
Route::post('hub_get_mensajes', 'MensajeController@apiGetMensajes');
Route::post('hub_get_incidencias', 'IncidenciaCivilController@apiGetIncidencias');
Route::post('hub_estado_incidencias', 'IncidenciaCivilController@apiChangeStatus');
Route::post('hub_estado_mensajes', 'MensajeController@apiChangeStatus');
Route::post('hub_config', 'HubConfigController@getHubConfig');
Route::post('hub_stats', 'SectorController@apiGetSectorStats');

Route::get('status', function () {
    return 'ACTIVO';
});
