import { schema } from 'normalizr';
const marca = new schema.Entity('marca')
const municipio = new schema.Entity('municipio')
const sector = new schema.Entity('sector',{
  municipio
})
const sectores = [sector]
const oficial = new schema.Entity('oficial')
const hub = new schema.Entity('hub')
const localizacion = new schema.Entity('localizacion')
const localizaciones = [localizacion]
const oficiales = [oficial]
const tipo_incidente_civil = new schema.Entity('tipo')

const modelo = new schema.Entity('modelo', {
  marca,
  sector
});

const unidad = new schema.Entity('unidad', {
  modelo,
  oficiales,
  hub,
  supervisor: oficial,
  sector,
  localizaciones
})

const unidades = [unidad]

const incidente = new schema.Entity('incidente', {
  tipo: tipo_incidente_civil,
  sector,
  unidades
})

const incidentes = [incidente]

const usuario = new schema.Entity('usuario')

const mensaje = new schema.Entity('mensaje', {
  oficial_unidad: oficial,
  remitente: oficial,
  hub
})

const mensajes = [mensaje]
const tipos = [tipo_incidente_civil]

const baseDataSchema = new schema.Object({
  unidades: unidades,
  sectores: sectores,
  incidentes: incidentes,
  mensajes: mensajes,
  tipos: tipos,
});

export {marca, sector, usuario, sectores, modelo,unidad,localizacion,incidentes,mensaje,mensajes,unidades,baseDataSchema,
  incidente,tipo_incidente_civil,oficial,hub,oficiales}