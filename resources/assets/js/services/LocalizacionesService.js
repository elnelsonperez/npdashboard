import axios from './../lib/axios'

export default class LocalizacionesService {

  constructor(options) {
    this.locs = axios.create({
      baseURL: '/api'
    });
  }

  async getLocationHistory (unidad_id, metros, limite) {
    const response = await this.locs.post("locations_snapped", {unidad_id, metros, limite})
    if (response.status === 200) {
      return response.data
    }
    error = new Error(response.status);
    error.data = response.data
    throw error;
  }

}