import axios from './../lib/axios'

export default class IncidenteService {

  constructor(options) {
    this.incidencias = axios.create({
      baseURL: '/api'
    });
  }

  async changeStatus ({incidencia_id, estado_id}) {
    const response = await this.incidencias.post("incidencias_civiles_estado", {incidencia_id, estado_id})
    if (response.status === 200) {
      return response.data
    }
    return null;
  }

  async fetchIncidencias () {
    const response = await this.incidencias.get("incidencias_civiles")
    if (response.status === 200) {
      return response.data
    }
    return null;
  }

  async fetchIncidencia (id) {
    const response = await this.incidencias.get("incidencias_civiles/"+id)
    if (response.status === 200) {
      return response.data
    }
    return null;
  }

}