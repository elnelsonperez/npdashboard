import axios from './../lib/axios'

export default class MensajeService {

  constructor(options) {
    this.unidades = axios.create({
      baseURL: '/api'
    });
  }

  async marcarMensajesComoLeidos (mensajes) {
    const response = await this.unidades.post("mensajes_leidos", {mensajes})
    if (response.status === 200) {
      return response.data
    }
    error = new Error(response.status);
    error.data = response.data
  }

  async enviarMensaje (oficial_unidad_id, contenido) {
    const response = await this.unidades.post("mensajes", {oficial_unidad_id,contenido})
    if (response.status === 200) {
      return response.data
    }
    error = new Error(response.status);
    error.data = response.data
  }

}