import {addressByFormat, address_component_from_google_geocode} from './../utils/helpers'

export default class GeocodingService {

  getLocationText (point,pointid) {
    return new Promise(res => {
      let timeoutCounter = 0;
      let cached = sessionStorage.getItem('geo-'+pointid);
      if (cached) {
        res(cached);
      } else {
        if (typeof google !== 'undefined') {
          if (!this.geocoder) {
            this.geocoder = new google.maps.Geocoder;
          }
          this.geocoder.geocode({'location': point}, (results, status)  => {
            const make = () => {
              if (status === "OVER_QUERY_LIMIT") {
                if (timeoutCounter < 5) {
                  setTimeout(make,300*timeoutCounter++)
                } else {
                  return null;
                }
              } else {
                const addr = addressByFormat(results,'sublocality');
                const components = address_component_from_google_geocode(addr.address_components)
                let result = "";
                if (components.addressLine1 && components.addressLine1 !== "") {
                  result = components.addressLine1;
                } else if (components.district && components.district !== "") {
                  result = components.district;
                } else {
                  result = components.stateOrProvince
                }
                sessionStorage.setItem('geo-'+pointid,result)
                res(result)
              }
            }
            make();
          });
        }
        else
          res(null)


      }


    })


  }

}