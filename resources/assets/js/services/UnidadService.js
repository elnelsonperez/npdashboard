import axios from './../lib/axios'

export default class UnidadService {

  constructor(options) {
    this.unidades = axios.create({
      baseURL: '/api'
    });
  }

  async fetchUnidadesFueraDeRango () {
    const response = await this.unidades.get("unidades_fuera_de_rango")
    if (response.status === 200) {
      return response.data
    }
    error = new Error(response.status);
    error.data = response.data
  }

  async fetch (id) {
    const response = await this.unidades.get("unidades_para_incidente",{params: {incidente_id: id}})
    if (response.status === 200) {
      return response.data
    }
    error = new Error(response.status);
    error.data = response.data
  }

  async fetchUnidadesParaIncidencia (id) {
    const response = await this.unidades.get("unidades_para_incidente",{params: {incidente_id: id}})
    if (response.status === 200) {
      return response.data
    }
    error = new Error(response.status);
    error.data = response.data
  }

  async asignarUnidad (unidad_id, incidencia_id) {
    const response = await this.unidades.post("unidad_incidente",
        {incidencia_id: incidencia_id, unidad_id: unidad_id})
    if (response.status === 200) {
      return response.data
    }
    error = new Error(response.status);
    error.data = response.data
  }

}