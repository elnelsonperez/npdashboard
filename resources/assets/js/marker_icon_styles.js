import mapIcons from './lib/map-icons'

const unidadIcon = {
  path: mapIcons.shapes.SHIELD,
  fillColor: '#1856a6',
  fillOpacity: .7,
  labelOrigin: {x: 0, y: -25},
  strokeWeight: 2,
  strokeColor: '#174a93',
  strokeOpacity: 0.8,
  scale: 0.6
}

const incidenteIcon = {
  path: mapIcons.shapes.MAP_PIN,
  fillColor: '#a10001',
  fillOpacity: .7,
  strokeWeight: 2,
  strokeColor: '#850001',
  strokeOpacity: .8,
  scale: 0.6
}

const colors = {
  asignadoFill: "#e1ab10",
  asignadoStroke: "#b98f10",
  enCursoFill: '#42841c',
  enCursoStroke :"#2f5f11",
  noAsignadoFill : '#a10001',
  noAsignadoStroke : '#850001',
  unidadFill: '#1856a6',
  unidadStroke: '#174a93'
}


export {unidadIcon,incidenteIcon,colors}