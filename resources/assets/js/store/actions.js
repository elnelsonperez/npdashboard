import * as types from './mutation_types'
import UnidadService from './../services/UnidadService'
import MensajeService from './../services/MensajeService'
import IncidenteService from "../services/IncidenteService";

const marcarMensajesComoLeidos =  ({commit}, {mensajes}) => {
  const service = new MensajeService();
  service.marcarMensajesComoLeidos(mensajes).then(r => {
    if (r) {
      for (let m of r) {
        commit(types.SET_MENSAJE, m)
      }
      return true;
    }
    else
      return false;
  }).catch(e => {
    return e;
  })
}

const startTime = ({commit}) => {
  setInterval(() => {
    commit(types.UPDATE_TIME)
  }, 1000)
}

const enviarMensaje = ({commit}, {mensaje, oficial_id}) => {
  const service = new MensajeService();
  service.enviarMensaje(oficial_id, mensaje).then(r => {
    if (r) {
      commit(types.SET_MENSAJE, r)
      return true;
    }
    else
      return false;
  }).catch(e => {
    return e;
  })
}

const changeIncidenciaStatus = ({commit}, {incidencia_id, estado_id}) => {
  const service = new IncidenteService();
  service.changeStatus({incidencia_id, estado_id}).then(r => {
    if (r) {
      commit(types.SET_INCIDENTE, r)
      return true;
    }
    else
      return false;
  }).catch(e => {
    return e;
  })
}

const fetchUnidadesFuera = ({ commit }) => {
  const service = new UnidadService();
  service.fetchUnidadesFueraDeRango().then(r => {
    if (r) {
      commit(types.SET_UNIDADES_FUERA_RANGO, r)
      return true;
    }
    else
      return false;
  }).catch(e => {
    return e;
  })
};

const setLocalizaciones  = ({ commit }, localizaciones) => {
  for (let l of localizaciones) {
    commit(types.SET_LOCALIZACION, l)
    commit(types.ADD_LOCALIZACION_TO_UNIDAD, l)
  }
}

const setMessage = ({ commit }, message) => {
    commit(types.SET_MENSAJE, message)
}

const setIncidencia = ({ commit }, incidencia) => {
    commit(types.SET_INCIDENTE, incidencia)
}

const setUnidad = ({commit }, unidad) => {
  commit(types.SET_UNIDAD, unidad)
}

const setIncidenteEstado = ({ commit }, payload) => {
  commit(types.SET_INCIDENTE_ESTADO, payload)
}

const setUnidadLastUpdateTime = ({ commit }, payload) => {
  commit(types.SET_UNIDAD_LAST_UPDATE_TIME, payload)
}

const addUnidadAIncidente = ({commit}, payload) => {
  commit(types.ADD_UNIDAD_A_INCIDENTE, payload)
}

export {setUnidadLastUpdateTime,fetchUnidadesFuera,changeIncidenciaStatus,setIncidenteEstado,setLocalizaciones,setUnidad, startTime, addUnidadAIncidente,setMessage,enviarMensaje,setIncidencia,marcarMensajesComoLeidos}