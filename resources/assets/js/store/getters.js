
import {denormalize} from 'normalizr';
import {baseDataSchema} from './../schemas'

function mensajesNoLeidos(state) {
  const mensajes = denormalize (
      {mensajes: state.normalized.result.mensajes},
      baseDataSchema,
      state.normalized.entities
  ).mensajes;
  return mensajes.filter(v => {
    return v.oficial_unidad.tipo.id === 2 && v.estado.id !== 2 && v.sentido === 1
  })
}

function contactos (state) {
  const unidades = denormalize (
      {unidades: state.normalized.result.unidades},
      baseDataSchema,
      state.normalized.entities
  ).unidades;

  const contactos = {};

  for (let u of unidades) {
    if (u.supervisor) {
      contactos[u.id] = u.supervisor
    }
  }

  return contactos;
}

function unidades (state) {
  return denormalize (
      {unidades: state.normalized.result.unidades},
      baseDataSchema,
      state.normalized.entities
  ).unidades;
}

function incidentes (state) {
  return denormalize (
      {incidentes: state.normalized.result.incidentes},
      baseDataSchema,
      state.normalized.entities
  ).incidentes;
}

function sectores (state) {
  return denormalize (
      {sectores: state.normalized.result.sectores},
      baseDataSchema,
      state.normalized.entities
  ).sectores;
}

function mensajes(state) {
  return denormalize (
      {mensajes: state.normalized.result.mensajes},
      baseDataSchema,
      state.normalized.entities
  ).mensajes;
}


export {unidades, incidentes,sectores,contactos,mensajesNoLeidos,mensajes}