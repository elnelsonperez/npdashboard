
const SET_UNIDADES_FUERA_RANGO = "SET_UNIDADES_FUERA_RANGO"
const ADD_UNIDAD_A_INCIDENTE = "ADD_UNIDAD_A_INCIDENTE"
const SET_INCIDENTE = "SET_INCIDENTE"
const SET_UNIDAD = "SET_UNIDAD"
const SET_MENSAJE = "SET_MENSAJE"
const SET_LOCALIZACION = "SET_LOCALIZACION"
const UPDATE_TIME = "UPDATE_TIME"
const SET_INCIDENTE_ESTADO = "SET_INCIDENTE_ESTADO"
const ADD_LOCALIZACION_TO_UNIDAD = "ADD_LOCALIZACION_TO_UNIDAD"

const SET_UNIDAD_LAST_UPDATE_TIME = "SET_UNIDAD_LAST_UPDATE_TIME"

export {SET_UNIDAD_LAST_UPDATE_TIME,
  SET_UNIDADES_FUERA_RANGO,ADD_UNIDAD_A_INCIDENTE,SET_UNIDAD,UPDATE_TIME,SET_INCIDENTE_ESTADO,
  ADD_LOCALIZACION_TO_UNIDAD,SET_INCIDENTE,SET_MENSAJE,SET_LOCALIZACION}