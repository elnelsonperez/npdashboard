import * as types from './mutation_types'
import Vue from 'vue'
import {normalize} from 'normalizr'
import {incidente as incidenteSchema,
  unidad as unidadSchema,
  mensaje as mensajeSchema,
  localizacion as localizacionSchema} from './../schemas'
import moment from 'moment'

export default {
  [types.UPDATE_TIME] (state) {
    state.now = moment()
  },

  [types.SET_INCIDENTE_ESTADO] (state, {estado, incidente_id}) {
    if (state.normalized.entities.incidente[incidente_id]) {
      state.normalized.entities.incidente[incidente_id].estado = estado;
      state.normalized.entities.incidente[incidente_id].estado_id = estado.id;
    }
  },

  [types.SET_UNIDAD_LAST_UPDATE_TIME] (state, {unidad_id, last_update_time}) {
    if (state.normalized.entities.unidad[unidad_id]) {
      state.normalized.entities.unidad[unidad_id].last_update_time = last_update_time;
    }
  },

  [types.SET_UNIDADES_FUERA_RANGO] (state, unidades) {
    state.unidadesFuera = unidades
  },

  [types.ADD_UNIDAD_A_INCIDENTE] (state, {unidad_id, incidente_id}) {
    if (state.normalized.entities.incidente[incidente_id]) {
      if (state.normalized.entities.incidente[incidente_id].unidades &&
          !state.normalized.entities.incidente[incidente_id].unidades.includes(unidad_id)) {
        state.normalized.entities.incidente[incidente_id].unidades.push(unidad_id)
      } else {
        Vue.set(state.normalized.entities.incidente[incidente_id],'unidades',[unidad_id])
      }
    }
  },

  [types.SET_UNIDAD] (state, unidad) {
    const nm =  normalize(unidad, unidadSchema)
    if (!state.normalized.entities.unidad[unidad.id]) {
      state.normalized.result.unidades.push(unidad.id)
    }
    Vue.set(state.normalized.entities.unidad,unidad.id,nm.entities.unidad[unidad.id])
  },

  [types.SET_INCIDENTE] (state, incidente) {
    const nm = normalize(incidente, incidenteSchema)
    if (! state.normalized.entities.incidente[incidente.id]) {
      state.normalized.result.incidentes.push(incidente.id)
    }
    Vue.set(state.normalized.entities.incidente,incidente.id,nm.entities.incidente[incidente.id])
  },

  [types.SET_LOCALIZACION] (state, loc) {
    const nm = normalize(loc, localizacionSchema)
    Vue.set(state.normalized.entities.localizacion,loc.id,nm.entities.localizacion[loc.id])
  },

  [types.ADD_LOCALIZACION_TO_UNIDAD] (state, loc) {

    let unidad = null
    const unidades = state.normalized.entities.unidad;
    for (let key of Object.keys(unidades)) {
      if (unidades[key].hub === loc.pms_hub_id) {
        unidad = unidades[key];
        break
      }
    }
    if (unidad) {
      if (!state.normalized.entities.unidad[unidad.id].localizaciones.find(l => l.id === loc.id)) {
        state.normalized.entities.unidad[unidad.id].localizaciones.push(loc.id)
      }
    }

  },


  [types.SET_MENSAJE] (state, mensaje) {
    const nm =  normalize(mensaje, mensajeSchema)
    if (!state.normalized.entities.mensaje[mensaje.id]) {
      state.normalized.result.mensajes.push(mensaje.id)
    }
    Vue.set(state.normalized.entities.mensaje,mensaje.id,nm.entities.mensaje[mensaje.id])
  }

};
