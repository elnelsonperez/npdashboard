import Vue from 'vue';
import Vuex from 'vuex';
import * as actions from './actions';
import * as getters from './getters';
import mutations from './mutations';
import moment from 'moment';
import {normalize} from 'normalizr';
import {baseDataSchema} from './../schemas'
Vue.use(Vuex);

const data = JSON.parse(baseData)
const normalizedData = normalize(data, baseDataSchema);

const store = new Vuex.Store({
  actions,
  getters,
  mutations,
  state: {
    normalized: normalizedData,
    unidadesFuera: null,
    now: moment()
  },
})

export default store