import moment from 'moment'

function addVertexToPolygon(polygon, point) {
  const copy = Object.assign({},polygon);
  copy.coordinates[0].push([point.lng, point.lat])
  return copy;
}

function orderLocationsByDateDesc(localizaciones) {
  return localizaciones.sort((a,b) => {
    return moment(a.fecha_obtenida).unix() < moment(b.fecha_obtenida).unix()
  })
}


function localizacionesToWaypoints(localizaciones) {
  return localizaciones.map(v => {

    return {
      location: new google.maps.LatLng(v.ubicacion.coordinates[1],v.ubicacion.coordinates[0]),
      stopover: false
    }
  })
}


function getLastUbicacion(localizaciones) {
  let last = null;
  let lastSeconds = null;
  for (let l of localizaciones) {
    if (last == null) {
      last = l;
      lastSeconds = moment(l.fecha_obtenida).unix()
    } else {
      if (moment(l.fecha_obtenida).unix() > lastSeconds) {
        last = l;
        lastSeconds = moment(l.fecha_obtenida).unix()
      }
    }
  }

  return last;
}

function formatDistance(metros) {
  if (metros >= 1000) {
    const km = metros / 1000;
    return km.toFixed(2) + " km"
  }
  return Math.round(metros) + " metros"
}

function formatTime(s) {
  const sec_num = s
  let hours   = Math.floor(sec_num / 3600);
  let minutes = Math.floor((sec_num - (hours * 3600)) / 60);
  let seconds = sec_num - (hours * 3600) - (minutes * 60);
  if (hours   < 10) {hours   = "0"+hours;}
  if (minutes < 10) {minutes = "0"+minutes;}
  if (seconds < 10) {seconds = "0"+seconds;}
  return hours+':'+minutes+':'+seconds;
}

function addressByFormat(addresses, format) {
  var result = null;
  for (var i = addresses.length - 1; i >= 0; i--) {
    if (addresses[i].types.indexOf(format) >= 0) {
      result = addresses[i];
    }
  }
  return result || addresses[0];
}

function address_component_from_google_geocode (address_components) {
  var result = {};
  for (var i = address_components.length - 1; i >= 0; i--) {
    var component = address_components[i];
    if (component.types.indexOf("postal_code") >= 0) {
      result.postalCode = component.short_name;
    } else if (component.types.indexOf("street_number") >= 0) {
      result.streetNumber = component.short_name;
    } else if (component.types.indexOf("route") >= 0) {
      result.streetName = component.short_name;
    } else if (component.types.indexOf("locality") >= 0) {
      result.city = component.short_name;
    } else if (component.types.indexOf("sublocality") >= 0) {
      result.district = component.short_name;
    } else if (component.types.indexOf("administrative_area_level_1") >= 0) {
      result.stateOrProvince = component.short_name;
    } else if (component.types.indexOf("country") >= 0) {
      result.country = component.short_name;
    }
  }
  result.addressLine1 = [ result.streetNumber, result.streetName ].join(" ").trim();
  result.addressLine2 = "";
  return result;
}

function getUnitLastLocationUpdateTime(unidad) {
  const minutos = getMinutesSinceDate(unidad.localizaciones.last().fecha_obtenida);
  if (minutos < 160) {
    return minutos + " min."
  }
  return getHoursSinceDate(unidad.localizaciones.last().fecha_obtenida) + " hrs."
}

function getHoursSinceDate (date) {
  const diff = moment.duration(moment().diff(moment(date)));
  return diff.asHours().toFixed(0)
}

function getMinutesSinceDate (date) {
  const diff = moment.duration(moment().diff(moment(date)));
  return diff.asMinutes().toFixed(0)
}


function getBoundsFromPoints(puntos) {
  const points = puntos.map(v => v.ubicacion)
  const bounds = new google.maps.LatLngBounds();
  for (let v of points) {
    const p = getLatLngFromPoint(v)
    bounds.extend(new google.maps.LatLng(p.lat, p.lng));
  }
  return bounds;
}

function getBoundsFromPolygon(polygon) {
  const values = getLatLngArrayFromPolygon(polygon);
  const bounds = new google.maps.LatLngBounds();
  for (let v of values) {
    bounds.extend(new google.maps.LatLng(v.lat, v.lng));
  }
  return bounds;
}

function getLatLngFromPoint (point) {
  if (point) {
    return {
      lat: point.coordinates[1],
      lng: point.coordinates[0]
    }
  }
  return null
}

function getLatLngArrayFromPolygon (polygon) {
  if (polygon) {
    const arr = polygon.coordinates[0];
    return arr.map(function (val) {
      return {
        lat: val[1],
        lng: val[0]
      }
    })
  }
  return null
}

function notEmpty(array) {
  return array && array.length > 0;
}

export {getMinutesSinceDate, getHoursSinceDate, getLatLngFromPoint,localizacionesToWaypoints,getBoundsFromPoints,getLastUbicacion,orderLocationsByDateDesc,formatDistance,addVertexToPolygon,formatTime,addressByFormat,address_component_from_google_geocode,getLatLngArrayFromPolygon,getBoundsFromPolygon,getUnitLastLocationUpdateTime}