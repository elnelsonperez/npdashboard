import store from "./../store";

export default function (Echo) {
  Echo.channel('d-'+destacamento_id)
      .listen('.incidencia_civil.unidad_asignada', (e) => {
        store.dispatch('setIncidenteEstado',
            {estado: e.estado, incidente_id: e.incidencia})
        store.dispatch('addUnidadAIncidente',
            {unidad_id: e.unidad, incidente_id: e.incidencia})
      })
      .listen('.incidencia_civil.created', (e) => {
        store.dispatch('setIncidencia', e.incidenciaCivil)
      })
      .listen('.hub_localizacion.created_batch', (e) => {
        store.dispatch('setLocalizaciones', e.localizaciones)
      })
      .listen('.incidencia_civil.status_changed', (e) => {
        store.dispatch('setIncidencia', e.incidenciaCivil)

        //This is so wrong to be here but im lazy
        if (
            e.incidenciaCivil.estado_id === 7 ||
            e.incidenciaCivil.estado_id === 6 ||
            e.incidenciaCivil.estado_id === 5 ||
            e.incidenciaCivil.estado_id === 4
        ) {
          const timersString = localStorage.getItem("timers");
          if (timersString) {
            let timers = JSON.parse(timersString)
            timers = timers.filter(e => e.incidencia_id !== e.incidenciaCivil.id)
            localStorage.setItem('timers', JSON.stringify(timers))
          }

        }

      })
      .listen('.mensaje_hub.received', (e) => {
        store.dispatch('setMessage', e.mensaje)
      })
      .listen('.unidad.last_update', (e) => {
        store.dispatch('setUnidadLastUpdateTime', e)
      })
      .listen('.mensaje_hub.batch_updated', (e) => {
        for (let m of e.mensajes) {
          store.dispatch('setMessage', m)
        }
      });
}
