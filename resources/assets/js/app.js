import * as VueGoogleMaps from 'vue2-google-maps';
import Vue from 'vue';
import 'vue2-animate/dist/vue2-animate.min.css';
import 'animate.css';
import Toastr from 'vue-toastr';
import 'vue-toastr/dist/vue-toastr.css'
import store from './store'
import Pusher from "pusher-js"
import EchoLib from "laravel-echo"
import VueRouter from 'vue-router'
import Echo from './echo/echo'
import VueSweetalert2 from 'vue-sweetalert2';
Vue.use(VueSweetalert2);
Vue.use(VueRouter)
Vue.use(Toastr)
Vue.use(VueGoogleMaps, {
  load: {
    key: 'AIzaSyASEocXRJGmlHQNKz6Y7p7fYngBdEMxrCw',
    v: '3',
    // libraries: 'places',
  }
});

Array.prototype.last = function() {
  return this[this.length-1];
}

import ControlPatrullas from './components/ControlPatrullas'
import ChatContainer from './components/ChatContainer'
const routes = [
  { path: '/',name: 'mapa', component: ControlPatrullas },
  { path: '/chat',name: 'chat', component: ChatContainer }
]

const router = new VueRouter({
  routes
})

window.Echo = new EchoLib({
  broadcaster: 'pusher',
  key: 'de93cb04a7eea4af7077',
  cluster: 'us2',
});

Echo(window.Echo)

const app = new Vue({
  el: '#app',
  store,
  router
});
