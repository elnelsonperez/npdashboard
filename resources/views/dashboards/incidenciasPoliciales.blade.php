@extends('layouts.main')
@section('title', 'Incidencias Policiales')
@section('styles')
@endsection
@section('content')
    <div class="row">
        <div class="col-xs-12 col-md-10 col-md-offset-1">
            @component('components.defaultPanel')
                @slot('heading','Informe de Incidentes Policiales')
                @slot('body')
                    <div class="table-responsive" style="min-height: 450px">
                        <table class="table text-nowrap" style="border: 1px solid #ddd">
                            <thead>
                            <tr>
                                <th>Unidad</th>
                                <th class="col-md-2">Tipo</th>
                                <th class="col-md-2">Fecha</th>
                                <th class="col-md-2">Ubicacion</th>
                                <th class="text-center" style="width: 20px;"><i class="icon-arrow-down12"></i></th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr class="active border-double">
                                <td colspan="5">Unidades Fuera de Zona</td>
                            </tr>
                            <tr>
                                <td>
                                    <div class="media-left">
                                        <div class=""><a href="#" class="text-default text-semibold">
                                                Unidad 25 - Toyota Corolla</a></div>
                                        <div class="text-muted text-size-small">
                                            <span class="icon-location4"></span>
                                            La Esmeralda, Santiago de Los Caballeros
                                        </div>
                                    </div>
                                </td>
                                <td>Fuera de Zona</td>
                                <td>
                                    31-01-2018 03:25 PM
                                </td>
                                <td>
                                    <span class="label bg-blue-700">
                                        Ver en Mapa
                                    </span>
                                </td>
                                <td class="text-center">
                                    <ul class="icons-list">
                                        <li class="dropdown">
                                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false"><i class="icon-menu7"></i></a>
                                            <ul class="dropdown-menu dropdown-menu-right">
                                                <li><a href="#"><i class="icon-envelop2"></i> Enviar Mensaje </a></li>
                                                <li><a href="#"><i class="icon-location3"></i> Ir a Ubicacion </a></li>
                                            </ul>
                                        </li>
                                    </ul>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <div class="media-left">
                                        <div class=""><a href="#" class="text-default text-semibold">
                                                Unidad 25 - Toyota Corolla</a></div>
                                        <div class="text-muted text-size-small">
                                            <span class="icon-location4"></span>
                                            La Esmeralda, Santiago de Los Caballeros
                                        </div>
                                    </div>
                                </td>
                                <td>Fuera de Zona</td>
                                <td>
                                    31-01-2018 03:25 PM
                                </td>
                                <td>
                                    <span class="label bg-blue-700">
                                        Ver en Mapa
                                    </span>
                                </td>
                                <td class="text-center">
                                    <ul class="icons-list">
                                        <li class="dropdown">
                                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false"><i class="icon-menu7"></i></a>
                                            <ul class="dropdown-menu dropdown-menu-right">
                                                <li><a href="#"><i class="icon-envelop2"></i> Enviar Mensaje </a></li>
                                                <li><a href="#"><i class="icon-location3"></i> Ir a Ubicacion </a></li>
                                            </ul>
                                        </li>
                                    </ul>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <div class="media-left">
                                        <div class=""><a href="#" class="text-default text-semibold">
                                                Unidad 25 - Toyota Corolla</a></div>
                                        <div class="text-muted text-size-small">
                                            <span class="icon-location4"></span>
                                            La Esmeralda, Santiago de Los Caballeros
                                        </div>
                                    </div>
                                </td>
                                <td>Fuera de Zona</td>
                                <td>
                                    31-01-2018 03:25 PM
                                </td>
                                <td>
                                    <span class="label bg-blue-700">
                                        Ver en Mapa
                                    </span>
                                </td>
                                <td class="text-center">
                                    <ul class="icons-list">
                                        <li class="dropdown">
                                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false"><i class="icon-menu7"></i></a>
                                            <ul class="dropdown-menu dropdown-menu-right">
                                                <li><a href="#"><i class="icon-envelop2"></i> Enviar Mensaje </a></li>
                                                <li><a href="#"><i class="icon-location3"></i> Ir a Ubicacion </a></li>
                                            </ul>
                                        </li>
                                    </ul>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <div class="media-left">
                                        <div class=""><a href="#" class="text-default text-semibold">
                                                Unidad 25 - Toyota Corolla</a></div>
                                        <div class="text-muted text-size-small">
                                            <span class="icon-location4"></span>
                                            La Esmeralda, Santiago de Los Caballeros
                                        </div>
                                    </div>
                                </td>
                                <td>Fuera de Zona</td>
                                <td>
                                    31-01-2018 03:25 PM
                                </td>
                                <td>
                                    <span class="label bg-blue-700">
                                        Ver en Mapa
                                    </span>
                                </td>
                                <td class="text-center">
                                    <ul class="icons-list">
                                        <li class="dropdown">
                                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false"><i class="icon-menu7"></i></a>
                                            <ul class="dropdown-menu dropdown-menu-right">
                                                <li><a href="#"><i class="icon-envelop2"></i> Enviar Mensaje </a></li>
                                                <li><a href="#"><i class="icon-location3"></i> Ir a Ubicacion </a></li>
                                            </ul>
                                        </li>
                                    </ul>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                @endslot
            @endcomponent
        </div>
    </div>
@endsection
@section('scripts')

@endsection