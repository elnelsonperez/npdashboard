@extends('layouts.main')
@section('title', 'Registro de Incidencias')
@section('styles')
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
    <link rel="stylesheet" href="{{asset('css/custom.css')}}">
@endsection
@section('content')
    <div class="row">
        <div class="col-xs-12 col-md-10 col-md-offset-1">
            @component('components.defaultPanel')
                @slot('heading','Registro de Incidencia Civil')
                @slot('body')
                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <h6>Ocurrieron errores al registrar la incidencia:</h6>
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    <form action="{{route('incidencias_civiles.store')}}" method="post">
                        {{csrf_field()}}
                        <input type="hidden" id="ubicacion_lat" name="ubicacion_lat">
                        <input type="hidden" id="ubicacion_lng" name="ubicacion_lng">
                        <legend class="text-bold">Datos del que reporta</legend>
                        <div class="row">
                            <div class="col-xs-6">
                                @include('components.input', [
                            'name' => 'nombre_civil',
                            'title' => 'Nombre completo'
                            ])
                            </div>
                            <div class="col-xs-6">
                                @include('components.input', [
                               'name' => 'telefono_civil',
                               'title' => 'Numero de contacto',
                               'type' => 'tel',
                               'id' => 'telefono_civil'
                               ])
                            </div>
                        </div>

                        <legend class="text-bold">Ubicacion y detalles del incidente</legend>
                        <div class="row">
                            <div class="col-sm-6">
                                @include('components.input', [
                              'name' => 'fecha_incidencia',
                              'title' => 'Fecha del incidente',
                              'type' => 'datetime-local',
                              'required' => true,
                              'value' => now()->format("Y-m-d\TH:i")
                              ])
                            </div>
                            <div class="col-sm-6">
                                @include('components.input', [
                              'name' => 'created_at',
                              'title' => 'Fecha de registro',
                              'type' => 'text',
                              'disabled' => true,
                              'id' => 'fecha_registro'
                              ])
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-4">
                                @component('components.select')
                                    @slot('title', 'Tipo de incidente')
                                    @slot('name', 'tipo')
                                    @slot('required',true)
                                    @foreach($tipos as $t)
                                        <option @if(old('tipo') == $t->id)selected @endif value="{{$t->id}}">{{$t->nombre}}</option>
                                    @endforeach
                                    @slot('search', true)
                                @endcomponent
                            </div>
                            <div class="col-sm-4">
                                @component('components.select')
                                    @slot('title', 'Prioridad')
                                    @slot('name', 'prioridad')
                                    @slot('required',true)
                                    @foreach($prioridades as $t)
                                        <option  @if(old('tipo') == $t->id)selected @endif  value="{{$t->id}}">{{$t->nombre}}</option>
                                    @endforeach
                                @endcomponent
                            </div>
                            <div class="col-sm-4">
                                @include('components.input', [
                              'name' => 'personas_involucradas',
                              'title' => 'Cantidad de personas involucradas',
                              'type' => 'number',
                              'value' => 1
                              ])
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-6">
                                @component('components.select')
                                    @slot('title', 'Municipio')
                                    @slot('name', 'municipio')
                                    @slot('required',true)
                                    @foreach($municipios as $t)
                                        <option value="{{$t->id}}">{{$t->nombre}}</option>
                                    @endforeach
                                    @slot('id','municipios')
                                    @slot('search', true)
                                @endcomponent
                            </div>
                            <div class="col-sm-6">
                                @component('components.select')
                                    @slot('title', 'Sector')
                                    @slot('name', 'sector')
                                    @slot('required',true)
                                    @slot('disabled', true)
                                    @slot('search', true)
                                    @slot('id','sectores')
                                @endcomponent
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label class="control-label">Buscar un lugar, calle o sector en el mapa</label>
                                    <input id="location" value="{{old('ubicacion_texto') or ""}}" type="text" name="ubicacion_texto" class="form-control">
                                </div>
                            </div>
                            <script>
                              $('#fecha_registro').val(moment().format('DD/MM/Y hh:mm:ss A'));
                              setInterval(function() {
                                $('#fecha_registro').val(moment().format('DD/MM/Y hh:mm:ss A'));
                              }, 1000);
                            </script>

                        </div>
                        <div class="row mb-15" >
                            <div class="col-xs-12">
                                <div id="mappicker" style="height: 400px;"></div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-6">
                                <div class="form-group">
                                    <label>Detalles de ubicacion: <span style="color: red">*</span></label>
                                    <textarea id="detalles_ubicacion"
                                              name="detalle_ubicacion" rows="3" class="form-control" style="resize: vertical;"
                                              placeholder="Introducir detalles del lugar de los hechos, nombre de edificios, refencias geograficas, etc"></textarea>
                                </div>
                            </div>
                            <div class="col-xs-6">
                                <div class="form-group">
                                    <label>Detalles del incidente: <span style="color: red">*</span></label>
                                    <textarea rows="3" name="detalle_incidente" class="form-control" style="resize: vertical;"
                                              placeholder="Detalles del evento e implicados"></textarea>
                                </div>
                            </div>
                        </div>
                        @include('components.formButton')
                    </form>

                @endslot
            @endcomponent
        </div>
    </div>
@endsection
@section('scripts')
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyASEocXRJGmlHQNKz6Y7p7fYngBdEMxrCw&libraries=places">
    </script>
    <script src="{{asset('js/locationpicker.js')}}" type="text/javascript"></script>
    <script>

      $("form").bind("keypress", function (e) {
        if (e.keyCode === 13) {
          return false;
        }
      });

      $('#municipios').on('change', function () {
        var sectores =  $('#sectores');
        sectores.find('option').remove();
        sectores.attr('disabled', true);
        sectores.selectpicker('refresh');
        $.get('/api/sectores', {municipio: $(this).val(), fields: ['id','nombre']}, function (res) {
          $.each(res, function(key, v) {
            $('#sectores')
                .append($("<option></option>")
                    .attr("value",v.id)
                    .text(v.nombre));
          });
          sectores.attr('disabled', false);
          sectores.selectpicker('refresh');
        })
      })

      $('#telefono_civil').formatter({
        'pattern': '@{{999}}-@{{999}}-@{{9999}}',
        'persistent': true
      });

      $('#mappicker').locationpicker({
        onchanged: function (currentLocation, radius, isMarkerDropped) {
          var addressComponents = $(this).locationpicker('map').location.addressComponents;
          var zona = addressComponents.district ? addressComponents.district+", " : "";
          $("#location").val( addressComponents.addressLine1+", "+ zona +
              addressComponents.stateOrProvince)
        },
        location: {
          latitude: 19.452743,
          longitude: -70.684875
        },
        inputBinding: {
          locationNameInput: $('#location'),
          latitudeInput: $("#ubicacion_lat"),
          longitudeInput: $("#ubicacion_lng"),
        },
        radius: 50,
        zoom: 14,
        enableAutocomplete: true,
        enableReverseGeocode: true,
        enableAutocompleteBlur: true,
        addressFormat: 'subpremise',
        autocompleteOptions: {
          componentRestrictions: {country: 'do'}
        }
      });

    </script>
@endsection