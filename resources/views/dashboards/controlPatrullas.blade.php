@extends('layouts.main')
@section('title', 'Control de Patrullas')
@section('styles')
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
    <link rel="stylesheet" href="{{asset('css/custom.css')}}">
@endsection
@section('content')
    <div id="app">
        <router-view></router-view>
    </div>
@endsection
@section('scripts')
    @javascript(['baseData' => $initialData])
    @javascript(['destacamento_id' => $destacamento_id])
    <script src="{{asset("js/app.js")}}" type="text/javascript"></script>
@endsection