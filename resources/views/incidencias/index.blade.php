@extends('layouts.main')
@section('title', 'Incidencias Civiles')
@section('styles')

@endsection
@section('content')

    <div class="row">
        <div class="col-xs-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4 class="panel-title">Listado de Incidencias Civiles</h4>
                    <div class="heading-elements">
                        <form class="heading-form" method="get" action="" id="searchform">
                            <div class="form-group has-feedback">
                                <input type="search" class="form-control" id="search" name="search" placeholder="Buscar...">
                                <div class="form-control-feedback">
                                    <i class="icon-search4 text-size-base text-muted"></i>
                                </div>
                            </div>
                        </form>
                        <script>
                          function qs(key) {
                            key = key.replace(/[*+?^$.\[\]{}()|\\\/]/g, "\\$&"); // escape RegEx meta chars
                            var match = location.search.match(new RegExp("[?&]"+key+"=([^&]+)(&|$)"));
                            return match && decodeURIComponent(match[1].replace(/\+/g, " "));
                          }
                          var search = qs("search");
                          if (search) {
                            $('#search').val(search)
                          }
                          $('#search').keypress(function (e) {
                            if (e.which === 13) {
                              $('#searchform').submit();
                              return false;
                            }
                          });
                        </script>
                    </div>
                </div>
                <div class="panel-body">
                    <table class="table table-framed table-condensed">
                        <thead>
                        <tr>
                            <th style="width: 30px">#</th>
                            <th>Tipo</th>
                            <th>Prioridad</th>
                            <th>Sector</th>
                            <th>Estado</th>
                            <th>Fecha registro</th>
                            <th>Fecha incidencia</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($incidencias as $i)
                            <tr class="incidencia-row" data-id="{{$i->id}}" style="cursor: pointer">
                                <td class="text-center">
                                    {{$i->id}}
                                </td>
                                <td>
                                    {{$i->tipo->nombre}}
                                </td>
                                  <td>
                                    {{$i->prioridad->nombre}}
                                </td>
                                <td>
                                    {{$i->sector->nombre}}, {{$i->sector->municipio->nombre}}
                                </td>
                                 <td>
                                    {{$i->estado->nombre}}
                                </td>
                                <td>
                                    {{$i->creado_en->format('d-m-y h:i:s A')}}
                                </td>
                                <td>
                                    {{$i->fecha_incidencia->format('d-m-y h:i:s A')}}
                                </td>

                            </tr>
                        @endforeach

                        </tbody>
                    </table>
                    <div style=" margin-top: 10px;text-align: center;">
                        {{$incidencias->links()}}
                    </div>

                    <div class="alert alert-primary no-border mt-15" style="margin-bottom: 0">
                        <button type="button" class="close" data-dismiss="alert"><span>×</span><span class="sr-only">Close</span></button>
Haga click en una incidencia para ver mas detalles.
                    </div>
                </div>
            </div>

        </div>
    </div>

@endsection
@section('scripts')
    <script>
        $('.incidencia-row').on('click', function () {
          window.open("/incidencias/"+$(this).data('id'), '_blank');
        })
    </script>
@endsection