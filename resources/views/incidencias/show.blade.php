@extends('layouts.main')
@section('title', 'Detalle de incidencia')
@section('styles')
    <style>
        #map {
            height: 250px;
            width: 100%;
        }
        .field-container {
            display: flex;
            flex-direction: column;
        }
        .field-row {
            display: flex;
            flex-direction: row;
            justify-content: space-between;
            margin-bottom: 10px;
        }
        .field {
            display: flex;
            flex-direction: column;

        }
        .field:not(:last-child) {
            margin-right: 10px;
        }
        .field .title {
            line-height: 1.2;
        }
        .field .content {
            font-size: 15px;
        }

        .right-widget {
            border-left: 1px dotted #d7d7d7;
            display: flex;
            flex-direction: column;
            justify-content: space-between;
        }

        .oficiales-holder {
            display: flex;
            flex-wrap: wrap;
            justify-content: space-around;
        }
        .oficial {
            width: 250px;
            margin-top: 15px;
        }
        .oficial.media:first-child {
            margin-top: 20px;
        }

        .fieldrow.wrapme {
            flex-wrap: wrap;
        }
    </style>
@endsection
@section('content')
    <div class="row">
        <div class="col-md-7">
            <div class="row">
                <div class="col-xs-12">
                    <div class="panel panel-primary">
                        <div class="panel-heading slim">
                            <h6 class="panel-title">
                                <span class="text-bold">Detalles de la incidencia</span>
                            </h6>
                            <div class="heading-elements">
                                <ul class="icons-list">
                                    <li><a data-action="collapse" class=""></a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="panel-body">
                            <div class="row" style="display: flex">
                                <div class="col-xs-7">

                                    <div class="field-container">
                                        <div class="field-row">
                                            <div class="field">
                                                <div class="title text-muted">
                                                    Fecha de incidencia
                                                </div>
                                                <div class="content">
                                                    {{$incidencia->fecha_incidencia->format('d-m-y h:i:s A')}}
                                                </div>
                                            </div>
                                            <div class="field">
                                                <div class="title text-muted">
                                                    Fecha de registro
                                                </div>
                                                <div class="content">
                                                    {{$incidencia->creado_en->format('d-m-y h:i:s A')}}
                                                </div>
                                            </div>
                                        </div>
                                        <div class="field-row">
                                            <div class="field">
                                                <div class="title text-muted">
                                                    Tipo
                                                </div>
                                                <div class="content">
                                                    {{$incidencia->tipo->nombre}}
                                                </div>
                                            </div>
                                            <div class="field">
                                                <div class="title text-muted">
                                                    Prioridad
                                                </div>
                                                <div class="content">
                                                    {{$incidencia->prioridad->nombre}}
                                                </div>
                                            </div>
                                            <div class="field">
                                                <div class="title text-muted">
                                                    Ubicación
                                                </div>
                                                <div class="content">
                                                    {{$incidencia->ubicacion_texto}}
                                                </div>
                                            </div>
                                        </div>

                                        <div class="field-row">
                                            <div class="field">
                                                <div class="title text-muted">
                                                    Detalle incidente
                                                </div>
                                                <div class="content">
                                                    {{$incidencia->detalle_incidente}}
                                                </div>
                                            </div>
                                        </div>
                                        <div class="field-row">
                                            <div class="field">
                                                <div class="title text-muted">
                                                    Detalle ubicación
                                                </div>
                                                <div class="content">
                                                    {{$incidencia->detalle_ubicacion}}
                                                </div>
                                            </div>
                                        </div>
                                        <div class="field-row">
                                            <div class="field">
                                                <div class="title text-muted">
                                                    Municipio
                                                </div>
                                                <div class="content">
                                                    {{$incidencia->sector->municipio->nombre}}
                                                </div>
                                            </div>
                                            <div class="field">
                                                <div class="title text-muted">
                                                    Sector
                                                </div>
                                                <div class="content">
                                                    {{$incidencia->sector->nombre}}
                                                </div>
                                            </div>
                                        </div>
                                        <div class="field-row">
                                            <div class="field">
                                                <div class="title text-muted">
                                                    Nombre del civil
                                                </div>
                                                <div class="content">
                                                    {{$incidencia->nombre_civil}}
                                                </div>
                                            </div>
                                            <div class="field">
                                                <div class="title text-muted">
                                                    Contacto del civil
                                                </div>
                                                <div class="content">
                                                    {{$incidencia->telefono_civil}}
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-xs-5 right-widget">

                                    <div class="media no-margin">
                                        <div class="media-body">
                                            <h6 class="media-heading text-semibold">{{$incidencia->estado->nombre}}</h6>
                                            <span class="text-muted">Estado de incidencia</span>
                                        </div>
                                        <div class="media-right media-middle">
                                            <i class="icon-finish icon-2x text-grey-800"></i>
                                        </div>
                                    </div>


                                    <div class="media no-margin">
                                        <div class="media-body">
                                            @if(is_null($tiempo_de_respuesta))
                                                <h6 id="timerespuesta" class="media-heading text-semibold">Calculando</h6>
                                            @else
                                                <h6 class="media-heading text-semibold">{{$tiempo_de_respuesta}}</h6>
                                            @endif
                                            <span class="text-muted">Tiempo de respuesta</span>
                                        </div>
                                        <div class="media-right media-middle">
                                            <i class="icon-watch icon-2x text-grey-800"></i>
                                        </div>
                                    </div>


                                    <div class="media no-margin">
                                        <div class="media-body">
                                            @if($incidencia->personas_involucradas > 0)
                                                <h6 class="media-heading text-semibold">
                                                    {{$incidencia->personas_involucradas}}
                                                    @if ($incidencia->personas_involucradas > 1)
                                                        personas
                                                        @else
                                                        persona
                                                    @endif
                                                </h6>
                                            @else
                                                <h6 class="media-heading text-semibold">Ninguna</h6>
                                            @endif
                                            <span class="text-muted">Personas involucradas</span>
                                        </div>
                                        <div class="media-right media-middle">
                                            <i class="icon-users icon-2x text-grey-800"></i>
                                        </div>
                                    </div>



                                    <div class="media no-margin">
                                        <div class="media-body">
                                            @if (is_null($unidad))
                                                <h6 class="media-heading text-semibold">Ninguna</h6>
                                            @else
                                                <h6 class="media-heading text-semibold">Unidad {{$unidad->id}}</h6>
                                            @endif

                                            <span class="text-muted">Unidad asignada</span>
                                        </div>
                                        <div class="media-right media-middle">
                                            <i class="icon-car2 icon-2x text-grey-800"></i>
                                        </div>
                                    </div>

                                    <div class="media no-margin">
                                        <div class="media-body">

                                            <h6 class="media-heading text-semibold">
                                                {{$incidencia->creador->oficial->nombre}}  {{$incidencia->creador->oficial->apellido}}</h6>
                                            <span class="text-muted">Registrada por</span>
                                        </div>
                                        <div class="media-right media-middle">
                                            <i class="icon-user-tie icon-2x text-grey-800"></i>
                                        </div>
                                    </div>


                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12">
                    <div class="panel panel-primary">
                        <div class="panel-heading slim">
                            <h6 class="panel-title">
                                <span class="text-bold">Detalles de la unidad asignada</span>
                            </h6>
                            <div class="heading-elements">
                                <ul class="icons-list">
                                    <li><a data-action="collapse" class=""></a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="panel-body">
                            @if($unidad)
                                <div class="field-container">
                                    <div class="field-row wrapme">
                                        <div class="field">
                                            <div class="title text-muted">
                                                Vehículo
                                            </div>
                                            <div class="content">
                                                {{$unidad->modelo->marca->nombre}}
                                                {{$unidad->modelo->nombre}}
                                                {{$unidad->ano}}
                                            </div>
                                        </div>
                                        <div class="field">
                                            <div class="title text-muted">
                                                Tipo
                                            </div>
                                            <div class="content">
                                                @if ($unidad->tipo == 0)
                                                    Cuatro ruedas
                                                @else
                                                    Motocicleta
                                                @endif
                                            </div>
                                        </div>
                                        <div class="field">
                                            <div class="title text-muted">
                                                Placa
                                            </div>
                                            <div class="content">
                                                {{$unidad->placa}}
                                            </div>
                                        </div>
                                        <div class="field">
                                            <div class="title text-muted">
                                                Estado
                                            </div>
                                            <div class="content">
                                                {{$unidad->estado->nombre}}
                                            </div>
                                        </div>
                                        <div class="field">
                                            <div class="title text-muted">
                                                Número de flota
                                            </div>
                                            <div class="content">
                                                {{$unidad->supervisor->flota->digitos}}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <legend class="text-bold" style="margin-bottom: 0; padding-bottom: 5px">Integrantes</legend>
                                <div class="oficiales-holder">
                                    @foreach ($unidad->oficiales as $o)
                                        <div class="media oficial">
                                            <div class="media-left">
                                                @if ($o->foto)
                                                    <img src={{$o->foto}} alt="">
                                                @else
                                                    <img src={{asset("image/defaultuser.png")}} alt="">
                                                @endif
                                            </div>
                                            <div class="media-body">
                                                <h6 class="media-heading">
                                                    {{$o->nombre}}  {{$o->apellido}}</h6>
                                                <span class="text-muted">
                                                <span class="icon-stars text-success"></span> {{$o->rango->nombre}} <br>
                                              <span class="icon-rating2 text-success"></span>

                                                    @if ($o->tipo->id == 2)
                                                        <span class="text-bold">
                                                    {{$o->tipo->nombre}}
                                                </span>
                                                    @else
                                                        {{$o->tipo->nombre}}
                                                    @endif

                                            </span>
                                            </div>
                                        </div>
                                    @endforeach
                                </div>

                            @else
                                <div class="alert alert-primary no-border">
                                    <button type="button" class="close" data-dismiss="alert"><span>×</span><span class="sr-only">Close</span></button>
                                    Una unidad no ha sido asignada a esta incidencia todavía.
                                </div>
                            @endif

                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-5">
            <div class="row">
                <div class="col-xs-12">
                    <div class="panel panel-primary">
                        <div class="panel-heading slim">
                            <h6 class="panel-title">
                                <span class="text-bold">Ubicación donde ocurrió la incidencia</span>
                            </h6>
                            <div class="heading-elements">
                                <ul class="icons-list">
                                    <li><a data-action="collapse" class=""></a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="panel-body">
                            <div id="map"></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12">
                    <div class="panel panel-primary">
                        <div class="panel-heading slim">
                            <h6 class="panel-title">
                                <span class="text-bold">Diagrama de tiempo</span>
                            </h6>
                            <div class="heading-elements">
                                <ul class="icons-list">
                                    <li><a data-action="collapse" class=""></a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="panel-body">
                            <ul class="list-feed">
                                @foreach($timeline as $t)
                                    <li class="border-blue-700">
                                        <div class="text-muted text-size-small mb-5">{{$t['time']}}</div>
                                        {!! $t['text'] !!}
                                    </li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
    <script src="https://maps.googleapis.com/maps/api/js?key={{env('GOOGLE_MAPS_API_KEY')}}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment-duration-format/2.2.2/moment-duration-format.min.js"></script>
    <script src="{{asset('js/gmaps.js')}}"></script>
    <script>

      function calculateTime() {
        var diff = moment.duration(moment().diff(moment('{{$incidencia->creado_en}}'))).format("hh:mm:ss");
        $('#timerespuesta').html(diff);
      }

      @if (is_null($tiempo_de_respuesta))
      calculateTime()
      setInterval(function() {
        calculateTime()
      }, 1000);
              @endif

      var map = new GMaps({
            div: '#map',
            lat: {{$incidencia->ubicacion->getLat()}},
            lng: {{$incidencia->ubicacion->getLng()}},
          });

      map.addMarker({
        lat: {{$incidencia->ubicacion->getLat()}},
        lng: {{$incidencia->ubicacion->getLng()}},
        title: 'Ubicación de la incidencia'
      });

      function convert(seconds) {
        seconds = Number(seconds);
        var hours = Math.floor(seconds / 3600);
        var minutes = Math.floor(seconds % 3600 / 60);
        var seconds = Math.floor(seconds % 3600 % 60);
        return ((hours > 0 ? hours + ":" + (minutes < 10 ? "0" : "") : "") + minutes + ":" + (seconds < 10 ? "0" : ""));
      }
    </script>
@endsection