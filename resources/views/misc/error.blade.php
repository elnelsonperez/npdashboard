<!DOCTYPE html>
<html lang="en">
@section('title','Acceso')
@include('partials.header')
<body class="navbar-bottom login-container">

@include('partials.mainbar',['blank' => true])
<div class="alert alert-danger no-border">
    <button type="button" class="close" data-dismiss="alert"><span>×</span><span class="sr-only">Close</span></button>
    {{$message}}
</div>
@include('partials.footer')

</body>
</html>
