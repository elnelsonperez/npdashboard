<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title> @yield('title') | NP Police Management System</title>

    <!-- Global stylesheets -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
    <link href={{asset("assets/css/icons/icomoon/styles.css")}} rel="stylesheet" type="text/css">
    <link href={{asset("assets/css/bootstrap.min.css")}} rel="stylesheet" type="text/css">
    <link href={{asset("assets/css/core.min.css")}} rel="stylesheet" type="text/css">
    <link href={{asset("assets/css/components.css")}} rel="stylesheet" type="text/css">
    <link href={{asset("assets/css/colors.min.css")}} rel="stylesheet" type="text/css">
    <link href={{asset("css/animate.css")}} rel="stylesheet" type="text/css">
    <link rel="icon" type="image/png" href="{{asset("favicon/favicon-32x32.png")}}" sizes="32x32" />
    <link rel="icon" type="image/png" href="{{asset("favicon/favicon-16x16.png")}}" sizes="16x16" />
    <style>
        @media (min-width: 769px) {
            .sidebar {
                width: 260px;
            }
        }
        .navbar-title {
            float: left;
            margin-top: 12px;
            font-weight: 500;
            font-size: 15px;
        }
        .table-condensed>tbody>tr>td {
            padding: 8px 10px;
        }
        .panel-primary .panel-heading {
            border-color: #1976D2;
            padding-bottom: 15px;
            padding-top: 15px;
            background-color: #1976D2;
        }
        .panel-heading.slim {
            padding-top: 10px;
            padding-bottom: 10px;
        }
        body {
            background-image: url("{{asset('image/sayagata-400px.png')}}");
            background-repeat: repeat;
        }

    </style>
@yield('styles')

<!-- Core JS files -->
    <script type="text/javascript" src={{asset("assets/js/core/libraries/jquery.min.js")}}></script>
    <script type="text/javascript" src={{asset("js/moment.min.js")}}></script>
    <script type="text/javascript" src={{asset("assets/js/core/libraries/bootstrap.min.js")}}></script>
    <script type="text/javascript" src={{asset("assets/js/plugins/ui/nicescroll.min.js")}}></script>
    <script type="text/javascript" src={{asset("assets/js/plugins/ui/drilldown.js")}}></script>
    <!-- /core JS files -->

    <!-- Theme JS files -->
    <script type="text/javascript" src={{asset("assets/js/core/app.js")}}></script>
    <!-- /theme JS files -->

    <script type="text/javascript" src={{asset("js/matchheight.js")}}></script>
    <script type="text/javascript" src={{asset("assets/js/plugins/forms/selects/bootstrap_select.min.js")}}></script>
    <script type="text/javascript" src="{{asset("assets/js/plugins/forms/inputs/formatter.min.js")}}"></script>

</head>