<!-- Main navbar -->
<div class="navbar navbar-inverse bg-primary-700">
    <div class="navbar-header">
        {{--<a class="navbar-brand" href="/"><img src="/assets/images/logo_light.png" alt=""></a>--}}
        <div style="margin-left: 25px">
            <a href="/" style="text-decoration: none;color: inherit;">
                <h4 >NP <span style="font-weight: 600">PMS </span> | <span style="font-weight: 300">Dashboard</span></h4>
            </a>

        </div>
        <ul class="nav navbar-nav pull-right visible-xs-block">
            <li><a data-toggle="collapse" data-target="#navbar-mobile"><i class="icon-tree5"></i></a></li>
            <li><a class="sidebar-mobile-main-toggle"><i class="icon-paragraph-justify3"></i></a></li>
        </ul>
    </div>

    @if (!isset($blank) || $blank == false)
        <div class="navbar-collapse collapse" id="navbar-mobile">
            <div class="navbar-title" style="margin-left: 25px">
             <span class="icon-office" style="margin-right: 5px"></span>
               Destacamento {{Auth::user()->destacamento()->nombre}}
            </div>
            {{--<ul class="nav navbar-nav">--}}
                {{--<li><a class="sidebar-control sidebar-main-toggle hidden-xs"><i class="icon-paragraph-justify3"></i></a></li>--}}
            {{--</ul>--}}

            <ul class="nav navbar-nav navbar-right">
                {{--<li>--}}
                    {{--<a href="#">--}}
                        {{--<i class="icon-cog3"></i>--}}
                        {{--<span class="visible-xs-inline-block position-right">Icon link</span>--}}
                    {{--</a>--}}
                {{--</li>--}}

                <li class="dropdown dropdown-user">
                    <a class="dropdown-toggle" data-toggle="dropdown" style=" padding-top: 9.5px;">
                        @if (Auth::user()->oficial->foto)
                            <img src={{asset(Auth::user()->oficial->foto)}} alt="">
                        @else
                            <img src={{asset("image/defaultuser.png")}} alt="">
                        @endif
                        <div style="display: inline-block; vertical-align: top; padding-left: 5px">
                            <div style="line-height: 1.3">{{Auth::user()->oficial->nombre}} {{Auth::user()->oficial->apellido}} </div>
                            <div style="line-height: 1; font-size: 11px">{{Auth::user()->tipoName()}}</div>
                        </div>




                        <i class="caret"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-menu-right">
                        <li><a href="#"><i class="icon-cog5"></i>Configuracion</a></li>
                        <li><a href="/logout"><i class="icon-switch2"></i>Salir</a></li>
                    </ul>
                </li>
            </ul>
        </div>
    @endif

</div>
<!-- /main navbar -->

