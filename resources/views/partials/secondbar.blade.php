<!-- Second navbar -->
<div class="navbar navbar-default" id="navbar-second">
    <ul class="nav navbar-nav no-border visible-xs-block">
        <li><a class="text-center collapsed" data-toggle="collapse" data-target="#navbar-second-toggle"><i class="icon-menu7"></i></a></li>
    </ul>

    <div class="navbar-collapse collapse" id="navbar-second-toggle">
        <ul class="nav navbar-nav">
            @if (Auth::user()->tipo == 1 ||Auth::user()->tipo == 5 )
                <li  @if(Route::currentRouteName() == 'registro_incidencias') class="active" @endif>
                    <a href="{{route('registro_incidencias')}}">
                        <i class="icon-stack-plus position-left"></i>
                        Registro de Incidentes Civiles
                    </a>
                </li>
            @endif
            @if (Auth::user()->tipo == 2 ||Auth::user()->tipo == 5)
                <li @if(Route::currentRouteName() == 'control_patrullas') class="active" @endif>
                    <a href="{{route('control_patrullas')}}">
                        <i class="icon-map position-left"></i>
                        Control de Unidades
                    </a>
                </li>
            @endif

            {{--<li  @if(Route::currentRouteName() == 'incidencias_policiales') class="active" @endif>--}}
            {{--<a href="{{route('incidencias_policiales')}}">--}}
            {{--<i class="icon-display4 position-left"></i>--}}
            {{--Incidencias Policiales--}}
            {{--</a>--}}
            {{--</li>--}}

            @if (Auth::user()->tipo == 3 ||Auth::user()->tipo == 5)
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <i class="icon-clipboard3 position-left"></i>Operaciones <span class="caret"></span>
                    </a>
                    <ul class="dropdown-menu width-200">
                        <li class="dropdown-submenu">
                            <a href="#"><i class="icon-warning"></i>Incidencias Civiles</a>
                            <ul class="dropdown-menu width-200">
                                <li class="dropdown-header highlight">Operaciones</li>
                                <li><a href="{{'/incidencias'}}"><i class="icon-list3"></i>Ver listado</a></li>
                            </ul>
                        </li>
                        <li class="dropdown-submenu">
                            <a href="#"><i class="icon-user"></i>Oficiales</a>
                            <ul class="dropdown-menu width-200">
                                <li class="dropdown-header highlight">Operaciones</li>
                                <li><a href="{{route('oficiales.create')}}"><i class="icon-add"></i> Crear oficial</a></li>
                                <li><a href="{{route('oficiales.index')}}"><i class="icon-list3"></i> Ver y editar </a></li>
                            </ul>
                        </li>
                        <li class="dropdown-submenu">
                            <a href="#"><i class="icon-car2"></i>Unidades</a>
                            <ul class="dropdown-menu width-200">
                                <li class="dropdown-header highlight">Operaciones</li>
                                <li><a href="{{route('unidades.create')}}"><i class="icon-add"></i> Crear unidad</a></li>
                                <li><a href="{{route('unidades.index')}}"><i class="icon-list3"></i> Ver y editar unidades </a></li>
                                <li><a href="{{route('hub_config')}}"><i class="icon-gear"></i> Editar Hub Config </a></li>
                            </ul>
                        </li>
                        {{--<li>--}}
                        {{--<a href="">--}}
                        {{--<i class="icon-align-center-horizontal"></i>--}}
                        {{--</a>--}}
                        {{--</li>--}}
                    </ul>
                </li>
            @endif

            @if (Auth::user()->tipo == 2 || Auth::user()->tipo == 3 ||Auth::user()->tipo == 5)
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <i class="icon-graph position-left"></i> Reportes <span class="caret"></span>
                    </a>
                    <ul class="dropdown-menu width-200">
                        <li><a href="{{route('reportes.crimen')}}">
                                <i class="icon-shield-notice"></i>Reporte de crimen</a></li>
                    </ul>
                </li>
            @endif

        </ul>
        <ul class="nav navbar-nav navbar-right">
            <li style="padding: 13px">
                <i class="icon-calendar2 position-left"></i>
                <span id="clock"></span>
            </li>
        </ul>
        <script>
          $('#clock').html(moment().format('DD/MM hh:mm:ss A'));
          setInterval(function() {
            $('#clock').html(moment().format('DD/MM hh:mm:ss A'));
          }, 1000);
        </script>
    </div>
</div>
<!-- /second navbar -->
