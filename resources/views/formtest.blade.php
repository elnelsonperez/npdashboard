@extends("layouts.main",[
'loadSelects' => true,
'loadFormatter' => true,
'breadcrumbs' => $breadCrumbs
])
@section("content")

    @component('components.formPanel')
        @slot('heading')
            Crear Nuevo Recurso
        @endslot
        @slot('body')
            <div class="row">
                <div class="col-xs-6">
                    @include('components.input',
                    [
                    'name' => 'name',
                    'title' => 'Nombre',
                    ])
                    @component('components.select', ['name' => 'hello', 'title' => 'Selecciona'])
                        <option value="">Hello</option>
                    @endcomponent
                </div>
            </div>

            @include('components.formButton',['link' => '/'])
        @endslot
    @endcomponent
@endsection