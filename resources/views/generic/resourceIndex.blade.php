@extends('components.resourceBase')
@section('heading', 'Listado de '. $title)
@section('elements')
    <a href="{{route($name.'.create')}}">
        <button type="button" class="btn bg-success-400 btn-labeled btn-rounded"><b>
                <i class="icon-plus-circle2"></i></b> Crear {{$title}}
        </button>
    </a>
@endsection
@section('body')

    @if (session('msg_success'))
        <div class="alert alert-success no-border">
            <button type="button" class="close" data-dismiss="alert">
                <span>×</span><span class="sr-only">Close</span></button>
            <span class="text-semibold"><i class="icon-check2"></i></span> {{session('msg_success')}}
        </div>
    @endif

    @if(session('msg_danger'))
        <div class="alert alert-danger no-border">
            <button type="button" class="close" data-dismiss="alert">
                <span>×</span><span class="sr-only">Close</span></button>
            <span class="text-semibold"><i class="icon-cross"></i></span> {{session('msg_danger')}}
        </div>
    @endif

    @if (isset($data))

        <table class="table table-striped table-responsive table-bordered table-condensed">
            <thead>
            <tr>
                @foreach($fields as $f)
                    <th>{{$f['title']}}</th>
                @endforeach
                <th style="text-align: center">Acciones</th>
            </tr>
            </thead>
            <tbody>
            @foreach($data as $row)
                <tr>
                    @foreach(array_keys($fields) as $field)
                        @if (isset($fieldCustom[$field]))
                            <td> {{call_user_func($fieldCustom[$field], $row)}}</td>
                        @else
                            <td>{{data_get($row, $field)}}</td>
                        @endif
                    @endforeach
                    <td style="width: 18px; text-align: center">
                        <a href="{{route($name.'.edit', $row->id)}}">
                            <span ><i class="icon-pencil"></i></span>
                        </a>

                        <form id="deleteform" style="display: inline-block; margin-left: 10px" action="{{route($name.'.destroy', $row->id)}}" method="post">
                            {{method_field('DELETE')}}
                            {{csrf_field()}}
                            <span class="text-danger-600" style="cursor: pointer" id="delete" > <i class="icon-trash"></i></span>
                        </form>
                        <script>
                          $('#delete').click(function(){
                            $("#deleteform").submit();
                          });
                        </script>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>

        <div style="  margin-top: 10px;text-align: center;">
            {{$data->links()}}
        </div>
    @endif
@endsection
