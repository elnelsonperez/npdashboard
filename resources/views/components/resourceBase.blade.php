@extends('layouts.main')
@section('title', $title)
@section('content')

    @if (isset($centered) && $centered == true)
        <div class="row">
            <div class="col-xs-6 col-xs-offset-3">
    @endif
    <div class="panel panel-default">
        <div class="panel-heading">
            <h2 class="panel-title">@yield('heading')</h2>
            <div class="heading-elements">
                @yield('elements')
            </div>
        </div>
        <div class="panel-body">
            @yield('body')
        </div>
    </div>
@if (isset($centered) && $centered == true)
            </div>
        </div>
    @endif
@endsection