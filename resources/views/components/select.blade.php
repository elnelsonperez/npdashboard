<div class="form-group{{ $errors->has($name) ? ' has-error' : '' }}">
    <label>{{$title}}:  @if(isset($required) && $required == true) <span style="color: red">*</span> @endif</label>
    <select name="{{$name}}" @if(isset($multiple)) multiple="multiple" @endif
    @if(isset($disabled)) disabled="disabled" @endif
    @if(isset($id)) id="{{$id}}" @endif
    @if(isset($search))data-live-search="true" @endif
            title="Seleccione..."
    class="bootstrap-select" data-width="100%">
        {{$slot}}
    </select>
    @if ($errors->has($name))
        <span class="help-block">
        <strong>{{ $errors->first($name) }}</strong>
    </span>
    @endif
    @if (isset($help))
        <span class="help-block">{{$help}}</span>
    @endif
</div>
