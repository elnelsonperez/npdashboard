<div class="panel panel-primary">
    <div class="panel-heading slim" style="background-color: #1976D2; border-color: #1976D2;">
        <h6 class="panel-title ">{{$heading or false}}</h6>
        <div class="heading-elements">
            {{$headingElements or false}}
        </div>
    </div>
    <div class="panel-body">
        @if(session('success'))
            <div class="alert alert-success alert-styled-left alert-arrow-left alert-bordered">
                <button type="button" class="close" data-dismiss="alert"><span>×</span><span class="sr-only">Close</span></button>
                {{session('success')}}
            </div>
        @endif
        {{$body or false}}
    </div>
</div>
