
<div class="form-group{{ $errors->has($name) ? ' has-error' : '' }}">
    <label>{{$title}}: @if(isset($required) && $required == true) <span style="color: red">*</span> @endif </label>
    <input @if(isset($id)) id="{{$id}}" @endif @if(isset($disabled)) disabled @endif type="{{$type or 'text'}}" name="{{$name or false}}"
           value="@if (isset($value)){{$value}}@elseif(old($name)){{old($name)}}@endif"
           class="form-control"  placeholder="{{$placeholder or 'Introduzca aqui'}}">
    @if ($errors->has($name))
    <span class="help-block">
        <strong>{{ $errors->first($name) }}</strong>
    </span>
    @endif
    @if (isset($help))
        <span class="help-block">{{$help}}</span>
    @endif
</div>

