@component('components.defaultPanel')
    @slot('heading')
        {{$heading or false}}
    @endslot
    @slot('headingElements')
        <ul class="icons-list">
            <li data-popup="tooltip" data-original-title="Regresar">
                <a href="{{url()->previous()}}">
                    <i class="icon-undo2"></i>
                </a>
            </li>
        </ul>
    @endslot
    @slot('body')
        {{$body or false}}
    @endslot
@endcomponent