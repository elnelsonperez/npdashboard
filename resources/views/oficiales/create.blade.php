@extends('layouts.main')
@section('title', 'Registro de Oficiales')
@section('styles')

@endsection
@section('content')
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            @component('components.formPanel', ['heading' => 'Registro de Oficiales'])
                @slot('body')

                    <form action="{{route('oficiales.store')}}" method="post">
                        {{csrf_field()}}
                        <div class="row">
                            <div class="col-xs-6">
                                @component('components.input',[
                                    'title' => 'Nombre',
                                    'name' => 'nombre'
                                    ])
                                @endcomponent
                            </div>
                            <div class="col-xs-6">
                                @component('components.input',[
                                   'title' => 'Apellido',
                                   'name' => 'apellido'
                                   ])
                                @endcomponent
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-xs-6">
                                @component('components.input',[
                                    'title' => 'Fecha de nacimiento',
                                    'name' => 'fecha_nacimiento',
                                    'type' => 'date'
                                    ])
                                @endcomponent
                            </div>
                            <div class="col-xs-6">
                                @component('components.input',[
                                    'title' => 'Fecha de ingreso',
                                    'name' => 'fecha_ingreso',
                                    'type' => 'date'
                                    ])
                                @endcomponent
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-xs-6">
                                @component('components.input',[
                                    'title' => 'Cédula',
                                    'name' => 'cedula',
                                    'type' => 'text',
                                    'id' => 'cedula',
                                    'help' => 'Formato: XXX-XXXXXXX-X'
                                    ])
                                @endcomponent
                            </div>
                            <div class="col-xs-6">
                                @component('components.select',[
                                    'title' => 'Sexo',
                                    'name' => 'sexo'
                                    ])
                                    <option @if (old('sexo') == '0') selected @endif value="0">Masculino</option>
                                    <option value="1">Femenino</option>
                                @endcomponent
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-xs-6">
                                @component('components.select',[
                                   'title' => 'Rango',
                                   'name' => 'rango_id',
                                        'search' => true
                                   ])
                                    @foreach($rangos as $r)
                                        <option @if( old('rango_id') == $r->id) selected @endif
                                        value="{{$r->id}}">{{$r->nombre}}</option>
                                    @endforeach
                                @endcomponent
                            </div>
                            <div class="col-xs-6">
                                @component('components.select',[
                                    'title' => 'Tipo',
                                    'name' => 'tipo_oficial_id',
                                    'search' => true
                                    ])
                                    @foreach($tipos as $t)
                                        <option @if( old('tipo_oficial_id') == $t->id) selected @endif
                                        value="{{$t->id}}">{{$t->nombre}}</option>
                                    @endforeach
                                @endcomponent
                            </div>
                        </div>
                        @component('components.formButton')@endcomponent
                    </form>
                @endslot
            @endcomponent
        </div>
    </div>

@endsection
@section('scripts')
    <script>
      $('#cedula').formatter({
        'pattern': '@{{999}}-@{{9999999}}-@{{9}}',

      });
    </script>
@endsection