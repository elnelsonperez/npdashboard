@extends('layouts.main')
@section('title', 'Reporte de Crimen')
@section('styles')
    <style href="{{asset('css/c3.css')}}" type="text/css"></style>
    <style>
        .daterangepicker.dropdown-menu {
            right: 200px !important;
        }
        #map {
            height: 520px;
        }
        @media print {
            .panel-body {page-break-inside: avoid;}
        }
    </style>
@endsection
@section('content')
    <div class="row">
        <div class="col-lg-12">
            <div class="row">
                <div class="col-xs-12" style="text-align: center">
                    <div class="form-group">
                        <button type="button" class="btn btn-block daterange-ranges" style="background-color: #1976D2; color: white">
                            <i class="icon-calendar22 position-left"></i> <span></span> <b class="caret"></b>
                        </button>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-6 col-md-3">
                    <div class="panel panel-body">
                        <div class="media no-margin">
                            <div class="media-body">
                                <h3 class="no-margin text-semibold">{{number_format($widgets['incidentes_count'])}}</h3>
                                <span class="text-uppercase text-size-mini text-muted">
                            Incidentes criminales</span>
                            </div>

                            <div class="media-right media-middle">
                                <i class="icon-shield-notice icon-3x text-blue-700"></i>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xs-6 col-md-3">
                    <div class="panel panel-body">
                        <div class="media no-margin">
                            <div class="media-body">
                                <h3 class="no-margin text-semibold">
                                    @if ($widgets['cambio'] > 0)
                                        <span class="icon-arrow-up-right2 text-danger-600"></span>
                                    @elseif ($widgets['cambio'] < 0)
                                        <span class="icon-arrow-down-right2 text-success-600"></span>
                                    @endif
                                    {{$widgets['cambio']}}%
                                </h3>
                                <span class="text-uppercase text-size-mini text-muted">Cambio periodo anterior</span>
                            </div>

                            <div class="media-right media-middle">
                                <i class="icon-statistics icon-3x text-blue-700"></i>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xs-6 col-md-3">
                    <div class="panel panel-body">
                        <div class="media no-margin">
                            <div class="media-body">
                                <h3 class="no-margin text-semibold">{{number_format($widgets['involucrados_count'])}}</h3>
                                <span class="text-uppercase text-size-mini text-muted">Personas involucradas</span>
                            </div>

                            <div class="media-right media-middle">
                                <i class="icon-users icon-3x text-blue-700"></i>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-xs-6 col-md-3">
                    <div class="panel panel-body">
                        <div class="media no-margin">
                            <div class="media-body">
                                <h3 class="no-margin text-semibold">{{number_format($widgets['tiempo_respuesta_prom'])}} minutos</h3>
                                <span class="text-uppercase text-size-mini text-muted">Tiempo de respuesta prom.</span>
                            </div>

                            <div class="media-right media-middle">
                                <i class="icon-watch2 icon-3x text-blue-700"></i>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12">

                    <div class="panel panel-primary">
                        <div class="panel-heading slim">
                            <h6 class="panel-title">
                                <span class="text-bold">Mapa de calor y puntos de concentración de incidentes criminales</span>
                            </h6>
                            <div class="heading-elements">
                                <ul class="icons-list">
                                    <li><a data-action="collapse" class=""></a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="panel-body" style="padding: 5px">
                            <div id="map">
                            </div>
                        </div>

                    </div>
                </div>
            </div>
            <div class="row" >
                <div class="col-sm-4 " >
                    <div class="panel panel-body text-center graficos">
                        <h6 class="text-semibold no-margin-bottom mt-5">Crímenes más comunes</h6>
                        <div class="text-size-small text-muted content-group-sm">Mostrando top 8</div>

                        <div class="svg-center" id="donut_tipos"></div>
                    </div>
                </div>
                <div class="col-sm-4 " >
                    <div class="panel panel-body text-center graficos">
                        <h6 class="text-semibold no-margin-bottom mt-5">Horas más peligrosas</h6>
                        <div class="text-size-small text-muted content-group-sm">Por cantidad de incidentes, top 8</div>

                        <div class="svg-center" id="count_hour"></div>
                    </div>
                </div>
                <div class="col-sm-4 ">
                    <div class="panel panel-body text-center graficos">
                        <h6 class="text-semibold no-margin-bottom mt-5">Días más peligrosos</h6>
                        <div class="text-size-small text-muted content-group-sm">Por cantidad de incidentes</div>

                        <div class="svg-center" id="count_day"></div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="panel panel-primary">
                        <div class="panel-heading slim">
                            <h6 class="panel-title">
                                <span class="text-bold">Sectores con mayor cantidad de incidentes (Top 10)</span>
                            </h6>
                            <div class="heading-elements">
                                <ul class="icons-list">
                                    <li><a data-action="collapse" class=""></a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="panel-body text-center graficos2">
                            <div id="sectores_chart"></div>
                        </div>
                    </div>

                </div>
                <div class="col-md-6">
                    <div class="panel panel-primary">
                        <div class="panel-heading slim">
                            <h6 class="panel-title">
                                <span class="text-bold">Tipo de incidente más frecuente por sector</span>
                            </h6>
                            <div class="heading-elements">
                                <ul class="icons-list">
                                    <li><a data-action="collapse" class=""></a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="panel-body text-center graficos2">
                            <div id="sectores_tipos_chart"></div>
                        </div>
                    </div>

                </div>
            </div>
            <div class="row">
                <div class="col-xs-12">
                    <div class="panel panel-primary">
                        <div class="panel-heading slim">
                            <h6 class="panel-title">
                                <span class="text-bold">Cantidad de incidentes atendidos por unidad</span>
                            </h6>
                            <div class="heading-elements">
                                <ul class="icons-list">
                                    <li><a data-action="collapse" class=""></a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="panel-body text-center graficos2">
                            <div id="count_unidad">
                            </div>
                        </div>
                    </div>

                </div>
            </div>
            <div class="row">
                <div class="col-xs-12">
                    <div class="panel panel-primary">
                        <div class="panel-heading slim">
                            <h6 class="panel-title">
                                <span class="text-bold">Tiempo de respuesta promedio por unidad</span>
                            </h6>
                            <div class="heading-elements">
                                <ul class="icons-list">
                                    <li><a data-action="collapse" class=""></a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="panel-body text-center graficos2">
                            <div id="prom_unidad">
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
    <script type="text/javascript" src="{{asset("assets/js/plugins/pickers/daterangepicker.js")}}"></script>
    <script type="text/javascript" src="{{asset("js/matchheight.js")}}"></script>
    <script type="text/javascript" src="{{asset("js/moment_es.js")}}"></script>
    <script type="text/javascript" src="{{asset('assets/js/plugins/visualization/d3/d3.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/js/plugins/visualization/d3/d3_tooltip.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/js/plugins/visualization/c3/c3latest.js')}}"></script>
    <script async defer
            src="https://maps.googleapis.com/maps/api/js?key={{env('GOOGLE_MAPS_API_KEY')}}
                    &libraries=visualization&callback=initMap">
    </script>
    <script>

      function initMap () {
        var map = new google.maps.Map(document.getElementById('map'), {
          zoom: 13,
          center: {lat: {{$center['lat']}}, lng: {{$center['lng']}}}
        });

        var points = []
        JSON.parse('{!! $heatmap['puntos'] !!}').forEach ( function (val) {

          points.push(new google.maps.LatLng(val))
        })

        heatmap = new google.maps.visualization.HeatmapLayer({
          data: points,
          map: map
        });

        heatmap.set('radius', 14);
        heatmap.set('opacity', .45);
      }
    </script>
    <script>
      moment.updateLocale('es');

      function getParameterByName(name, url) {
        if (!url) url = window.location.href;
        name = name.replace(/[\[\]]/g, "\\$&");
        var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
            results = regex.exec(url);
        if (!results) return null;
        if (!results[2]) return '';
        return decodeURIComponent(results[2].replace(/\+/g, " "));
      }

      var fromdate = getParameterByName('from')
      var todate = getParameterByName('to')

      var startDate = fromdate ? moment(fromdate) : moment().subtract(29,'days')
      var endDate = todate ? moment(todate) : moment()

      $('.daterange-ranges').daterangepicker(
          {
            startDate:  startDate,
            endDate: endDate,
            minDate: moment().subtract(1,'years'),
            maxDate: moment(),
            ranges: {
              'Hoy': [moment(), moment()],
              'Ayer': [moment().subtract(1,'days'), moment().subtract( 1,'days')],
              'Últimos 7 días': [moment().subtract( 6,'days',), moment()],
              'Últimos 30 días': [moment().subtract(29,'days', ), moment()],
              'Este mes': [moment().startOf('month'), moment().endOf('month')],
              'Mes anterior': [moment().subtract( 1,'month',).startOf('month'), moment().subtract(1,'month').endOf('month')]
            },
            opens: 'left',
            applyClass: 'btn-small bg-slate-600',
            cancelClass: 'btn-small btn-default',
            "locale": {
              "startLabel" : "Fecha inicial",
              "endLabel": "Fecha final",
              "format": "DD/MM/YYYY",
              "separator": " - ",
              "applyLabel": "Aplicar",
              "cancelLabel": "Cancelar",
              "fromLabel": "Desde",
              "toLabel": "Hasta",
              "customRangeLabel": "Personalizado",
              "daysOfWeek": [
                "Do",
                "Lu",
                "Ma",
                "Mi",
                "Ju",
                "Vi",
                "Sa"
              ],
              "monthNames": [
                "Enero",
                "Febrero",
                "Marzo",
                "Abril",
                "Mayo",
                "Junio",
                "Julio",
                "Agosto",
                "Septiembre",
                "Octubre",
                "Noviembre",
                "Diciembre"
              ],
              "firstDay": 1
            }
          },
          function(start, end) {
            window.location.replace("/reportes/crimen?from="+start.format('YYYY-MM-DD')+'&to='+end.format('YYYY-MM-DD'))
            $('.daterange-ranges span').html(start.format('MMMM D, YYYY') + ' &nbsp; - &nbsp; ' + end.format('MMMM D, YYYY'));
          }
      );
      // Display date format
      $('.daterange-ranges span').html(startDate.format('MMMM D, YYYY') + ' &nbsp; - &nbsp; ' + endDate.format('MMMM D, YYYY'));
    </script>
    <script>
      $('.graficos').matchHeight();

      var colors = [
        "#ef4e2f",
        "#EF5350",
        "#ef7c41",
        "#efb920",
        "#efc06d",
        "#efe66e",
        "#b3ef51",
        "#66BB6A",
        "#29B6F6",

      ]

      var sectores_chart_data = {!! json_encode($graphs['count_by_sector']) !!}
      var sectores_chart = c3.generate({
            bindto: "#sectores_chart",
            size : {   height: 550,},
            data: {
              colors: {
                'Cantidad de incidentes': "#ef7c41",
              },
              columns: [
                ['Cantidad de incidentes'].concat(sectores_chart_data.map(function (v) {
                  return v.cantidad;
                })),
              ],
              type: 'bar'
            },

            bar: {
              width: {
                ratio: 0.5 // this makes bar width 50% of length between ticks
              }
            },
            grid: {
              x: {
                show: true
              },
              y: {
                show: true
              }
            },
            zoom: {
              enabled: true
            },
            axis: {
              x: {
                type: 'category',
                categories: sectores_chart_data.map(function (v) {
                  return v.nombre;
                })
              },
              y: {
                tick: {
                  format: d3.format('d')
                }
              },
              rotated: true
            }
          });

      var count_unidad_data = {!! json_encode($graphs['count_by_unidad']) !!}
      var count_unidad = c3.generate({
            bindto: "#count_unidad",
            data: {
              colors: {
                'Cantidad de incidentes atendidos': "#66BB6A",
              },
              columns: [
                ['Cantidad de incidentes atendidos'].concat(count_unidad_data.map(function (v) {
                  return v.cantidad;
                })),
              ],
              type: 'bar'
            },

            bar: {
              width: {
                ratio: 0.5 // this makes bar width 50% of length between ticks
              }
            },
            grid: {
              x: {
                show: true
              },
              y: {
                show: true
              }
            },
            zoom: {
              enabled: true
            },
            axis: {
              x: {
                type: 'category',
                categories: count_unidad_data.map(function (v) {
                  return  'Unidad '+v.id;
                })
              },
              y: {
                tick: {
                  format: d3.format('d')
                },
                label: "Cantidad"
              }
            }
          });


      var prom_by_unidad_data = {!! json_encode($graphs['prom_by_unidad']) !!}
      var prom_by_unidad = c3.generate({
            bindto: "#prom_unidad",
            data: {
              colors: {
                'Tiempo Promedio': "#ef7c41",
              },
              columns: [
                ['Tiempo Promedio'].concat(prom_by_unidad_data.map(function (v) {
                  return v.tiempo_respuesta_prom;
                })),
              ],
              type: 'bar'
            },

            bar: {
              width: {
                ratio: 0.5 // this makes bar width 50% of length between ticks
              }
            },
            grid: {
              x: {
                show: true
              },
              y: {
                show: true
              }
            },
            zoom: {
              enabled: true
            },
            axis: {
              x: {
                type: 'category',
                categories: prom_by_unidad_data.map(function (v) {
                  return  'Unidad '+v.unidad_id;
                })
              },
              y: {
                tick: {
                  format: d3.format('d')
                },
                label: "Minutos"
              }
            }
          });

      var sectores_tipos_chart_data = {!! json_encode($graphs['count_by_type_sector']) !!}
      var sectores_tipos_chart = c3.generate({
            bindto: "#sectores_tipos_chart",
            size: {
              height: 550,
            },
            data: {
              colors: {
                'Cantidad de incidentes': "#ef7c41",
              },
              columns: [
                ['Cantidad de incidentes'].concat(sectores_tipos_chart_data.map(function (v) {
                  return v.cantidad;
                })),
              ],
              type: 'bar'
            },

            bar: {
              width: {
                ratio: 0.5 // this makes bar width 50% of length between ticks
              }
            },
            grid: {
              x: {
                show: true
              },
              y: {
                show: true
              }
            },
            zoom: {
              enabled: true,
              rescale: true

            },
            axis: {
              x: {
                type: 'category',
                categories: sectores_tipos_chart_data.map(function (v) {
                  return v.sector + " / " + v.tipo;
                }),
              },
              y: {
                tick: {
                  format: d3.format('d')
                }
              },
              rotated: true
            }
          });



      var donut_tipos = {!! json_encode($graphs['count_by_type']) !!}
          donut_tipos = donut_tipos.map(function(v, i) {
        return {
          "status": v.nombre,
          "value": v.cantidad,
          "color": colors[(i+1)%colors.length-1]
        }
      })

      var count_hour = {!! json_encode($graphs['count_by_hour']) !!}
          count_hour = count_hour.map(function(v, i) {
        return {
          "status": hourToRange(v.hora),
          "value": v.cantidad,
          "color": colors[(i+1)%colors.length-1]
        }
      })

      var count_day = {!! json_encode($graphs['count_by_day']) !!}
          count_day = count_day.map(function(v, i) {
        return {
          "status": v.dia,
          "value": v.cantidad,
          "color": colors[(i+1)%colors.length-1]
        }
      })


      function hourToRange(hour) {
        var H = hour
        var h = H % 12 || 12;
        var ampm = (H < 12 || H === 24) ? "AM" : "PM";
        return h + ":00 " + ampm +' - '+ h + ":59 " + ampm;
      }

      // Initialize chart
      animatedDonutWithLegend("#donut_tipos", 150,donut_tipos);
      animatedDonutWithLegend("#count_day", 150,count_day);
      animatedDonutWithLegend("#count_hour", 150, count_hour);

      // Chart setup
      function animatedDonutWithLegend(element, size, data) {

        // Add data set


        // Main variables
        var d3Container = d3.select(element),
            distance = 2, // reserve 2px space for mouseover arc moving
            radius = (size/2) - distance,
            sum = d3.sum(data, function(d) { return d.value; });


        // Create chart
        // ------------------------------

        // Add svg element
        var container = d3Container.append("svg");

        // Add SVG group
        var svg = container
            .attr("width", size)
            .attr("height", size)
            .append("g")
            .attr("transform", "translate(" + (size / 2) + "," + (size / 2) + ")");

        // Construct chart layout
        // ------------------------------

        // Pie
        var pie = d3.layout.pie()
            .sort(null)
            .startAngle(Math.PI)
            .endAngle(3 * Math.PI)
            .value(function (d) {
              return d.value;
            });

        // Arc
        var arc = d3.svg.arc()
            .outerRadius(radius)
            .innerRadius(radius / 2.5);


        //
        // Append chart elements
        //

        // Group chart elements
        var arcGroup = svg.selectAll(".d3-arc")
            .data(pie(data))
            .enter()
            .append("g")
            .attr("class", "d3-arc")
            .style({
              'stroke': '#fff',
              'stroke-width': 2,
              'cursor': 'pointer'
            });

        // Append path
        var arcPath = arcGroup
            .append("path")
            .style("fill", function (d) {
              return d.data.color;
            });


        // Add interactions
        arcPath
            .on('mouseover', function (d, i) {

              // Transition on mouseover
              d3.select(this)
                  .transition()
                  .duration(500)
                  .ease('elastic')
                  .attr('transform', function (d) {
                    d.midAngle = ((d.endAngle - d.startAngle) / 2) + d.startAngle;
                    var x = Math.sin(d.midAngle) * distance;
                    var y = -Math.cos(d.midAngle) * distance;
                    return 'translate(' + x + ',' + y + ')';
                  });

              // Animate legend
              $(element + ' [data-slice]').css({
                'opacity': 0.3,
                'transition': 'all ease-in-out 0.15s'
              });
              $(element + ' [data-slice=' + i + ']').css({'opacity': 1});
            })
            .on('mouseout', function (d, i) {

              // Mouseout transition
              d3.select(this)
                  .transition()
                  .duration(500)
                  .ease('bounce')
                  .attr('transform', 'translate(0,0)');

              // Revert legend animation
              $(element + ' [data-slice]').css('opacity', 1);
            });

        // Animate chart on load
        arcPath
            .transition()
            .delay(function(d, i) {
              return i * 500;
            })
            .duration(500)
            .attrTween("d", function(d) {
              var interpolate = d3.interpolate(d.startAngle,d.endAngle);
              return function(t) {
                d.endAngle = interpolate(t);
                return arc(d);
              };
            });


        //
        // Append counter
        //



        //
        // Append legend
        //

        // Add element
        var legend = d3.select(element)
            .append('ul')
            .attr('class', 'chart-widget-legend')
            .selectAll('li').data(pie(data))
            .enter().append('li')
            .attr('data-slice', function(d, i) {
              return i;
            })
            .attr('style', function(d, i) {
              return 'border-bottom: 2px solid ' + d.data.color;
            })
            .text(function(d, i) {
              return d.data.status + ': ';
            });

        // Add value
        legend.append('span')
            .text(function(d, i) {
              return d.data.value;
            });
      }

    </script>
@endsection