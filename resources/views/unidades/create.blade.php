@extends('layouts.main')
@section('title', 'Registro de Unidades')
@section('styles')

@endsection
@section('content')
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            @component('components.formPanel', ['heading' => 'Registro de Unidades'])
                @slot('body')

                    <form action="{{route('unidades.store')}}" method="post">
                        {{csrf_field()}}
                        <div class="row">
                            <div class="col-xs-6">
                                @component('components.select',[
                                    'name' => 'marca_id',
                                    'title' => 'Marca',
                                    'search' => true,
                                    'id' => 'marcas'
                                ])
                                    @foreach($marcas as $r)
                                        <option @if( old('marca') == $r->id) selected @endif
                                        value="{{$r->id}}">{{$r->nombre}}</option>
                                    @endforeach
                                @endcomponent
                            </div>
                            <div class="col-xs-6">
                                @component('components.select',[
                                    'name' => 'modelo_id',
                                    'title' => 'Modelo',
                                    'search' => true,
                                    'disabled' => true,
                                    'id' => 'modelos'
                                ])
                                @endcomponent
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-xs-6">
                                @component('components.select', [
                                     'name' => 'tipo',
                                     'title' => 'Tipo'
                                ])
                                    <option @if (old('tipo') == '0') selected @endif value="0">Automovil</option>
                                    <option @if (old('tipo') == '1') selected @endif  value="1">Motocicleta</option>
                                @endcomponent
                            </div>
                            <div class="col-xs-6">
                                @component('components.input', [
                                    'name' => 'placa',
                                    'title' => 'Placa'
                                ])
                                @endcomponent
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-6">
                                @component('components.select', [
                                     'name' => 'sector_id',
                                     'title' => 'Sector',
                                     'search' => true
                                ])
                                    @foreach($sectores as $r)
                                        <option @if( old('sector_id') == $r->id) selected @endif
                                        value="{{$r->id}}">{{$r->nombre}}, {{$r->municipio->nombre}}</option>
                                    @endforeach
                               @endcomponent
                            </div>
                            <div class="col-xs-6">
                                @component('components.input', [
                                    'name' => 'ano',
                                    'title' => 'Año',
                                    'type' => 'number'
                                ])
                                @endcomponent
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-6">
                                @component('components.select', [
                                     'name' => 'estado_unidad_id',
                                     'title' => 'Estado inicial'
                                ])
                                    @foreach($estados as $r)
                                        <option @if( old('estado_unidad_id') == $r->id) selected @endif
                                        value="{{$r->id}}">{{$r->nombre}}</option>
                                    @endforeach
                                @endcomponent
                            </div>
                            <div class="col-xs-6">
                                @component('components.select', [
                                        'name' => 'pms_hub_id',
                                        'title' => 'Hub'
                                   ])
                                    @foreach($hubs as $r)
                                        <option @if( old('estado_unidad_id') == $r->id) selected @endif
                                        value="{{$r->id}}">#{{$r->id}} Serial: {{$r->serial}}</option>
                                    @endforeach
                                @endcomponent
                            </div>
                        </div>
                        <div class="row">
                            <legend class="text-bold">Integrantes</legend>
                            <div class="col-xs-6">
                                @component('components.select',[
                                    'name' => 'supervisor',
                                    'title' => 'Supervisor',
                                    'search' => true
                                ])
                                    @foreach($supervisores as $r)
                                        <option @if( old('supervisor') == $r->id) selected @endif
                                        value="{{$r->id}}">{{$r->nombre}} {{$r->apellido}}, {{$r->rango->nombre}}</option>
                                    @endforeach
                                @endcomponent
                            </div>
                            <div class="col-xs-6">
                                @component('components.select',[
                                    'name' => 'miembros[]',
                                    'title' => 'Miembros',
                                    'multiple' => true,
                                    'search' => true
                                ])
                                    @foreach($miembros as $r)
                                        <option @if( old('miembros') && in_array($r->id, old('miembros'))) selected @endif
                                        value="{{$r->id}}">{{$r->nombre}} {{$r->apellido}}, {{$r->rango->nombre}}</option>
                                    @endforeach
                                @endcomponent
                            </div>
                        </div>



                        @component('components.formButton')@endcomponent
                    </form>
                @endslot
            @endcomponent
        </div>
    </div>

@endsection
@section('scripts')
    <script>
      $('#marcas').on('change', function () {
        var modelos =  $('#modelos');
        modelos.find('option').remove();
        modelos.attr('disabled', true);
        modelos.selectpicker('refresh');
        $.get('/api/modelos', {marca: $(this).val(), fields: ['id','nombre']}, function (res) {
          $.each(res, function(key, v) {
            $('#modelos')
                .append($("<option></option>")
                    .attr("value",v.id)
                    .text(v.nombre));
          });
          modelos.attr('disabled', false);
          modelos.selectpicker('refresh');
        })
      })
    </script>
@endsection