@extends('layouts.main')
@section('title', 'Configuración de Hubs')
@section('styles')

@endsection
@section('content')
    <div class="row">
        <div class="col-xs-12 col-sm-8 col-sm-offset-2">
            @component('components.defaultPanel', ['heading' => 'Configuración de Hubs'])
                @slot('headingElements')
                    <ul class="icons-list">
                        <li data-popup="tooltip" data-original-title="Regresar">
                            <a href="{{route('hub_config')}}">
                                <i class="icon-undo2"></i>
                            </a>
                        </li>
                    </ul>
                @endslot
                @slot('body')

                    @if (isset($configs))
                        <div class="row" style="text-align: center">
                            <div class="col-xs-12">
                                <h4>Configurando hub de unidad # {{$hub->unidad->id}} </h4>
                            </div>
                        </div>
                        <form action="{{route('hub_config.store')}}" method="post">
                            {{csrf_field()}}
                            <input type="hidden" name="hub_id" value="{{$hub->id}}">
                            <div class="row">
                                <legend class="text-bold">Configuraciones</legend>
                                <div class="col-xs-4">
                                    @component('components.input', [
                                        'name' => 'distanceBetweenLocations',
                                        'title' => 'Distancia entre localizaciones enviadas',
                                        'help' => 'Cada cuanto deberá el Hub enviar una localización',
                                        'type' => 'number',
                                        'placeholder' => 'Valor en cantidad de metros',
                                        'id' => 'distanceBetweenLocations',
                                        'value' => $configs['distanceBetweenLocations']
                                    ])@endcomponent
                                </div>
                                <div class="col-xs-4">
                                    @component('components.input', [
                                        'name' => 'timeoutSendLocation',
                                        'title' => 'Tiempo para mandar localización',
                                        'help' => 'Si la unidad esta estática, cada cuanto enviar una localización',
                                        'type' => 'number',
                                        'placeholder' => 'Valor en minutos',
                                        'id' => 'timeoutSendLocation',
                                         'value' => $configs['timeoutSendLocation']
                                    ])@endcomponent
                                </div>
                                <div class="col-xs-4">
                                    @component('components.select',[
                                      'name' => 'enabled',
                                      'title' => 'Habilitar o Deshabilitar Hub',
                                      'id' => 'enabled'
                                  ])
                                        <option @if($configs['enabled'] == '1') selected @endif
                                        value="1">Habilitado</option>
                                        <option @if($configs['enabled'] == '0') selected @endif value="0">Deshabilitado</option>
                                    @endcomponent

                                </div>
                            </div>
                            @component('components.formButton')@endcomponent
                        </form>

                    @else
                        <form action="{{route('hub_config')}}" method="GET">
                            {{csrf_field()}}
                            <div class="row">
                                <div class="col-xs-6 col-xs-offset-3">
                                    @component('components.select',[
                                        'name' => 'hub_id',
                                        'title' => 'PMS Hub a modificar',
                                        'search' => true,
                                        'id' => 'hubs'
                                    ])
                                        @foreach($hubs as $h)
                                            <option @if( old('hub_id') == $h->id) selected @endif
                                            value="{{$h->id}}">#{{$h->id}} - Unidad {{$h->unidad->id}},
                                                {{$h->unidad->modelo->marca->nombre}}
                                                {{$h->unidad->modelo->nombre}}
                                                {{$h->unidad->ano}}

                                            </option>
                                        @endforeach
                                    @endcomponent
                                </div>
                            </div>
                            @component('components.formButton')@endcomponent
                        </form>

                    @endif



                @endslot
            @endcomponent
        </div>
    </div>
@endsection
@section('scripts')
    <script>

    </script>
@endsection