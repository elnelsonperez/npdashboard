<!DOCTYPE html>
<html lang="en">
@section('title','Acceso')
@include('partials.header')
<body class="navbar-bottom login-container">

@include('partials.mainbar',['blank' => true])
<!-- Page container -->
<div class="page-container">

    <!-- Page content -->
    <div class="page-content">

        <!-- Main content -->
        <div class="content-wrapper">

            <!-- Simple login form -->
            <form method="POST" action="{{ route('login') }}">

                {{csrf_field()}}

                <div class="panel panel-body login-form">
                    <div class="text-center">
                        <div class="icon-object border-blue-700 text-blue-700">
                            <i class="icon-user-tie"></i></div>
                        <h5 class="content-group">Acceso al NP PMS<small class="display-block">
                                Ingrese sus credenciales</small></h5>
                    </div>

                    <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }} has-feedback has-feedback-left">
                        <input type="text" name="email" class="form-control" value="elnel@gmail.com" placeholder="Email">
                        @if ($errors->has('email'))
                            <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                        @endif
                        <div class="form-control-feedback">
                            <i class="icon-user text-muted"></i>
                        </div>
                    </div>

                    <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }} has-feedback has-feedback-left">
                        <input type="password" name="password" value="123456" class="form-control" placeholder="password">

                        @if ($errors->has('password'))
                            <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                        @endif
                        <div class="form-control-feedback">
                            <i class="icon-lock2 text-muted"></i>
                        </div>
                    </div>

                    <div class="form-group">
                        <button type="submit" class="btn btn-primary bg-blue-700 btn-block">Acceder
                            <i class="icon-circle-right2 position-right"></i></button>
                    </div>

                </div>
            </form>
            <!-- /simple login form -->

        </div>
        <!-- /main content -->

    </div>
    <!-- /page content -->

</div>
<!-- /page container -->

@include('partials.footer')

</body>
</html>
