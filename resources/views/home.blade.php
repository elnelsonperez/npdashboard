@extends('layouts.main')
@section('title', 'Portada')
@section('styles')
    <style>
        .container {
            display: flex;
            flex-direction: column;

            justify-content: center;
            align-items: center;
            align-self: center;
          padding-top: 30px;
          padding-bottom: 30px;
            margin-top: 50px;
            background-color: rgba(252,252,252,.6);
        }
        .title .thinner {

            font-weight: 300;
        }
        .title .bolder {
            font-weight: 600;
        }
        .title {
            font-size: 6em;
        }
        .welcome {
            font-size: 28px;
            font-weight: 300;
        }
        .subtitle {
            padding-top: 35px;
            font-size: 1.38em;
            font-weight: 300;
        }
        .type {
            font-size: 1.9em;
            color: #0082ef;
        }
    </style>
@endsection
@section('content')
    <div class="container">
        <div class="welcome">
            Bienvenido al
        </div>
        <div class="title">
            <span class="thinner">NP </span>
            <span class="bolder">PMS</span>
            <span class="thinner"> | Dashboard </span>
        </div>
        <div class="type">{{Auth::user()->tipoName()}}</div>
        <div class="subtitle" style="text-align: center">
            Seleccione una de las opciones del menú superior
        </div>
    </div>
@endsection

