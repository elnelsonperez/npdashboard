<!DOCTYPE html>
<html lang="es">
@include('partials.header')
<body class="navbar-bottom">

@include('partials.mainbar')
@include('partials.secondbar')

<!-- Page header -->
<div class="page-header" style="margin-bottom: 18px">
    @if (isset($breadcrumbs))
    <div class="breadcrumb-line">
        <ul class="breadcrumb">
            <li><a href="/"><i class="icon-home2 position-left"></i>Tablero</a></li>

                @foreach($breadcrumbs as $c)
                    <li  @if (isset($c['active']) && $c['active'] == true) class="active" @endif>
                        @if (!isset($c['active']) || $c['active'] == false)
                            <a href="{{$c['link'] or false}}">{{$c['text'] or false}}</a>
                        @else
                            {{$c['text'] or false}}
                        @endif
                    </li>
                @endforeach

        </ul>
        @endif
        {{--<ul class="breadcrumb-elements">--}}
            {{--<li><a href="#"><i class="icon-comment-discussion position-left"></i> Link</a></li>--}}
            {{--<li class="dropdown">--}}
                {{--<a href="#" class="dropdown-toggle" data-toggle="dropdown">--}}
                    {{--<i class="icon-gear position-left"></i>--}}
                    {{--Dropdown--}}
                    {{--<span class="caret"></span>--}}
                {{--</a>--}}

                {{--<ul class="dropdown-menu dropdown-menu-right">--}}
                    {{--<li><a href="#"><i class="icon-user-lock"></i> Account security</a></li>--}}
                    {{--<li><a href="#"><i class="icon-statistics"></i> Analytics</a></li>--}}
                    {{--<li><a href="#"><i class="icon-accessibility"></i> Accessibility</a></li>--}}
                    {{--<li class="divider"></li>--}}
                    {{--<li><a href="#"><i class="icon-gear"></i> All settings</a></li>--}}
                {{--</ul>--}}
            {{--</li>--}}
        {{--</ul>--}}
    </div>

</div>
<!-- /page header -->

<!-- Page container -->
<div class="page-container">

    <!-- Page content -->
    <div class="page-content">
        <!-- Main content -->
        <div class="content-wrapper">

            @yield('content')

        </div>
        <!-- /main content -->

    </div>
    <!-- /page content -->

</div>
<!-- /page container -->


@include('partials.footer')
@yield('scripts')
<script>
  // Basic select
  $('.bootstrap-select').selectpicker();
  $('a.notop').click(function(e)
  {
    e.preventDefault();
  });
</script>

</body>

</html>
