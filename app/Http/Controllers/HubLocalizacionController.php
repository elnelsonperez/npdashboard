<?php

namespace App\Http\Controllers;

use App\Events\HubLocalizacionCreated;
use App\Events\HubLocalizacionesReceived;
use App\Models\HubLocalizacion;
use App\Models\PmsHub;
use App\Models\Unidad;
use App\Shared\HandlesLocations;
use Carbon\Carbon;
use Grimzy\LaravelMysqlSpatial\Types\Point;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class HubLocalizacionController extends  Controller
{

    use HandlesLocations;

    public function store (Request $request) {

        $validator = Validator::make($request->all(),  [
            'hub_serial' => 'required|exists:pms_hubs,serial',
            'processed_on' => 'required',
            'locations' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json([
                'errors' => $validator->errors()
            ],422);
        }
        $data = $validator->valid();
        /**
         * @var $hub PmsHub
         */
        $hub = PmsHub::where('serial', $data['hub_serial'])->get()->first();
        $hub->unidad->triggerLastUpdate();
        $destacamento_id = $hub->unidad->destacamento->id;
        $arr = [];
        foreach ($data['locations'] as $loc) {
            $location = new HubLocalizacion();
            $location->fecha_obtenida = $loc['time'];
            $location->ubicacion = new Point($loc['lat'],$loc['lng']);
            $location->pms_hub_id = $hub->id;
            $location->save();
            $arr[] = $location;
        }

        broadcast(new HubLocalizacionesReceived($arr,$destacamento_id));
        return response()->json(null,201);
    }

    public function getEvenlySpacedLocationsSnapped (Request $r) {

        $data = $this->validate($r,[
            'unidad_id' => 'required|exists:unidades,id',
            'metros' => 'required',
            'limite' => 'sometimes|required'
        ]);

        $limite = 20;
        if (isset($data['limite'])) {
            $limite = $data['limite'];
        }

        $locations = Unidad::find($data['unidad_id'])
            ->localizaciones()
            ->orderByDesc('fecha_obtenida')
            ->limit(150)->get();


        if (count($locations) > 1) {
            $result = [];
            foreach ($locations as $l) {
                if (count($result) <= $limite) {
                    $last = end($result);
                    if ($last) {
                        $distance = $this->distanceBetweenPoints($l->ubicacion,$last->ubicacion);
                        if ($distance >= $data['metros']) {
                            $result[] = $l;
                        }
                    } else {
                        $result[] = $l;
                    }
                }
            }

            return $this->requestSnappedPoints($result);
        }
        return null;
    }

    private function requestSnappedPoints ($locations) {

        $locs = [];
        foreach ($locations as $loc) {
            /**
             * @var $ubi Point
             */
            $ubi = $loc->ubicacion;
            $locs[] = $ubi->getLat().','.$ubi->getLng();
        }
        $locs = implode("|",$locs);

        if ($locs) {
            $client = new Client();
            $response = $client->get("https://roads.googleapis.com/v1/snapToRoads", [
                'query' => [
                    'key' => env('GOOGLE_MAPS_API_KEY'),
                    'path' => $locs
                ]
            ]);
            foreach (json_decode($response->getBody()->getContents())->snappedPoints as $p) {
                if (isset($locations[$p->originalIndex])) {
                    $locations[$p->originalIndex]->snapped = new Point($p->location->latitude,$p->location->longitude);
                }
            }
            return $locations;
        }

        return null;
    }
}