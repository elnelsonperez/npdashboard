<?php

namespace App\Http\Controllers;

use App\Events\MessageReceived;
use App\Events\MessagesUpdated;
use App\Models\EstadoMensajeHub;
use App\Models\MensajeHub;
use App\Models\Oficial;
use App\Models\PmsHub;
use App\Models\TipoOficial;
use App\Models\Unidad;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Pipeline\Hub;
use Illuminate\Support\Facades\Auth;
use Validator;

class MensajeController extends Controller
{

    public function apiGetMensajes(Request $request) {
        $validator = Validator::make($request->all(),  [
            'hub_serial' => 'required|exists:pms_hubs,serial',
            'processed_on' => 'required',
            'today' => 'sometimes|required',
            'from' => 'sometimes|required',
            'to' => 'sometimes|required'
        ]);

        if ($validator->fails()) {
            return response()->json([
                'errors' => $validator->errors()
            ],422);
        }

        $data = $validator->valid();

        /**
         * @var $hub PmsHub
         */
        $hub = PmsHub::where('serial', $data['hub_serial'])->get()->first();

        if (!$hub) {
            return response()->json([
                'errors' => [
                    'serial' => 'Serial no encontrado en DB'
                ]
            ],422);
        }

        $unidad = $hub->unidad;
        if (!$unidad) {
            return response()->json([
                'errors' => [
                    'unidad' => 'Hub no asociado a ninguna unidad'
                ]
            ],422);
        }

        $from = null;
        $to = null;
        if (isset($data['today']) && $data['today']) { //Fetch today messages
            $from = Carbon::today()->toDateTimeString();
            $to = Carbon::tomorrow()->toDateTimeString();
        }
        if (isset($data['from']) && $data['from']) {
            $from = $data['from'];
        }

        if (isset($data['to']) && $data['to']) {
            $to = $data['to'];
        }

        $mensajes = $this->getMensajesDeUnidad($unidad, $from,$to);
        $unidad->triggerLastUpdate();
        return $mensajes;
    }

    private function getMensajesDeUnidad (Unidad $u, $from = null, $to = null) {
        $query = MensajeHub::join('oficiales as o','o.id','mensajes_hubs.oficial_unidad_id')
            ->where('o.unidad_id', $u->id)
            ->where('o.tipo_oficial_id', TipoOficial::SUPEVISOR)
            ->withCellParams();

        if (!is_null($from)) {
            $query = $query->where('mensajes_hubs.actualizado_en', '>', $from);
        }

        if (!is_null($to)) {
            $query = $query->where('mensajes_hubs.actualizado_en', '<=', $to);
        }

        return $query->orderByDesc('mensajes_hubs.creado_en')->simplePaginate(10);
    }

    public function apiCrearMensaje (Request $request) {
        $validator = Validator::make($request->all(),  [
            'hub_serial' => 'required|exists:pms_hubs,serial',
            'processed_on' => 'required',
            'oficial_unidad_id' => 'required',
            'contenido' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json([
                'errors' => $validator->errors()
            ],422);
        }

        $data = $validator->valid();

        $msg = MensajeHub::create([
            'oficial_unidad_id' => Oficial::find($data['oficial_unidad_id'])->id,
            'contenido' => $data['contenido'],
            'pms_hub_id' => PmsHub::where('serial',$data['hub_serial'])->get()->first()->id,
            'sentido' => MensajeHub::UNIDAD_A_CUARTEL,
            'estado_mensaje_hub_id' => EstadoMensajeHub::ENTREGADO
        ]);

        $destacamentp = $msg->hub->unidad->destacamento->id;
        broadcast(new MessageReceived($msg,$destacamentp));

        $response = MensajeHub::where('id',$msg->id)->withCellParams()->get()->first();
        return response()->json($response,201);
    }

    public function marcarComoLeido (Request $r) {
        $data = $this->validate($r, [
            'mensajes' => 'required'
        ]);
        MensajeHub::whereIn('id',$data['mensajes'])
            ->update(['estado_mensaje_hub_id' => EstadoMensajeHub::LEIDO]);

        return MensajeHub::whereIn('id',$data['mensajes'])->withCommon()->get();
    }

    public function apiChangeStatus (Request $request) {
        $validator = Validator::make($request->all(),  [
            'hub_serial' => 'required|exists:pms_hubs,serial',
            'processed_on' => 'required',
            'mensajes_ids' => 'required',
            'estado_id' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json([
                'errors' => $validator->errors()
            ],422);
        }
        $data = $validator->valid();
        $destacamento = PmsHub::where('serial',$data['hub_serial'])->get()->first()->unidad->destacamento->id;

        MensajeHub::whereIn('id',$data['mensajes_ids'])
            ->update(['estado_mensaje_hub_id' => $data['estado_id']]);

        broadcast(new MessagesUpdated($data['mensajes_ids'],$destacamento));
        return response()->json(null,200);
    }

    public function store (Request $r) {
        $data = $this->validate($r, [
            'oficial_unidad_id' => 'required|exists:oficiales,id',
            'contenido' =>'required'
        ]);

        $oficial = Oficial::find($data['oficial_unidad_id']);
        $msg = MensajeHub::create([
            'oficial_unidad_id' => $oficial->id,
            'usuario_id' => Auth::user()->id,
            'contenido' => $data['contenido'],
            'pms_hub_id' => $oficial->unidad->hub->id,
            'sentido' => MensajeHub::CUARTEL_A_UNIDAD,
            'estado_mensaje_hub_id' => EstadoMensajeHub::ENVIADO
        ]);

        return $msg->load($msg->getCommonRelations());
    }

}