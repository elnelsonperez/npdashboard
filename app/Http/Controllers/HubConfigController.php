<?php

namespace App\Http\Controllers;

use App\Models\HubConfig;
use App\Models\PmsHub;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class HubConfigController extends Controller
{

    const defaults = [
        'distanceBetweenLocations' => 15,
        'timeoutSendLocation' => 5,
        'enabled' => true
    ];

    private function configsToSimpleArray ($configs) {
        $result = [];
        foreach ($configs as $c) {
            $val  = $c['value'];
            settype($val, $c['type']);
            $result[$c['key']] = $val;

        }
      return $result;
    }


    public function index (Request $request) {

        $this->validate($request, [
            'hub_id' => 'sometimes|exists:pms_hubs,id'
        ]);

        if ($request->has('hub_id')) {
            $hub = PmsHub::find($request->get('hub_id'));
            $configs = $hub->configs;
            if ($configs) {
                $configs = array_merge(self::defaults,
                    $this->configsToSimpleArray($configs->filter(function ($value) {
                    return in_array($value->key,self::defaults);
                })->toArray()));
            } else {
                $configs = self::defaults;
            }

            return view('hubconfig.index',[
                'configs' => $configs,
                'hub' => $hub
            ]);
        } else {
            $hubs = PmsHub::with(['unidad.modelo.marca', 'unidad.modelo'])->get();
            return view('hubconfig.index',[
                'hubs' => $hubs
            ]);
        }

    }

    public function store (Request $request) {
        $data = $this->validate($request, [
            'hub_id' => 'exists:pms_hubs,id',
            'distanceBetweenLocations' => 'required|numeric',
            'timeoutSendLocation' => 'required|numeric',
            'enabled' => 'required'
        ]);


        $this->updateOrCreateConfig(
            'distanceBetweenLocations',
            $data['distanceBetweenLocations'],
            'int',
            $data['hub_id']
        );

        $this->updateOrCreateConfig(
            'timeoutSendLocation',
            $data['timeoutSendLocation'],
            'int',
            $data['hub_id']
        );

        $this->updateOrCreateConfig(
            'enabled',
            $data['enabled'],
            'boolean',
            $data['hub_id']
        );

        $request->session()->flash('success', 'Configuración actualizada correctamente.');
        return redirect()->back();

    }

    private function updateOrCreateConfig ($key,$value,$type,$hub) {
        $config = HubConfig::where('key',$key)->where('pms_hub_id',$hub)->get()->first();
        if ($config) {
            $config->update(['value' => $value, 'type' => $type]);
        } else {
            HubConfig::create([
                'key' => $key,
                'value' => $value,
                'type' => $type,
                'pms_hub_id' => $hub
            ]);
        }
    }

    public function getHubConfig (Request $request) {

        $validator = Validator::make($request->all(),  [
            'hub_serial' => 'required|exists:pms_hubs,serial',
            'force' => 'required',
            'processed_on' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json([
                'errors' => $validator->errors()
            ],422);
        }
        $data = $validator->valid();
        /**
         * @var $hub PmsHub
         */

        $hub = PmsHub::where('serial', $data['hub_serial'])->get()->first();

        if (!$hub) {
            return response()->json([
                'errors' => [
                    'serial' => 'Serial no encontrado en DB'
                ]
            ],422);
        }

        $unidad = $hub->unidad;
        if (!$unidad) {
            return response()->json([
                'errors' => [
                    'unidad' => 'Hub no asociado a ninguna unidad'
                ]
            ],422);
        }


        $oficiales = $hub->unidad->oficiales;
        if (!$oficiales) {
            return response()->json([
                'errors' => [
                    'oficiales' => 'No hay oficiales asignados a esta unidad'
                ]
            ],422);
        }


        if ($data['force'] == true) {
            $configs = $hub->configs;
            if ($configs) {
                $configs = array_merge(self::defaults,$this->configsToSimpleArray($configs->filter(function ($value) {
                    return in_array($value->key,self::defaults);
                })->toArray()));
            } else {
                $configs = self::defaults;
            }
            $hub->dataPendiente()->update(['configs' => 0, 'configs_ultimo_pull' => Carbon::now()]);
        }
        else {
            if ($hub->dataPendiente->configs == true) {
                $configs = $hub->configs;
                if ($configs) {
                    $configs = array_merge(self::defaults,$this->configsToSimpleArray($configs->filter(function ($value) {
                        return in_array($value->key,self::defaults);
                    })->toArray()));
                } else {
                    $configs = self::defaults;
                }
                $hub->dataPendiente()->update(['configs' => 0, 'configs_ultimo_pull' => Carbon::now()]);
            } else {
                return response()->json(null,204);
            }
        }


        $ofis = $oficiales->load([
            'flota',
            'ibutton' => function($q) {
                $q->select(['id', 'oficial_id','code']);
            }
        ]);

        $configs['oficiales'] = $ofis->map(function ($user) {
            return collect($user->toArray())
                ->only(['id', 'nombre', 'apellido','ibutton'])
                ->all();
        });

        $configs['allowedMacAddresses'] = [];
        $configs['ibuttons'] = [];
        foreach ($ofis as $o) {
            if ($o->flota->mac_address) {
                $configs['allowedMacAddresses'][] = $o->flota->mac_address;
                if ($o->ibutton) {
                    $configs['ibuttons'][$o->flota->mac_address] = $o->ibutton->code;
                }
            }
        }

        $configs['destacamento'] = $unidad->destacamento->only(['nombre','id','ubicacion']);
        $configs['sector'] = $unidad->sector->only(['limites','id','nombre']);

        $unidad->triggerLastUpdate();
        return $configs;
    }

}
