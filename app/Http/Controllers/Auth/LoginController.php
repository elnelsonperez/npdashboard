<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{


    public function show () {
        return view('auth.login');
    }

    private function validateLogin(Request $request)
    {
        return $this->validate($request, [
            'email' => 'required|string',
            'password' => 'required|string',
        ]);
    }

    public function login(Request $request)
    {

        $data = $this->validateLogin($request);

        if (Auth::attempt(['email' => $data['email'], 'password' => $data['password']])) {
            // Authentication passed...
            return redirect('/');
        } else {
            return redirect()->back()->withErrors(['email' => 'Datos de autenticacion incorrectos']);
        }
    }

    public function logout()
    {
        Auth::logout();
        return redirect('/login');
    }
}
