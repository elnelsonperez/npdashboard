<?php

namespace App\Http\Controllers;

use App\Events\UnidadCreated;
use App\Models\EstadoIncidenciaCivil;
use App\Models\EstadoUnidad;
use App\Models\HubLocalizacion;
use App\Models\IncidenciaCivil;
use App\Models\Marca;
use App\Models\Oficial;
use App\Models\PmsHub;
use App\Models\PrioridadIncidenciaCivil;
use App\Models\Sector;
use App\Models\TipoOficial;
use App\Models\Unidad;
use App\Shared\HandlesLocations;
use CiroVargas\GoogleDistanceMatrix\GoogleDistanceMatrix;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class UnidadController extends Controller
{

    use HandlesLocations;

    public function index () {
        $name = 'unidades';
        $title = 'Unidades';

        $data = Unidad::with([
            'modelo',
            'modelo.marca',
            'estado',
            'sector',
            'oficiales'
        ])->paginate(20);

        $fields = [
            'id' => [
                'title' => '#'
            ],
            'vehiculo' => [
                'title' => 'Vehículo'
            ],
            'eltipo' => [
                'title' => 'Tipo'
            ],
            'placa' => [
                'title' => 'Placa'
            ],
            'estado.nombre' => [
                'title' => 'Estado'
            ],
            'sector.nombre' => [
                'title' => 'Sector'
            ],
            'creado_en' => [
                'title' => 'Fecha registro'
            ],
        ];

        $custom = [
            'vehiculo' => function ($r) {
                return $r->modelo->marca->nombre.' '.$r->modelo->nombre.' '.$r->ano;
            },
            'eltipo' => function ($r) {
                return $r->tipo == 0 ? "Automovil" : 'Motocicleta';
            },
        ];

        return view('generic.resourceIndex')->with([
            'name' => $name,
            'title' => $title,
            'data' => $data,
            'fields' => $fields,
            'fieldCustom' => $custom
        ]);
    }


    public function create () {
        return view('unidades.create', [
            'miembros' => Oficial::doesntHave('unidad')
                ->where('tipo_oficial_id', TipoOficial::MIEMBRO)->orderBy('nombre')->get(),
            'supervisores' => Oficial::doesntHave('unidad')
                ->where('tipo_oficial_id', TipoOficial::SUPEVISOR)
                ->orderBy('nombre')->get(),
            'marcas' => Marca::orderBy('nombre')->get(),
            'sectores' => Sector::with('municipio')->orderBy('sectores.nombre')->get(),
            'estados' => EstadoUnidad::all(),
            'hubs' => PmsHub::doesntHave('unidad')->get()
        ]);
    }

    public function store (Request $r) {
        $data = $this->validate($r, [
            'marca_id' => 'required',
            'modelo_id' => 'required',
            'tipo' => 'required',
            'placa' => 'required',
            'sector_id' => 'required',
            'ano' => 'required',
            'supervisor' => 'required',
            'miembros' => 'required',
            'estado_unidad_id' => 'required',
            'pms_hub_id' => 'required'
        ]);

        $oficiales = [];
        foreach ($data['miembros'] as $m) {
            $oficiales[] = $m;
        }
        $oficiales[] = $data['supervisor'];
        unset($data['miembros']);
        unset($data['supervisor']);
        unset($data['marca_id']);
        unset($data['serial']);
        $data['destacamento_id'] = Auth::user()->destacamento()->id;
        $unidad = Unidad::create($data);

        $unidad->oficiales()->saveMany(Oficial::find($oficiales));

        broadcast(new UnidadCreated($unidad,$data['destacamento_id']));

        $r->session()->flash('success', 'Unidad registrada correctamente.');
        return redirect()->back();
    }

    public function unidadesParaIncidente (Request $r ) {
        $r->validate([
            'incidente_id' => 'required|numeric|exists:incidencias_civiles,id'
        ]);

        /**
         * @var $unidades Collection
         */
        $unidades = Auth::user()
            ->destacamento()
            ->unidades()
            ->whereNotIn('id',
                IncidenciaCivil::
                where (function ($q) {
                    $q->where('prioridad_id',PrioridadIncidenciaCivil::URGENTE);
                    $q->orWhere('prioridad_id',PrioridadIncidenciaCivil::ALTA);
                })->where(function ($q) {
                    $q->where('estado_id', EstadoIncidenciaCivil::EN_CURSO)
                        ->orWhere('estado_id',EstadoIncidenciaCivil::ASIGNADA);
                })->join('unidades_incidencias as ui','ui.incidencia_id','=','incidencias_civiles.id')
                    ->select('unidad_id')->distinct()->get()->toArray()
            )
            ->has('oficiales')
            ->where('estado_unidad_id',
                EstadoUnidad::DISPONIBLE)
            ->commonWith();


        $incidente = IncidenciaCivil::find($r->get('incidente_id'));

        if (
            $incidente->prioridad_id === PrioridadIncidenciaCivil::URGENTE ||
            $incidente->prioridad_id === PrioridadIncidenciaCivil::ALTA
        ) {
            $unidades = $unidades->whereNotIn('id',
                IncidenciaCivil::
                    where('estado_id', EstadoIncidenciaCivil::EN_CURSO)
                    ->orWhere('estado_id',EstadoIncidenciaCivil::ASIGNADA)
                    ->join('unidades_incidencias as ui','ui.incidencia_id','=','incidencias_civiles.id')
                    ->select('unidad_id')->distinct()->get()->toArray()
            );
        }

        $unidades = $unidades->get();

        $chunks = $unidades->chunk(20);
        $i = 0;

        foreach ($chunks as $unids) {
            $gmr = new GoogleDistanceMatrix(env('GOOGLE_MAPS_API_KEY'));
            $req = $gmr
                ->setLanguage('cs')
                ->setMode(GoogleDistanceMatrix::MODE_DRIVING);

            foreach ($unids as $unidad) {
                $loc = $unidad->localizacionActual();
                $req->addOrigin(
                    $loc->getLat().','.$loc->getLng()
                );
            }

            $req->addDestination(
                $incidente->ubicacion->getLat().','.
                $incidente->ubicacion->getLng()
            );

            try {
                $response = $req->sendRequest();
                foreach ($response->getRows() as $r) {
                    $unidades->get($i)->distancia = $r->getElements()[0]->getDistance()->getValue();
                    $unidades->get($i)->tiempo = $r->getElements()[0]->getDuration()->getValue();
                    $i++;
                }
            }
            catch (\Exception $e) {
                foreach ($unidades as $unidad) {
                    $unidad->tiempo = null;
                    $unidad->distancia = $this->distanceBetweenPoints(
                        $unidad->localizacionActual(),
                        $incidente->ubicacion);
                }
            }

        }
        return $unidades;
    }

    public function obtenerUnidadesFueraDeSector () {

        $unidades = DB::table('unidades')
            ->whereRaw("exists (select * from `sectores` where `unidades`.`sector_id` = `sectores`.`id`)")
            ->join('pms_hubs as ph','unidades.id','ph.unidad_id')
            ->join(\DB::raw("
            (   
                SELECT pll.*
                FROM pms_hubs_localizaciones pll
                INNER JOIN (
                   SELECT pms_hub_id, max(fecha_obtenida) max_fecha
                   FROM pms_hubs_localizaciones 
                   GROUP BY pms_hub_id
                ) a on (a.pms_hub_id = pll.pms_hub_id and a.max_fecha = pll.fecha_obtenida)
            ) z
            "), function ($j) {
                $j->on('ph.id','=','z.pms_hub_id');
            })
            ->join('sectores as s','s.id','=','unidades.sector_id')
            ->whereNotNull('s.limites')
            ->whereRaw("ST_CONTAINS(s.limites,z.ubicacion) = 0")
            ->select([
                'unidades.id',
                'z.id as ubicacion_id',
                's.id as sector_id'])->get();
        $result = [];

        foreach ($unidades as $u) {
            $lowest = null;
            $loc = HubLocalizacion::where('id',$u->ubicacion_id)->get()->first()->ubicacion;
            $limites = Sector::where('id',$u->sector_id)->get()->first()->limites;
            $dist = $this->getDistanceFromPolygon($limites,$loc);
            array_push(
                $result,
                ['localizacion' => $loc,
                    'unidad_id' => $u->id,
                    'distancia' => $dist]);
        }

        return collect($result);
    }




}