<?php

namespace App\Http\Controllers;

use App\Models\PmsHub;
use App\Models\Unidad;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Validator;

class DataPullingController extends Controller
{

    public function getHubData (Request $request) {
        $validator = Validator::make($request->all(),  [
            'hub_serial' => 'required|exists:pms_hubs,serial',
            'processed_on' => 'required',
            'mensajes.today' => 'sometimes|required',
            'mensajes.from' => 'sometimes|required',
            'mensajes.to' => 'sometimes|required'
        ]);

        if ($validator->fails()) {
            return response()->json([
                'errors' => $validator->errors()
            ],422);
        }

        $data = $validator->valid();

        /**
         * @var $hub PmsHub
         */
        $hub = PmsHub::where('serial', $data['hub_serial'])->get()->first();

        if (!$hub) {
            return response()->json([
                'errors' => [
                    'serial' => 'Serial no encontrado en DB'
                ]
            ],422);
        }

        $unidad = $hub->unidad;
        if (!$unidad) {
            return response()->json([
                'errors' => [
                    'unidad' => 'Hub no asociado a ninguna unidad'
                ]
            ],422);
        }

        $response = [];
        $response['mensajes'] = $this->apiGetMensajes($data,$unidad);
        $response['incidencias'] = $this->apiGetIncidencias($data,$unidad);
        return response()->json($response,200);
    }

    public function apiGetIncidencias (array $data, Unidad $unidad) {

    }

    public function apiGetMensajes(array $data, Unidad $unidad) {

        $from = null;
        $to = null;
        if (isset($data['today']) && $data['today']) { //Fetch today messages
            $from = Carbon::today()->toDateTimeString();
            $to = Carbon::tomorrow()->toDateTimeString();
        }
        if (isset($data['from']) && $data['from']) {
            $from = $data['from'];
        }

        if (isset($data['to']) && $data['to']) {
            $to = $data['to'];
        }

        $mensajes = $this->getMensajesDeUnidad($unidad, $from,$to);
        return $mensajes;

    }

    private function getMensajesDeUnidad (Unidad $u, $from = null, $to = null) {
        $query = MensajeHub::join('oficiales as o','o.id','mensajes_hubs.oficial_unidad_id')
            ->where('o.unidad_id',$u->id)
            ->withCellParams();

        if (!is_null($from)) {
            $query = $query->where('mensajes_hubs.creado_en', '>', $from);
        }

        if (!is_null($to)) {
            $query = $query->where('mensajes_hubs.creado_en', '<=', $to);
        }

        return $query->get();
    }

}
