<?php

namespace App\Http\Controllers;

use App\Services\ReporteCrimenService;
use Carbon\Carbon;
use Illuminate\Http\Request;

class ReporteCrimenController extends Controller
{

    public function index (Request $request, ReporteCrimenService $service) {

        $from = Carbon::now()->subDays(29)->toDateString();
        $to = Carbon::now()->toDateString();

        if ($request->has('from')) {
            $from = $request->get('from');
        }

        if ($request->has('to')) {
            $to = $request->get('to');
        }


        return view('reportes.crimen' , [
            'widgets' => [
                'incidentes_count' => $service->incidentesCount($from, $to),
                'involucrados_count' => $service->involucradosCount($from, $to),
                'cambio' => $service->cambioPeriodoAnterior($from, $to),
                'tiempo_respuesta_prom' => $service->tiempoDeRespuestaProm($from,$to)
            ],
            'graphs' => [
                'count_by_type' => $service->incidentesCountByType($from,$to,8),
                'count_by_sector' => $service->incidentesCountBySector($from,$to),
                'count_by_unidad' => $service->incidentesCountByUnidad($from,$to),
                'count_by_type_sector' => $service->incidentesCountByTypeAndSector($from, $to,10),
                'count_by_hour' => $service->incidentesCountByHour($from,$to,8),
                'count_by_day' => $service->incidentesCountByDay($from,$to),
                'count_by_day_hour' => $service->incidentesCountByDayHour($from, $to),
                'prom_by_unidad' => $service->tiempoDeRespuestaByUnidad($from,$to)
            ],
            'heatmap' => [
                'puntos' => json_encode($service->getLocationsAsLatLngArray($from, $to))
            ],
            'center' => [
                'lat' => 19.452513,
                'lng' => -70.679210
            ]
        ]);
    }

}