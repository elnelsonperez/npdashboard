<?php

namespace App\Http\Controllers;

use App\Models\Destacamento;
use App\Models\EstadoIncidenciaCivil;
use App\Models\EstadoUnidad;
use App\Models\MensajeHub;
use App\Models\Municipio;
use App\Models\Oficial;
use App\Models\PrioridadIncidenciaCivil;
use App\Models\TipoIncidenciaCivil;
use App\Models\Unidad;
use Illuminate\Support\Facades\Auth;

class DashboardController extends Controller
{

    public function indexDashboardPatrullas () {

        /**
         * @var $dest Destacamento
         */
        $dest = Auth::user()->destacamento();
        if (is_null($dest)) {
            return view('misc.error', ['message' =>
                "El usuario actual no esta asociado a un oficial,
                o el oficial al que esta asignado no pertenece a ningun destacamento."]);
        }

        $unidades = Unidad::has('oficiales')
            ->where('destacamento_id', $dest->id)
            ->where('estado_unidad_id', EstadoUnidad::DISPONIBLE)
            ->commonWith()
            ->get();

        $sectores = $dest->zona->municipio->sectores()
            ->whereNotNull('limites')
            ->with('municipio')
            ->get();

        $incidentes = $dest->incidenciasCiviles()
            ->where('estado_id','!=',EstadoIncidenciaCivil::CANCELADA)
            ->where('estado_id','!=',EstadoIncidenciaCivil::RESUELTA)
            ->commonWith()->get();
//        $oficiales  = Oficial::whereHas('unidades', function ($query) use ($unidades) {
//            $query->whereIn('id',$unidades->pluck('id')->toArray());
//        })->get()->pluck('id');
//        dd($oficiales);
        $mensajes = MensajeHub::withCommon()->get();
        $tipos = TipoIncidenciaCivil::all();
        return view('dashboards.controlPatrullas', [
            'initialData' => json_encode([
                'unidades' => $unidades,
                'sectores' => $sectores,
                'incidentes' => $incidentes,
                'mensajes' => $mensajes,
                'tipos' => $tipos
            ]),
            'destacamento_id' => $dest->id
        ]);
    }

    public function indexDashboardRegistroIncidencias () {
        return view('dashboards.registroIncidencias',[
            'tipos' => TipoIncidenciaCivil::orderBy('nombre')->get(),
            'prioridades' => PrioridadIncidenciaCivil::orderByDesc('orden')->get(),
            'municipios' => Municipio::orderBy('nombre')->get(),
        ]);
    }

    public function indexIncidenciasPoliciales()
    {
        return view("dashboards.incidenciasPoliciales");
    }
}