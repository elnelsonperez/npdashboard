<?php

namespace App\Http\Controllers;

use App\Models\Municipio;
use App\Models\Sector;
use App\Services\ReporteCrimenService;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class SectorController extends Controller
{

    public function getSectores(Request $request)
    {
        $validatedData = $request->validate([
            'municipio' => 'sometimes|exists:municipios,id',
            'fields' => 'sometimes|array'
        ]);

        if (!isset($validatedData['fields'])) {
            $select = 'sectores.*';
        } else {
            $select = $request->get('fields');
        }

        if (isset($validatedData['municipio'])) {
           return Municipio::find($validatedData['municipio'])->sectores()->whereNotNull('limites')->select($select)->get();
        }

        return Sector::select($select)->whereNotNull('limites')->get();
    }

    public function apiGetSectorStats(Request $request, ReporteCrimenService $service)
    {
        $validator = Validator::make($request->all(),  [
            'hub_serial' => 'required|exists:pms_hubs,serial',
            'processed_on' => 'required',
            'sector_id' => 'required|exists:sectores,id'
        ]);

        if ($validator->fails()) {
            return response()->json([
                'errors' => $validator->errors()
            ],422);
        }

        $data = $validator->valid();
        $from = Carbon::now()->subDays(29)->toDateString();
        $to = Carbon::now()->toDateString();

        return response()->json([
            'count_by_type' => $service->incidentesCountByType(
                $from,$to,4,$data['sector_id']),
            'count_by_hour' => $service->incidentesCountByHour($from,$to,4,$data['sector_id']),
            'count_by_day' => $service->incidentesCountByDay($from,$to,$data['sector_id']),
        ]);

    }

}
