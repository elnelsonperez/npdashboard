<?php

namespace App\Http\Controllers;

use App\Events\IncidenciaCivilCreated;
use App\Events\IncidenciaCivilStatusChanged;
use App\Events\UnidadAsignada;
use App\Http\Requests\IncidenciaCivilPost;
use App\Models\Destacamento;
use App\Models\EstadoIncidenciaCivil;
use App\Models\IncidenciaCivil;
use App\Models\PmsHub;
use App\Models\Unidad;
use App\Services\DestacamentoService;
use App\Services\IncidenciaCivilService;
use App\Shared\HandlesLocations;
use Carbon\Carbon;
use CiroVargas\GoogleDistanceMatrix\GoogleDistanceMatrix;
use Doctrine\DBAL\Query\QueryBuilder;
use Grimzy\LaravelMysqlSpatial\Types\Point;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;


class IncidenciaCivilController extends Controller
{

    use HandlesLocations;

    public function store (
        IncidenciaCivilPost $request,
        DestacamentoService $destService,
        IncidenciaCivilService $service) {

        $ubicacion = new Point($request->get('ubicacion_lat'), $request->get('ubicacion_lng'));

        $destacamento_id = $destService->destacamentoMasCercano($ubicacion)->id;

        $data = $request->validated();
        $data['destacamento'] = $destacamento_id;
        $data['ubicacion'] = $ubicacion;
        $data['creador'] = Auth::user()->id;
        $data['fecha_incidencia'] =  Carbon::createFromFormat (
            "Y-m-d\TH:i", $data['fecha_incidencia']
        )->toDateTimeString();

        $incidendia = $service->createIncidenciaCivil($data);
        $destacamento = $incidendia->destacamento->id;

        broadcast(new IncidenciaCivilCreated(IncidenciaCivil::find($incidendia->id),
            $destacamento));

        $request->session()->flash('success', 'Incidencia registrada correctamente');
        return redirect()->back();
    }

    public function index(Request $r) {
        $incidencias = IncidenciaCivil::orderByDesc('incidencias_civiles.id')->select('incidencias_civiles.*');
        if ($r->has('search') && !is_null($r->get('search')) && $r->get('search') !== "") {
            $s = $r->get('search');
            $incidencias = $incidencias
                ->join('sectores as s','s.id','=','sector_id')
                ->where('incidencias_civiles.id', $s)
                ->orWhere('ubicacion_texto','like',"%$s%")
                ->orWhere('s.nombre','like',"%$s%")
            ;
        }
        $incidencias = $incidencias->paginate(20);
        return view('incidencias.index', [
            'incidencias' => $incidencias
        ]);
    }

    public function show($id) {
        /**
         * @var $incidencia IncidenciaCivil
         */
        $incidencia = IncidenciaCivil::findOrFail($id);
        if ($incidencia->estado->id == EstadoIncidenciaCivil::CREADA) {
            $incidencia->update(['estado_id' => EstadoIncidenciaCivil::ABIERTA]);
            broadcast(new IncidenciaCivilStatusChanged($incidencia,
                $incidencia->destacamento_id));
        }
        $incidencia = IncidenciaCivil::findOrFail($id);
        $tiempo = $incidencia->fechaFinalizada();
        $unidad = $incidencia->unidades->first();

        if ($tiempo) {
            $tiempo_de_respuesta = $this->secondsToTime($tiempo->diffInSeconds($incidencia->creado_en));
        } else {
            $tiempo_de_respuesta = null;
        }

        $timeline = $this->getTimeline($incidencia);

        return view('incidencias.show',[
            'incidencia' => $incidencia,
            'tiempo_de_respuesta' => $tiempo_de_respuesta,
            'unidad' => $unidad,
            'timeline' => $timeline
        ]);
    }

    private function getTimeline (IncidenciaCivil $incidencia) {
        $timeline = [];
        $filtered = $incidencia->revisionHistory->filter(function ($h) {
            return $h->key == "estado_id";
        });

        $ordered = $filtered->sort(function($a,$b) {
            return $a->created_at->timestamp - $b->created_at->timestamp;
        });

        foreach ($ordered as $h) {
            if ($h->userResponsible()) {
                $who = $h->userResponsible()->oficial->nombre . " " . $h->userResponsible()->oficial->apellido;
            } else {
                $who = "El supervisor";
            }


            if ($h->oldValue() && $h->oldValue()->id == EstadoIncidenciaCivil::CREADA) {
                $timeline[] = [
                    'time' => $incidencia->creado_en->format('d-m h:i:s A'),
                    'text' => "<span class='text-bold'>$who</span> registró la incidencia."
                ];
            }

            if ($h->newValue() && $h->newValue()->id == EstadoIncidenciaCivil::ABIERTA) {
                $timeline[] = [
                    'time' => $h->created_at->format('d-m h:i:s A'),
                    'text' => "<span class='text-bold'>$who</span> abrió la incidencia."
                ];
            }

            if ($h->newValue()->id == EstadoIncidenciaCivil::ASIGNADA) {
                $unidad_id = $incidencia->unidades()->first()->id;
                $timeline[] = [
                    'time' => $h->created_at->format('d-m h:i:s A'),
                    'text' => "<span class='text-bold'>$who</span> asignó la incidencia a la 
                                <span class='text-bold'>Unidad $unidad_id</span>.",
                ];
            }

            if ($h->newValue()->id == EstadoIncidenciaCivil::EN_CURSO) {
                $timeline[] = [
                    'time' => $h->created_at->format('d-m h:i:s A'),
                    'text' => "La incidencia fue aceptada por la unidad.",
                ];
            }

            if ($h->newValue()->id == EstadoIncidenciaCivil::RECHAZADA) {
                $timeline[] = [
                    'time' => $h->created_at->format('d-m H:i:s A'),
                    'text' => "La unidad cedió la incidencia.",
                ];
            }

            if ($h->newValue()->id == EstadoIncidenciaCivil::CANCELADA) {
                $timeline[] = [
                    'time' => $h->created_at->format('d-m h:i:s A'),
                    'text' => "<span class='text-bold'>$who</span> marco la incidencia como Cancelada."
                ];
            }

            if ($h->newValue()->id == EstadoIncidenciaCivil::RESUELTA) {
                $timeline[] = [
                    'time' => $h->created_at->format('d-m h:i:s A'),
                    'text' => "La incidencia fue marcada como <span class='text-bold'>Resulta</span>"
                ];
            }

        }

        return $timeline;
    }

    public function asignarUnidad(Request $r, IncidenciaCivilService $service)
    {
        $data = $this->validate($r,[
            'incidencia_id' => 'required|exists:incidencias_civiles,id',
            'unidad_id' => 'required|exists:unidades,id',
        ]);

        $incidencia = $service->asignarUnidad (
            $data['incidencia_id'],
            $data['unidad_id']
        );

        $incidencia->update(['estado_id' => EstadoIncidenciaCivil::ASIGNADA]);

        $destacamento = $incidencia->destacamento->id;

        broadcast(new UnidadAsignada(
            $data['unidad_id'],
            $data['incidencia_id'],
            EstadoIncidenciaCivil::find(EstadoIncidenciaCivil::ASIGNADA),
            $destacamento));

        return $incidencia->load($incidencia->getCommonWithArray());
    }

    public function apiGetIncidencias(Request $request,GoogleDistanceMatrix $gmr) {
        $validator = Validator::make($request->all(),  [
            'hub_serial' => 'required|exists:pms_hubs,serial',
            'processed_on' => 'required',
            'from' => 'sometimes|required'
        ]);

        if ($validator->fails()) {
            return response()->json([
                'errors' => $validator->errors()
            ],422);
        }

        $data = $validator->valid();

        /**
         * @var $hub PmsHub
         */
        $hub = PmsHub::where('serial', $data['hub_serial'])->get()->first();

        if (!$hub) {
            return response()->json([
                'errors' => [
                    'serial' => 'Serial no encontrado en DB'
                ]
            ],422);
        }

        $unidad = $hub->unidad;
        if (!$unidad) {
            return response()->json([
                'errors' => [
                    'unidad' => 'Hub no asociado a ninguna unidad'
                ]
            ],422);
        }

        $from = null;
        if (isset($data['from']) && $data['from']) {
            $from = $data['from'];
        }


        $incidencias = $unidad->incidencias()
            ->where(function ($q) {
                /**
                 * @var $q QueryBuilder
                 */
                $q->where('estado_id',EstadoIncidenciaCivil::ASIGNADA);
                $q->orWhere('estado_id',EstadoIncidenciaCivil::EN_CURSO);
            })
            ->withCellData();

        if ($from) {
            $incidencias = $incidencias->where('incidencias_civiles.actualizado_en','>',$from);
        }

        $incidencias  = $incidencias->get();

        if (count($incidencias) > 0) {
            $this->appendDistanciaYTiempo($incidencias,$unidad,$gmr);
            return response()->json($incidencias,200);
        }

        $unidad->triggerLastUpdate();
        return response()->json(null,204);
    }

    private function appendDistanciaYTiempo (&$incidencias, $unidad, GoogleDistanceMatrix $gmr) {
        $req = $gmr
            ->setLanguage('cs')
            ->setMode(GoogleDistanceMatrix::MODE_DRIVING);

        foreach ($incidencias as $inc) {
            /**
             * @var $loc Point
             */
            $loc = $unidad->localizacionActual();
            $req->addOrigin(
                $loc->getLat().','.$loc->getLng()
            );
            $req->addDestination(
                $inc->ubicacion->getLat().','.
                $inc->ubicacion->getLng()
            );
        }

        try {
            $response = $req->sendRequest();
            $i = 0;
            foreach ($response->getRows() as $r) {
                $incidencias->get($i)->distancia = $this->metersAsKm( $r->getElements()[0]->getDistance()->getValue());
                $incidencias->get($i)->tiempo = $this->secondsToTime($r->getElements()[0]->getDuration()->getValue());
                $i++;
            }
        } catch (\Exception $e) {
            foreach ($incidencias as $inc) {
                $inc->tiempo = 'N/A';
                $inc->distancia = $this->metersAsKm($this->distanceBetweenPoints(
                    $unidad->localizacionActual(),
                    $inc->ubicacion));
            }
        }
        return $incidencias;
    }

    public function apiChangeStatus (Request $request) {
        $validator = Validator::make($request->all(),  [
            'hub_serial' => 'required|exists:pms_hubs,serial',
            'processed_on' => 'required',
            'estado_id' => 'required|exists:estados_incidencias_civiles,id',
            'incidencia_id' => 'required|exists:incidencias_civiles,id'
        ]);

        if ($validator->fails()) {
            return response()->json([
                'errors' => $validator->errors()
            ],422);
        }

        $data = $validator->valid();

        /**
         * @var $hub PmsHub
         */
        $hub = PmsHub::where('serial', $data['hub_serial'])->get()->first();
        if (!$hub) {
            return response()->json([
                'errors' => [
                    'serial' => 'Serial no encontrado en DB'
                ]
            ],422);
        }

        $unidad = $hub->unidad;
        $incidencia = IncidenciaCivil::find($data['incidencia_id']);
        if (intval($data['estado_id']) == EstadoIncidenciaCivil::RECHAZADA) {
            $incidencia->unidades()->sync([]);
        }
        $incidencia->update(['estado_id' => $data['estado_id']]);

        broadcast(new IncidenciaCivilStatusChanged($incidencia,
            $incidencia->destacamento_id));

        $unidad->triggerLastUpdate();
        return response()->json($incidencia->load($incidencia->getCellDataArray()),200);
    }

    public function changeStatus (Request $r) {
        $data = $this->validate($r, [
            'estado_id' => 'required|exists:estados_incidencias_civiles,id',
            'incidencia_id' => 'required|exists:incidencias_civiles,id'
        ]);

        $incidencia = IncidenciaCivil::find($data['incidencia_id']);

        if ($data['incidencia_id'] == EstadoIncidenciaCivil::RECHAZADA) {
            $incidencia->unidades()->sync([]);
        }

        $incidencia->update(['estado_id' => $data['estado_id']]);
        broadcast(new IncidenciaCivilStatusChanged($incidencia,
            $incidencia->destacamento_id));

        return $incidencia->load($incidencia->getCommonWithArray());
    }

    public function getIncidencias () {
        return IncidenciaCivil::where('estado_id','!=',EstadoIncidenciaCivil::RESUELTA)->commonWith()->get();
    }

    public function getIncidencia ($id) {
        return IncidenciaCivil::findOrFail($id)->commonWith()->get();
    }

}