<?php

namespace App\Http\Controllers;

use App\Events\OficialCreated;
use App\Models\Oficial;
use App\Models\Rango;
use App\Models\TipoOficial;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class OficialController extends Controller
{

    public function index()
    {
        $name = 'oficiales';
        $title = 'Oficiales';

        $data = Oficial::with([
            'rango',
            'tipo',
            'destacamento',
            'unidad'
        ])->paginate(15);

        $fields = [
            'id' => [
                'title' => '#'
            ],
            'cedula' => [
                'title' => 'Cedula'
            ],
            'nombre' => [
                'title' => 'Nombre'
            ],
            'destacamento.nombre' => [
                'title' => 'Destacamento'
            ],
            'unidad.id' => [
                'title' => 'Unidad'
            ],
            'rango.nombre' => [
                'title' => 'Rango'
            ],
            'tipo.nombre' => [
                'title' => 'Tipo'
            ],
            'fecha_ingreso' => [
                'title' => 'Fecha ingreso'
            ],
            'creado_en' => [
                'title' => 'Fecha registro'
            ],
        ];

        $custom = [
            'nombre' => function ($r) {
                return $r->nombre. ' '. $r->apellido;
            },
            'fecha_ingreso' => function ($r) {
                return $r->fecha_ingreso->format('d-m-y');
            },
            'creado_en' => function ($r) {
                return $r->creado_en->format('d-m-y');
            },
            'unidad.id' => function ($r) {
            if ($r->unidad)
                return 'U-'.$r->unidad->id;
            else
                return 'N/A';
            }
        ];

        return view('generic.resourceIndex')->with([
            'name' => $name,
            'title' => $title,
            'data' => $data,
            'fields' => $fields,
            'fieldCustom' => $custom
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('oficiales.create', [
            'rangos' => Rango::orderBy('nombre')->get(),
            'tipos' => TipoOficial::orderBy('nombre')->get()
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $this->validate($request, [
            'nombre' => 'required',
            'apellido' => 'required',
            'fecha_nacimiento' => 'required',
            'fecha_ingreso' => 'required',
            'cedula' => 'required',
            'sexo' => 'required',
            'rango_id' => 'required',
            'tipo_oficial_id' => 'required',
        ]);

        $data['destacamento_id'] = Auth::user()->destacamento()->id;
        $oficial = Oficial::create($data);
        broadcast(new OficialCreated($oficial,$data['destacamento_id']));

        $request->session()->flash('success', 'Oficial registrado correctamente.');
        return redirect()->back();
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
