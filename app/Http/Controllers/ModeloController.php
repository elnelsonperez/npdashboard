<?php

namespace App\Http\Controllers;

use App\Models\Marca;
use App\Models\Modelo;
use Illuminate\Http\Request;

class ModeloController extends Controller
{

    public function getModelos(Request $request)
    {
        $validatedData = $request->validate([
            'marca' => 'sometimes|exists:marcas,id',
            'fields' => 'sometimes|array'
        ]);

        if (!isset($validatedData['fields'])) {
            $select = 'modelos.*';
        } else {
            $select = $request->get('fields');
        }

        if (isset($validatedData['marca'])) {
           return Marca::find($validatedData['marca'])->modelos()->select($select)->get();
        }

        return Modelo::select($select)->get();
    }

}
