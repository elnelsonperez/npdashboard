<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class IncidenciaCivilPost extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nombre_civil' => 'present|max:255',
            'sector' => 'required',
            'prioridad' => 'required',
            'tipo' => 'required',
            'telefono_civil' => 'present|max:12',
            'ubicacion_lat' => 'required',
            'ubicacion_lng' => 'required',
            'fecha_incidencia' => 'required',
            'detalle_ubicacion' => 'required',
            'detalle_incidente' => 'required',
            'ubicacion_texto' => 'present',
            'personas_involucradas' => 'present|numeric',
        ];
    }

    public function messages()
    {
        return [
            'ubicacion_lat.required' => "Necesita seleccionar una ubicacion",
            'detalle_ubicacion.required' => "Los detalles de ubicacion son requeridos",
            'detalle_incidente.required' => "Los detalles del incidente son requeridos",
        ];
    }
}
