<?php
namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Venturecraft\Revisionable\RevisionableTrait;

class NpModel extends Model
{

    const CREATED_AT = "creado_en";
    const UPDATED_AT = "actualizado_en";

    /**
     * Just to get ide completion
     * @var array
     */
    protected $spatialFields = [];

    use RevisionableTrait;
    protected $revisionEnabled = false;
    protected $guarded = [];

    public function identifiableName()
    {
        return $this;
    }

    /**
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeDestacamento ($query) {
        return $query->where('destacamento_id','=',Auth::user()->destacamento->id);
    }

}