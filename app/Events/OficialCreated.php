<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;

use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class OficialCreated implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    private $destacamento;
    public $oficial;


    /**
     * Create a new event instance.
     *
     * @return void
     */

    public function __construct($oficial, $destacamento)
    {
        $this->destacamento = $destacamento;
        $this->oficial = $oficial;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new Channel('d-'.$this->destacamento);
    }

    public function broadcastAs()
    {
        return 'oficial.created';
    }
}
