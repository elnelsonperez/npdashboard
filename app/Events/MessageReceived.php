<?php

namespace App\Events;

use App\Models\MensajeHub;
use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class MessageReceived implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $mensaje;
    private $destacamento;

    public function __construct(MensajeHub $msg,$destacamento_id)
    {
        $this->destacamento = $destacamento_id;
        $this->mensaje = $msg->load($msg->getCommonRelations())->toArray();
    }

    public function broadcastOn()
    {
        return new Channel('d-'.$this->destacamento);
    }

    public function broadcastAs()
    {
        return 'mensaje_hub.received';
    }
}
