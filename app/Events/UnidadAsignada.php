<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;

use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class UnidadAsignada implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    private $destacamento;
    public $unidad;
    public $incidencia;
    public $estado;

    /**
     * Create a new event instance.
     *
     * @return void
     */

    public function __construct($unidad,$incidencia, $estado, $destacamento)
    {
        $this->destacamento = $destacamento;
        $this->unidad = $unidad;
        $this->estado = $estado;
        $this->incidencia = $incidencia;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new Channel('d-'.$this->destacamento);
    }

    public function broadcastAs()
    {
        return 'incidencia_civil.unidad_asignada';
    }
}
