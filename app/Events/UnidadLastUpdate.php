<?php

namespace App\Events;

use App\Models\Unidad;
use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class UnidadLastUpdate implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    private $destacamento;
    public $unidad_id;
    public $last_update_time;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(Unidad $unidad, $destacamento_id)
    {
        $this->unidad_id = $unidad->id;
        $this->last_update_time = $unidad->last_update_time->toDateTimeString();
        $this->destacamento = $destacamento_id;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new Channel('d-'.$this->destacamento);
    }

    public function broadcastAs()
    {
        return 'unidad.last_update';
    }
}
