<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;

use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class HubLocalizacionesReceived implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;
    public $localizaciones;
    private $destacamento;
    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($locs,$destacamento_id)
    {
        $this->localizaciones = $locs;
        $this->destacamento = $destacamento_id;
    }

    public function broadcastOn()
    {
        return new Channel('d-'.$this->destacamento);
    }

    public function broadcastAs()
    {
        return 'hub_localizacion.created_batch';
    }
}
