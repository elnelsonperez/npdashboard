<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;

use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class UnidadCreated implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    private $destacamento;
    public $unidad;


    /**
     * Create a new event instance.
     *
     * @return void
     */

    public function __construct($unidad, $destacamento)
    {
        $this->destacamento = $destacamento;
        $this->unidad = $unidad;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new Channel('d-'.$this->destacamento);
    }

    public function broadcastAs()
    {
        return 'unidad.created';
    }
}
