<?php

namespace App\Events;

use App\Models\IncidenciaCivil;
use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class IncidenciaCivilStatusChanged implements  ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    private $destacamento;
    public $incidenciaCivil;

    public function __construct(IncidenciaCivil $incidenciaCivil, $destacamento_id)
    {
        $this->destacamento = $destacamento_id;
        $this->incidenciaCivil =
            $incidenciaCivil->load($incidenciaCivil->getCommonWithArray())->toArray();
    }

    public function broadcastOn()
    {
        return new Channel('d-'.$this->destacamento);
    }

    public function broadcastAs()
    {
        return 'incidencia_civil.status_changed';
    }
}
