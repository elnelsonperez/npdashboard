<?php

namespace App\Events;

use App\Models\MensajeHub;
use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class MessagesUpdated implements  ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $mensajes;
    private $destacamento;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(array $mensajes_ids,$destacamento)
    {
        $this->destacamento = $destacamento;
        $this->mensajes = MensajeHub::whereIn('id', $mensajes_ids)->withCommon()->get()->toArray();
    }


    public function broadcastOn()
    {
        return new Channel('d-'.$this->destacamento);
    }

    public function broadcastAs()
    {
        return 'mensaje_hub.batch_updated';
    }

}
