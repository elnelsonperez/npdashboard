<?php

namespace App\Events;

use App\Models\IncidenciaCivil;
use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class IncidenciaCivilCreated implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $incidenciaCivil;
    private $destacamento;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(IncidenciaCivil $incidenciaCivil, $destacamento_id)
    {
        $this->destacamento = $destacamento_id;
        $this->incidenciaCivil =
            $incidenciaCivil->load($incidenciaCivil->getCommonWithArray())->toArray();
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new Channel('d-'.$this->destacamento);
    }

    public function broadcastAs()
    {
        return 'incidencia_civil.created';
    }
}
