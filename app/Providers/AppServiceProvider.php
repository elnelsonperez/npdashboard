<?php

namespace App\Providers;

use App\Models\HubConfig;
use App\Models\MensajeHub;
use App\Models\PmsHub;
use App\Models\Unidad;
use App\Observers\HubObserver;
use App\Observers\HubPendingDataObserver;
use App\Observers\UnidadObserver;
use CiroVargas\GoogleDistanceMatrix\GoogleDistanceMatrix;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        MensajeHub::observe(HubPendingDataObserver::class);
        HubConfig::observe(HubPendingDataObserver::class);
        PmsHub::observe(HubObserver::class);
        Unidad::observe(UnidadObserver::class);

        $this->app->singleton(GoogleDistanceMatrix::class, function ($app) {
            return new GoogleDistanceMatrix(env('GOOGLE_MAPS_API_KEY'));
        });

    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        if ($this->app->environment() !== 'production') {
            $this->app->register(\Barryvdh\LaravelIdeHelper\IdeHelperServiceProvider::class);
            $this->app->register(\Way\Generators\GeneratorsServiceProvider::class);
            $this->app->register(\Xethron\MigrationsGenerator\MigrationsGeneratorServiceProvider::class);
            $this->app->register('CoRex\Laravel\Model\ModelServiceProvider');
        }
    }
}
