<?php

namespace App\Services;


use App\Models\Destacamento;
use App\Shared\HandlesLocations;
use Grimzy\LaravelMysqlSpatial\Types\Point;

class DestacamentoService
{
    use HandlesLocations;

    public function destacamentoMasCercano (Point $point) {
        $destacamentos = Destacamento::distanceSphereValue('ubicacion',
            $point)->get();
        return $this->findClosest($destacamentos, 'distance');
    }

}