<?php

namespace App\Services;

use App\Models\EstadoIncidenciaCivil;
use App\Models\EstadoUnidad;
use App\Models\IncidenciaCivil;
use App\Models\Sector;
use App\Models\Unidad;
use Carbon\Carbon;
use Illuminate\Database\Query\JoinClause;
use Illuminate\Support\Facades\DB;

class ReporteCrimenService
{

    public function incidentesCount ($from, $to) {
        return IncidenciaCivil::validas()
            ->whereDate('creado_en','>=',$from)
            ->whereDate('creado_en','<=',$to)
            ->count();
    }

    public function incidentesCountByType ($from, $to, $limit = 5, $sector = null) {

        $inci = IncidenciaCivil::validas()
            ->whereDate('creado_en','>=',$from)
            ->whereDate('creado_en','<=',$to)
            ->join('tipos_incidencias_civiles as t','t.id','incidencias_civiles.tipo_id')
            ->groupBy('t.nombre')
            ->limit($limit)
            ->orderByDesc('cantidad')
            ->orderBy('t.nombre', 'ASC')
            ->select(['t.nombre', \DB::raw('COUNT(*) cantidad')]);
        if (!is_null($sector)) {
            $inci = $inci->whereIn('incidencias_civiles.id',
                Sector::find($sector)->incidenciasCiviles()->select('id')->get()->toArray());
        }
        $inci = $inci->get();
        return $inci->toArray();
    }

    public function incidentesCountBySector ($from, $to, $limit = 10) {
        $inci = IncidenciaCivil::validas()
            ->whereDate('creado_en','>=',$from)
            ->whereDate('creado_en','<=',$to)
            ->join('sectores as s','s.id','incidencias_civiles.sector_id')
            ->groupBy('s.nombre')
            ->limit($limit)
            ->orderByDesc('cantidad')
            ->select(['s.nombre', \DB::raw('COUNT(*) cantidad')])->get();
        return $inci->toArray();
    }



    public function incidentesCountByUnidad ($from, $to) {
        $inci = IncidenciaCivil::validas()
            ->whereDate('incidencias_civiles.creado_en','>=',$from)
            ->whereDate('incidencias_civiles.creado_en','<=',$to)
            ->join('unidades_incidencias as ui','ui.incidencia_id','incidencias_civiles.id')
            ->join('unidades as u','u.id','ui.unidad_id')
            ->groupBy('u.id')
            ->orderByDesc('cantidad')
            ->select(['u.id', \DB::raw('COUNT(*) cantidad')])->get();
        return $inci->toArray();
    }



    public function incidentesCountByTypeAndSector ($from, $to, $limit) {
        $inci = IncidenciaCivil::validas()
            ->whereDate('incidencias_civiles.creado_en','>=',$from)
            ->whereDate('incidencias_civiles.creado_en','<=',$to)
            ->join('tipos_incidencias_civiles as t','t.id','incidencias_civiles.tipo_id')
            ->join('sectores as s','s.id','incidencias_civiles.sector_id')
            ->groupBy(['s.nombre','t.nombre'])
            ->select(['s.nombre as sector','t.nombre as tipo', \DB::raw('COUNT(*) cantidad')]);

        $maxs = DB::table(DB::raw("({$inci->toSql()}) as z"))
            ->mergeBindings($inci->getQuery())
            ->select(DB::raw("max(z.cantidad) as cantidad, z.sector"))
            ->groupBy(['z.sector']);

        $cross = DB::table(DB::raw("({$maxs->toSql()}) as x"))
            ->mergeBindings($maxs)
            ->join(DB::raw("({$inci->toSql()}) as h"),function (JoinClause $j) {
                $j->on('x.cantidad','=','h.cantidad');
                $j->on('h.sector','=','x.sector');
            })
            ->mergeBindings($inci->getQuery())
            ->select(DB::raw('h.*'))
            ->limit($limit)
            ->orderByDesc('h.cantidad')
        ;

        return $cross->get()->toArray();
    }

    public function incidentesCountByHour ($from, $to, $limit, $sector = null) {
        $inci = IncidenciaCivil::validas()
            ->whereDate('creado_en','>=',$from)
            ->whereDate('creado_en','<=',$to)
            ->limit($limit)
            ->orderByDesc('cantidad')
            ->orderBy('hora')
            ->groupBy(\DB::raw('hour(fecha_incidencia)'))
            ->select([\DB::raw('hour(fecha_incidencia) as hora'), \DB::raw('COUNT(*) cantidad')])
        ;

        if (!is_null($sector)) {
            $inci = $inci->whereIn('incidencias_civiles.id',
                Sector::find($sector)->incidenciasCiviles()->select('id')->get()->toArray());
        }
        $inci = $inci->get();

        return $inci->toArray();
    }

    public function tiempoDeRespuestaByUnidad ($from, $to) {
        $unidades = Unidad::where('estado_unidad_id', EstadoUnidad::DISPONIBLE)->select('id')->get();
        $result = [];


        foreach ($unidades as $u) {
            $res = $this->tiempoDeRespuestaProm($from, $to,$u->id);
            if ($res !== 0) {
                array_push($result, [
                    'unidad_id' => $u->id,
                    'tiempo_respuesta_prom' => $res
                ]);
            }

        }
        $collection = collect($result);
        $collection = $collection->sort(function($a,$b) {
            return floatval($a['tiempo_respuesta_prom']) - floatval($b['tiempo_respuesta_prom']);
        });

        return array_values($collection->toArray());
    }


    public function incidentesCountByDay ($from, $to, $sector = null) {
        $dias = [
            'Domingo',
            'Lunes',
            'Martes',
            'Miércoles',
            'Jueves',
            'Viernes','Sábado'
        ];
        $inci = IncidenciaCivil::validas()
            ->whereDate('creado_en','>=',$from)
            ->whereDate('creado_en','<=',$to)
            ->groupBy(\DB::raw('DAYOFWEEK(fecha_incidencia)'))

            ->select([
                \DB::raw('DAYOFWEEK(fecha_incidencia) as dia'),
                \DB::raw('COUNT(*) cantidad')])
            ->orderByDesc('cantidad')->orderBy('dia');

        if (!is_null($sector)) {
            $inci = $inci->whereIn('incidencias_civiles.id',
                Sector::find($sector)->incidenciasCiviles()->select('id')->get()->toArray());
        }
        $inci = $inci->get();

        $res = [];
        foreach ( $inci->toArray() as $i) {
            $res []= [
                'dia' => $dias[$i['dia']-1],
                'cantidad' => $i['cantidad']
            ];
        }
        return $res;
    }

    public function incidentesCountByDayHour ($from, $to, $sector = null) {
        $dias = [
            'Domingo',
            'Lunes',
            'Martes',
            'Miércoles',
            'Jueves',
            'Viernes','Sábado'
        ];

        $inci = IncidenciaCivil::validas()
            ->whereDate('creado_en','>=',$from)
            ->whereDate('creado_en','<=',$to)
            ->groupBy([\DB::raw('hour(fecha_incidencia)'),\DB::raw('DAYOFWEEK(fecha_incidencia)')])
            ->select([
                \DB::raw('hour(fecha_incidencia) as hora'),
                \DB::raw('DAYOFWEEK(fecha_incidencia) as dia'),
                \DB::raw('COUNT(*) cantidad')]);
        if (!is_null($sector)) {
            $inci = $inci->whereIn('incidencias_civiles.id',
                Sector::find($sector)->incidenciasCiviles()->select('id')->get()->toArray());
        }
        $inci = $inci->get();

        $res = [];

        foreach ( $inci->toArray() as $i) {
            $res []= [
                'dia' => $dias[$i['dia']-1],
                'hora' => $i['hora'],
                'cantidad' => $i['cantidad']
            ];
        }

        return $res;
    }

    public function involucradosCount ($from, $to) {
        return IncidenciaCivil::validas()
            ->whereDate('creado_en','>=',$from)
            ->whereDate('creado_en','<=',$to)
            ->sum('personas_involucradas');
    }

    public function getLocationsAsLatLngArray ($from, $to) {
        $inc = IncidenciaCivil::validas()
            ->whereDate('creado_en','>=',$from)
            ->whereDate('creado_en','<=',$to)
            ->select('ubicacion')->get();
        $res = [];
        foreach ($inc as $i) {
            array_push($res, [
                'lat' => $i->ubicacion->getLat(),
                'lng' => $i->ubicacion->getLng()
            ]);
        }
        return $res;
    }

    public function tiempoDeRespuestaProm ($from, $to, $unidad = null) {
        $inc = IncidenciaCivil::where('estado_id', EstadoIncidenciaCivil::RESUELTA)
            ->whereDate('creado_en','>=',$from)
            ->whereDate('creado_en','<=',$to);

        if (!is_null($unidad)) {
            $inc = $inc->whereIn('id', Unidad::find($unidad)->incidencias()->select('id')->get()->toArray());
        }

        $inc = $inc->get();
        $times = [];
        foreach ($inc as $i) {
            $done = $i->fechaFinalizada();
            if (!is_null($done)) {
                array_push($times, $i->fechaFinalizada()->diffInMinutes($i->creado_en));
            }
        }

        $count = count($times);
        if ($count == 0) {
            return 0;
        }
        return number_format(array_sum($times)/$count,1);
    }

    public function cambioPeriodoAnterior ($from, $to) {
        $fromCarbon = Carbon::createFromFormat("Y-m-d", $from);
        $toCarbon = Carbon::createFromFormat("Y-m-d", $to);
        $period = $fromCarbon->diff($toCarbon)->days;
        $newFrom = $fromCarbon->subDays($period);
        $newTo = $fromCarbon->subDay();
        return $this->incidentesCount($from,$to) -
            $this->incidentesCount($newFrom->toDateString(), $newTo->toDateString())
            *100;
    }

}