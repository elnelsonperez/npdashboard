<?php

namespace App\Services;


use App\Models\EstadoIncidenciaCivil;
use App\Models\IncidenciaCivil;
use App\Models\Unidad;
use Carbon\Carbon;

class IncidenciaCivilService
{

    public function createIncidenciaCivil(array $data)
    {
        $o = new IncidenciaCivil();

        if (isset($data['creado_en'])) {
            $o->creado_en = $data['creado_en'];
        }

        if (isset($data['actualizado_en'])) {
            $o->actualizado_en = $data['actualizado_en'];
        }

        $o->nombre_civil = $data['nombre_civil'];
        $o->telefono_civil = $data['telefono_civil'] == "   -   -    " ? null : $data['telefono_civil'];
        $o->destacamento_id = $data['destacamento'];
        $o->sector_id = $data['sector'];
        $o->prioridad_id = $data['prioridad'];
        $o->tipo_id = $data['tipo'];
        $o->ubicacion = $data['ubicacion'];
        $o->fecha_incidencia = $data['fecha_incidencia'];
        $o->creada_por = $data['creador'];
        $o->ubicacion_texto = $data['ubicacion_texto'] ?? "No provista";
        $o->detalle_incidente = $data['detalle_incidente'];
        $o->detalle_ubicacion = $data['detalle_ubicacion'];
        $o->personas_involucradas = $data['personas_involucradas'];
        $o->estado()->associate(EstadoIncidenciaCivil::find(EstadoIncidenciaCivil::CREADA));
        $o->save();
        return $o;
    }

    public function asignarUnidad ($incidente_id, $unidad_id) {
        $incidente = IncidenciaCivil::find($incidente_id);
        $incidente->unidades()->sync($unidad_id);
        return $incidente;
    }
    
}