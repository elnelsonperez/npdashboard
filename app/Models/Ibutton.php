<?php

namespace App\Models;

use App\NpModel;

class Ibutton extends NpModel
{
    protected $table = 'ibuttons';
    public function oficial() {
        return $this->belongsTo(Oficial::class,'oficial_id');
    }
}
