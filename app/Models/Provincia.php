<?php

namespace App\Models;

use App\NpModel;
use Grimzy\LaravelMysqlSpatial\Eloquent\SpatialTrait;

class Provincia extends NpModel
{
    use SpatialTrait;
    public $timestamps = false;
    protected $table = 'provincias';
    protected $spatialFields = [
        'limites'
    ];

    public function municipios ()
    {
        return $this->hasMany(Municipio::class,'provincia_id');
    }
}
