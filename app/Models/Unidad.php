<?php

namespace App\Models;

use App\Events\UnidadLastUpdate;
use App\NpModel;
use Carbon\Carbon;

class Unidad extends NpModel
{
    protected $guarded = [];

    protected $table = 'unidades';

    protected $dates = ['last_update_time'];
    protected $appends = ['supervisor'];

    public function identifiableName()
    {
        return $this;
    }

    public function triggerLastUpdate () {
        $this->last_update_time = Carbon::now();
        $this->save();
        broadcast(new UnidadLastUpdate($this,$this->destacamento_id));
    }

    public function getSupervisorAttribute () {
        $oficial = $this->oficiales()->with('flota')
            ->where('tipo_oficial_id', TipoOficial::SUPEVISOR)->get()->first();
        if ($oficial) {
            return $oficial;
        }
        return null;
    }


    public function localizaciones () {
        return $this->hasManyThrough (
            HubLocalizacion::class,
            PmsHub::class,
            'unidad_id',
            'pms_hub_id'
        );
    }

    public  function scopeCommonWith ($query) {
        return $query->with(
          $this->getCommonWithArray()
        );
    }

    public function getCommonWithArray () {
        return [
            'modelo.marca',
            'sector' => function($q) {
                $q->select(['id','nombre']);
            },
            'estado',
            'oficiales.rango',
            'oficiales.tipo',
            'oficiales.flota',
            'hub',
            'localizaciones' => function ($q) {
                $q->orderByDesc('fecha_obtenida');
            }
        ];
    }

    public function localizacionActual () {
        return $this->localizaciones()
            ->orderByDesc('fecha_obtenida')
            ->limit(1)->get()->first()->ubicacion;
    }

    public function estado() {
        return $this->belongsTo(EstadoUnidad::class,'estado_unidad_id');
    }

    public function sector () {
        return $this->belongsTo(Sector::class,'sector_id');
    }

    public function destacamento () {
        return $this->belongsTo(Destacamento::class,'destacamento_id');
    }

    public function modelo () {
        return $this->belongsTo(Modelo::class,'modelo_id');
    }

    public function hub () {
        return $this->hasOne(PmsHub::class, "unidad_id");
    }

    public function oficiales () {
        return $this->hasMany(Oficial::class);
    }

    public function incidencias() {
        return $this->belongsToMany(IncidenciaCivil::class,'unidades_incidencias',
            'unidad_id','incidencia_id')->withTimestamps();
    }

    public function incidenciasPoliciales () {
        return $this->belongsToMany(IncidenciaPolicial::class,'incidencias_policiales_unidades',
            'unidad_id', 'incidencia_policial_id');
    }

}
