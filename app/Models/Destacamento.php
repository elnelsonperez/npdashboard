<?php

namespace App\Models;

use App\NpModel;
use Grimzy\LaravelMysqlSpatial\Eloquent\SpatialTrait;
use Grimzy\LaravelMysqlSpatial\Types\Point;

class Destacamento extends NpModel
{

    protected $table = 'destacamentos';
    use SpatialTrait;
    protected $spatialFields = [
        'ubicacion'
    ];

    public function zona () {
        return $this->belongsTo(Zona::class,"zona_id");
    }

    public function localizaciones () {
        return $this->hasManyThrough (
            HubLocalizacion::class,
            PmsHub::class,
            'unidad_id',
            'pms_hub_id'
        );
    }

    public function currentSargentoGuardia ($select = null) {
        $res =  $this->oficiales()
            ->where('tipo_oficial_id', TipoOficial::SARGENTO_GUARDIA);
        if ($select) {
            $res = $res->select($select)->get()->first();
        }
        return $res;
    }

    public function unidades () {
        return $this->hasMany(Unidad::class);
    }

    public function oficiales () {
        return $this->hasMany(Oficial::class,'destacamento_id');
    }

    public function incidenciasCiviles () {
        return $this->hasMany(IncidenciaCivil::class,'destacamento_id');
    }

    public function incidenciasPoliciales () {
        return $this->hasMany(IncidenciaPolicial::class,'destacamento_id');
    }
}
