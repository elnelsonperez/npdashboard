<?php

namespace App\Models;

use App\NpModel;
use Illuminate\Database\Eloquent\Builder;


class HubConfig extends NpModel
{
    protected $table = 'hubs_configs';
    protected $primaryKey = ['key', 'pms_hub_id'];
    public $incrementing = false;
    public $timestamps = false;

    protected function setKeysForSaveQuery(Builder $query)
    {
        $keys = $this->getKeyName();
        if(!is_array($keys)){
            return parent::setKeysForSaveQuery($query);
        }

        foreach($keys as $keyName){
            $query->where($keyName, '=', $this->getKeyForSaveQuery($keyName));
        }

        return $query;
    }

    /**
     * Get the primary key value for a save query.
     *
     * @param mixed $keyName
     * @return mixed
     */
    protected function getKeyForSaveQuery($keyName = null)
    {
        if(is_null($keyName)){
            $keyName = $this->getKeyName();
        }

        if (isset($this->original[$keyName])) {
            return $this->original[$keyName];
        }

        return $this->getAttribute($keyName);
    }

    public function hubs () {
        return $this->belongsTo(PmsHub::class,'pms_hub_id');
    }
}
