<?php

namespace App\Models;

use App\Models\PmsHub;
use App\NpModel;
use Illuminate\Database\Eloquent\Model;

class HubDataPendiente extends NpModel
{
    public $timestamps = false;
    protected $table = "hubs_data_pendiente";
    protected $dates = [
        'configs_ultimo_pull'
    ];
    protected $casts = [
        'configs' => 'boolean'
    ];

    public function hub () {
        return $this->belongsTo(PmsHub::class, 'pms_hub_id');
    }
}
