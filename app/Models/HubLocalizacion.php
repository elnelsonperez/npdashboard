<?php

namespace App\Models;
use App\NpModel;
use Grimzy\LaravelMysqlSpatial\Eloquent\SpatialTrait;

class HubLocalizacion extends NpModel
{
    use SpatialTrait;
    protected $table = 'pms_hubs_localizaciones';
    const UPDATED_AT = null;
    protected $spatialFields = [
        'ubicacion'
    ];

    protected $dates = [
        'fecha_obtenida'
    ];

    public function hub () {
        return $this->belongsTo(PmsHub::class,'pms_hub_id');
    }

    public function unidad () {

    }

}
