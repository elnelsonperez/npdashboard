<?php

namespace App\Models;
use App\Models\HubDataPendiente;
use App\NpModel;

class PmsHub extends NpModel
{
    protected $table = 'pms_hubs';

    public function dataPendiente () {
        return $this->hasOne(HubDataPendiente::class,'pms_hub_id');
    }
    public function configs () {
        return $this->hasMany(HubConfig::class,'pms_hub_id');
    }

    public function unidad () {
        return $this->belongsTo(Unidad::class,"unidad_id");
    }

    public function localizaciones () {
        return $this->hasMany(HubLocalizacion::class, 'pms_hub_id');
    }

    public function mensajes () {
        return $this->hasMany(MensajeHub::class,'pms_hub_id');
    }
}
