<?php

namespace App\Models;


use App\NpModel;

class Marca extends NpModel
{
    protected $table = 'marcas';
    public $timestamps = false;

    public function modelos () {
        return $this->hasMany(Modelo::class,'marca_id');
    }
}
