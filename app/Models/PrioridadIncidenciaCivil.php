<?php

namespace App\Models;

use App\NpModel;

class PrioridadIncidenciaCivil extends NpModel
{
    protected $table = 'prioridades_incidencias_civiles';
    public $timestamps = false;

    const URGENTE = 1;
    const ALTA = 2;
}
