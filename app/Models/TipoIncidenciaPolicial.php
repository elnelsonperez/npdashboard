<?php

namespace App\Models;

use App\NpModel;

class TipoIncidenciaPolicial extends NpModel
{
    protected $table = 'tipos_incidencias_policiales';
    public $timestamps = false;

}
