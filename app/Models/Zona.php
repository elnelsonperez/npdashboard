<?php

namespace App\Models;

use App\NpModel;

class Zona extends NpModel
{
    protected $table = "zonas";
    public $timestamps = false;

    public function destacamento () {
        return $this->hasOne(Destacamento::class,'zona_id');
    }

    public function municipio () {
        return $this->belongsTo(Municipio::class,'municipio_id');
    }

    public function sectores () {
        return $this->hasMany(Sector::class,'zona_id');
    }
}
