<?php

namespace App\Models;

use App\NpModel;

class Modelo extends NpModel
{
    protected $table = 'modelos';
    public $timestamps = false;

    public function marca()
    {
        return $this->belongsTo(Marca::class,'marca_id');
    }
}
