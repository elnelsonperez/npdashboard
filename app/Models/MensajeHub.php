<?php

namespace App\Models;

use App\NpModel;
use Illuminate\Support\Facades\Auth;

class MensajeHub extends NpModel
{

    protected $table = "mensajes_hubs";
    const CUARTEL_A_UNIDAD = 0;
    const UNIDAD_A_CUARTEL = 1;

    protected $dates = [
        'fecha_enviado'
    ];

    protected $appends = [
        'remitente'
    ];

    public function scopeWithCellParams ($query) {
        return $query->select([
            'mensajes_hubs.id',
            'mensajes_hubs.creado_en',
            'mensajes_hubs.actualizado_en',
            'mensajes_hubs.sentido',
            'mensajes_hubs.contenido',
            'mensajes_hubs.oficial_unidad_id',
            'mensajes_hubs.estado_mensaje_hub_id',
        ])
      ->with([
            'oficialUnidad' => function ($query) {
                $query->select(['nombre','apellido','id']);
            }
        ]);
    }

    public function getCommonRelations () {
        return ['oficialUnidad','estado','hub'];
    }

    public function getRemitenteAttribute () {
        if ($this->usuario_id) {
            return MensajeHub::find($this->id)->usuario->
            oficial()->select(['id','nombre','apellido'])->get()->first();
        } else {
            return MensajeHub::find($this->id)->hub->unidad->destacamento
                ->currentSargentoGuardia(['id','nombre','apellido']);
        }
    }

    public function usuario () {
        return $this->belongsTo(Usuario::class,'usuario_id');
    }

    public function scopeWithCommon ($query) {
        return $query->with($this->getCommonRelations());
    }

    public function oficialUnidad () {
        return $this->belongsTo(Oficial::class,"oficial_unidad_id");
    }

    public function estado () {
        return $this->belongsTo(EstadoMensajeHub::class,'estado_mensaje_hub_id');
    }

    public function hub () {
        return $this->belongsTo(PmsHub::class,'pms_hub_id');
    }
}
