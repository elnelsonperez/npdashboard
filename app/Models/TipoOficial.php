<?php

namespace App\Models;

use App\NpModel;

class TipoOficial extends NpModel
{
    protected $table = 'tipos_oficiales';
    public $timestamps = false;

    const SUPEVISOR = 2;
    const MIEMBRO = 1;
    const SARGENTO_GUARDIA = 3;
    public function oficiales () {
        return $this->hasMany(Oficial::class);
    }

    public function identifiableName()
    {
        return $this;
    }

}
