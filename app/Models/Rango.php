<?php

namespace App\Models;

use App\NpModel;

class Rango extends NpModel
{
    public $timestamps = false;
    protected $table = 'rangos';
}
