<?php
namespace App\Models;

use App\NpModel;
use Grimzy\LaravelMysqlSpatial\Eloquent\SpatialTrait;

class Sector extends NpModel
{
    use SpatialTrait;
    public $timestamps = false;
    protected $spatialFields = [
        'limites'
    ];

    protected $table = 'sectores';
    public function municipio()
    {
        return $this->belongsTo(Municipio::class,'municipio_id');
    }

    public function unidades () {
        return $this->hasMany(Unidad::class,'sector_id');
    }
    public function zona() {
        return $this->belongsTo(Zona::class,'zona_id');
    }

    public function incidenciasCiviles () {
        return $this->hasMany(IncidenciaCivil::class,'sector_id');
    }
}
