<?php

namespace App\Models;

use App\NpModel;

class EstadoUnidad extends NpModel
{
    protected $table = 'estados_unidades';
    public $timestamps = false;
    const DISPONIBLE = 1;
    const FUERA_DE_SERVICIO = 2;
    const EN_USO = 3;
}
