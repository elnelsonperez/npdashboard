<?php

namespace App\Models;

use App\NpModel;
use Grimzy\LaravelMysqlSpatial\Eloquent\SpatialTrait;
use Illuminate\Support\Facades\App;

class IncidenciaCivil extends NpModel
{
    protected $table = 'incidencias_civiles';
    use SpatialTrait;
    protected $revisionEnabled = true;
    protected $keepRevisionOf = ['estado_id'];

    protected $spatialFields = [
        'ubicacion'
    ];

    protected $dates = [
        'fecha_incidencia'
    ];


    public function scopeValidas ($q) {
        return $q->where('estado_id','!=',EstadoIncidenciaCivil::CANCELADA);
    }

    public function getCommonWithArray () {
        return [
            'tipo',
            'prioridad',
            'estado',
            'sector' => function ($query) {
                $query->select(['id','nombre','municipio_id']);
            },
            'sector.municipio' => function ($query) {
                $query->select(['id','nombre']);
            },
            'unidades',
        ];
    }

    public function getCellDataArray () {
        return [
            'tipo',
            'prioridad',
            'sector' => function ($query) {
                $query->select(['id','nombre']);
            },
            'estado',
            'creador' => function ($query) {
                return $query->select(['id','oficial_id']);
            },
            'creador.oficial' => function ($query){
                return $query->select(['id','nombre','apellido']);
            }
        ];
    }

    public function scopeWithCellData ($query) {
        return $query->with($this->getCellDataArray());
    }

    public function scopeCommonWith ($query) {
        return $query->with($this->getCommonWithArray());
    }

    public function destacamento () {
        return $this->belongsTo(Destacamento::class,"destacamento_id");
    }

    public function creador () {
        return $this->belongsTo(Usuario::class, "creada_por");
    }

    public function tipo () {
        return $this->belongsTo(TipoIncidenciaCivil::class,"tipo_id");
    }

    public function estado () {
        return $this->belongsTo(EstadoIncidenciaCivil::class,'estado_id');
    }

    public function unidades () {
        return $this->belongsToMany(Unidad::class,'unidades_incidencias',
            'incidencia_id','unidad_id')->withTimestamps();
    }

    public function sector () {
        return $this->belongsTo(Sector::class,'sector_id');
    }

    public function prioridad () {
        return $this->belongsTo(PrioridadIncidenciaCivil::class,'prioridad_id');
    }

    public function fechaFinalizada() {
        $filtered = $this->revisionHistory->filter(function ($h) {
            return $h->key == "estado_id";
        });

        $completado = $filtered->search(function ($item) {
            return $item->newValue()->id == EstadoIncidenciaCivil::RESUELTA;
        });

        if ($completado) {
            return $filtered->get($completado)->created_at;
        } else {
            return null;
        }

    }

}