<?php

namespace App\Models;

use App\NpModel;

class EstadoIncidenciaCivil extends NpModel
{
    public $timestamps = false;
    protected $table = 'estados_incidencias_civiles';

    //Ids in DB
    const CREADA = 1;
    const ABIERTA = 2;
    const ASIGNADA = 3;
    const EN_CURSO = 4;
    const CANCELADA = 5;
    const RESUELTA = 6;
    const RECHAZADA = 7;

    public function incidencias () {
        return $this->hasMany(IncidenciaCivil::class);
    }

}