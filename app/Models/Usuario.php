<?php

namespace App\Models;

use App\NpModel;
use Illuminate\Notifications\Notifiable;
use Illuminate\Auth\Authenticatable;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Foundation\Auth\Access\Authorizable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;

class Usuario  extends NpModel implements
    AuthenticatableContract,
    AuthorizableContract,
    CanResetPasswordContract
{
    use Authenticatable, Authorizable, CanResetPassword, Notifiable;
    const TIPO_SADMIN = 5;
    const TIPO_NORMAL = 0;
    const TIPO_911 = 1;
    const TIPO_SARGENTO_GUARDIA = 2;
    const TIPO_SUPERVISOR = 3;

    protected $table = 'usuarios';

    protected $hidden = [
        'password', 'remember_token',
    ];

    public function tipoName () {
        if ($this->tipo == 1) {
            return 'Despachador 911';
        }
        if ($this->tipo == 2) {
            return 'Sargento Guardia';
        }

        if ($this->tipo == 3) {
            return 'Supervisor';
        }
        if ($this->tipo == 5) {
            return 'Administrador';
        }
        return '';
    }

    public function isAdmin () {
        return $this->tipo == self::TIPO_SADMIN;
    }

    public function oficial () {
        return $this->belongsTo(Oficial::class,'oficial_id');
    }

    public function incidenciasPolicicales () {
        return $this->hasMany(IncidenciaPolicial::class,'creada_por');
    }

    public function destacamento () {
        if (!is_null($this->oficial) && !is_null($this->oficial->destacamento))
            return $this->oficial->destacamento;
        return null;
    }


}
