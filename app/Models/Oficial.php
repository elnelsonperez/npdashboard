<?php

namespace App\Models;
use App\NpModel;

class Oficial extends NpModel
{
    protected $table = 'oficiales';
    protected $dates = [
      'fecha_nacimiento', 'fecha_ingreso'
    ];
    protected $revisionEnabled = false;
    protected $keepRevisionOf = array(
        'unidad_id', 'tipo_oficial_id'
    );

    public function usuario () {
        return $this->hasOne(Usuario::class,'oficial_id');
    }

    public function getSexo () {
        return $this->sexo == 0 ? "M" : "F";
    }

    public function flota() {
        return $this->hasOne(Flota::class,'oficial_id');
    }

    public function unidad () {
        return $this->belongsTo(Unidad::class,'unidad_id');
    }

    public function rango () {
        return $this->belongsTo(Rango::class,'rango_id');
    }

    public function destacamento () {
        return $this->belongsTo(Destacamento::class,'destacamento_id');
    }

    public function recados () {
        return $this->hasMany(MensajeHub::class,'enviado_por');
    }

    public function tipo () {
        return $this->belongsTo(TipoOficial::class,'tipo_oficial_id');
    }

    public function ibutton () {
        return $this->hasOne(Ibutton::class,'oficial_id');
    }

    public function incidenciasPoliciales () {
        return $this->belongsToMany(IncidenciaPolicial::class,'incidencias_policiales_oficiales',
            'oficial_id', 'incidencia_policial_id');
    }




}
