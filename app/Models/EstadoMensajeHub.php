<?php

namespace App\Models;

use App\NpModel;

class EstadoMensajeHub extends NpModel
{
    protected $table = 'estados_mensajes_hubs';
    public $timestamps = false;
    const ENVIADO = 1;
    const ENTREGADO = 3;
    const LEIDO = 2;
}
