<?php

namespace App\Models;

use App\NpModel;

class EstadoHistoricoIncidencia extends NpModel
{
    protected $table = 'estados_historicos_incidencias_civiles';

    public function estado () {
        return $this->belongsTo(EstadoIncidenciaCivil::class,'estado_incidencia_id');
    }

    public function usuario () {
        return $this->belongsTo(Usuario::class,'usuario_id');
    }

    public function incidencia () {
        return $this->belongsTo(IncidenciaCivil::class,'incidencia_id');
    }

}