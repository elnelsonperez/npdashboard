<?php

namespace App\Models;

use App\NpModel;
use Grimzy\LaravelMysqlSpatial\Eloquent\SpatialTrait;

class IncidenciaPolicial extends NpModel
{

    protected $table = 'incidencias_policiales';
    use SpatialTrait;
    protected $spatialFields = [
        'ubicacion'
    ];
    protected $dates = [
        'fecha_incidencia'
    ];

    public function destacamento () {
        return $this->belongsTo(Destacamento::class,"destacamento_id");
    }

    public function creador () {
        return $this->belongsTo(Usuario::class, "creada_por");
    }

    public function tipo () {
        return $this->belongsTo(TipoIncidenciaPolicial::class,"tipo_id");
    }

    public function unidades () {
        return $this->belongsToMany(Unidad::class,'incidencias_policiales_unidades',
            'incidencia_policial_id','unidad_id');
    }

    public function oficiales () {
        return $this->belongsToMany(Unidad::class,'incidencias_policiales_oficiales',
            'incidencia_policial_id','oficial_id');
    }

}
