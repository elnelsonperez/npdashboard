<?php

namespace App\Models;

use App\NpModel;

class Flota extends NpModel
{
    protected $table = 'flotas';
    public function oficial() {
        return $this->belongsTo(Oficial::class,'oficial_id');
    }
}
