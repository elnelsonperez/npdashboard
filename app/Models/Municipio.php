<?php

namespace App\Models;

use App\NpModel;
use Grimzy\LaravelMysqlSpatial\Eloquent\SpatialTrait;

class Municipio extends NpModel
{
    protected $table = 'municipios';
    use SpatialTrait;
    public $timestamps = false;
    protected $spatialFields = [
        'limites'
    ];

    public function provincia () {
      return $this->belongsTo(Provincia::class,'provincia_id');
    }

    public function sectores () {
        return $this->hasMany(Sector::class,'municipio_id');
    }
}
