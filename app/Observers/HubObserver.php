<?php

namespace App\Observers;

use App\Models\HubDataPendiente;
use App\Models\PmsHub;

class HubObserver
{
    public function created (PmsHub $hub) {
        HubDataPendiente::create([
            'pms_hub_id' => $hub->id
        ]);
    }
}