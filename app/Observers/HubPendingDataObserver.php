<?php

namespace App\Observers;

use App\Models\HubConfig;
use App\Models\HubDataPendiente;
use App\Models\MensajeHub;
use App\NpModel;

class HubPendingDataObserver
{

    public function created (NpModel $m) {
        $class = get_class($m);
       if ($class == HubConfig::class) {
            HubDataPendiente::where('pms_hub_id',$m->id)
                ->update(['configs' => 1]);
        }
    }

    public function updated (NpModel $m) {
        $class = get_class($m);
         if ($class == HubConfig::class) {
            HubDataPendiente::where('pms_hub_id',$m->id)
                ->update(['configs' => 1]);
        }
    }

}