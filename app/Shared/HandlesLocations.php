<?php

namespace App\Shared;

use Carbon\Carbon;
use CiroVargas\GoogleDistanceMatrix\Response\Element;
use CiroVargas\GoogleDistanceMatrix\Response\GoogleDistanceMatrixResponse;
use CiroVargas\GoogleDistanceMatrix\Response\Row;
use Grimzy\LaravelMysqlSpatial\Types\Point;
use Illuminate\Database\Eloquent\Collection;

trait HandlesLocations {

    public function getDistanceFromPolygon ($polygon, $point) {
        $distancias = [];
        foreach ($polygon->getLineStrings()[0]->getPoints() as $p) {
            $dist = $this->distanceBetween($p->getLat(),$p->getLng(),
                $point->getLat(),$point->getLng());
            array_push($distancias,$dist);
        }
        sort($distancias);
        return $distancias[0];
    }


    public function getSingleDistanceFromMaxtrixResponse (GoogleDistanceMatrixResponse $res) {
        return $res->getRows()[0]->getElements()[0]->getDistance()->getValue();
    }

    public  function distanceBetween($lat1, $lon1, $lat2, $lon2) {
        $theta = $lon1 - $lon2;
        $dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) +  cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
        $dist = acos($dist);
        $dist = rad2deg($dist);
        $miles = $dist * 60 * 1.1515;
        return round($miles*1609.34,2);
    }

    public  function distanceBetweenPoints(Point $p1, Point $p2) {
        return $this->distanceBetween($p1->getLat(),$p1->getLng(),
            $p2->getLat(),$p2->getLng());
    }

    public function findClosest (Collection $collection, $key) {
        $i = null;
        foreach ($collection as $item) {
            if (is_null($i)) {
                $i = $item;
            } else {
                if ($item->$key < $i->$key) {
                    $i = $item;
                }
            }
        }
        return $i;
    }

    public function metersAsKm ($meters) {
        return round($meters/1000,2).' km';
    }

    public function secondsToTime ($seconds) {
        $t = round($seconds);
        return sprintf('%02d:%02d:%02d', ($t/3600),($t/60%60), $t%60);
    }

}